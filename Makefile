.PHONY: %
.DEFAULT_GOAL := up

up:
	docker-compose down; docker-compose up --build

# --project-name as basing on that parameter(default ./ dir name) are recognized running services and the network is created
# Using that parameter allows to simultaneously start local-production with development environment
up-local-prod:
	docker-compose --file local-production.yml --project-name savings-prod up --build --detach

exec:
	docker exec -it savings_api $(cmd) 

sh:
	make exec cmd=sh

units:
	@make exec cmd="sh -c \"ENV=test yarn test:units\""

e2e:
	@make exec cmd="sh -c \"ENV=test yarn test:e2e --testPathPattern=$(p)\""
