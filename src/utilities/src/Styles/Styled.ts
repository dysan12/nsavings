import baseStyled, { ThemedStyledInterface } from 'styled-components';
import { ThemeInterface } from './Theme';

export const styled = baseStyled as ThemedStyledInterface<ThemeInterface>;
