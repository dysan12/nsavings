import { createGlobalStyle } from 'styled-components';
import { ThemeInterface } from './Theme';

export const GlobalStyle = createGlobalStyle<{ theme: ThemeInterface }>`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-size: ${props => props.theme.fontSize.basal};
    font-weight: ${props => props.theme.fontWeight.regular};
    font-family: ${props => props.theme.fontFamily };
    color: ${props => props.theme.colors.text.first};
  },
  html, body {
    background-color: ${props => props.theme.colors.background.first};
  }
`;
