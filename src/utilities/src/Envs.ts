export const Envs = {
  Spreadsheet: {
    ApiKey: process.env.REACT_APP_SPREADSHEET_API_KEY ?? '',
    SpreadsheetId: process.env.REACT_APP_SPREADSHEET_ID ?? '',
    DinnersPageId: process.env.REACT_APP_SPREADSHEET_DINNERS_SHEET_ID ?? '',
    BreakfastPageId: process.env.REACT_APP_SPREADSHEET_BREAKFAST_SHEET_ID ?? '',
  }
}
