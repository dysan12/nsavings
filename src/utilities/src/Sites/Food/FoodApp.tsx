import React, { useState } from "react";
import { styled } from '../../Styles/Styled';
import { IngredientList } from './Components/IngredientList';
import { MealTable } from './Components/MealTable';
import { PlanSettings, usePlanSettings } from './Components/PlanSettings';
import { useFoodSheets, Meal } from './FoodSheet';
import { IngredientProcessor } from './FoodSheet/IngredientProcessor';

export function FoodApp() {
  const useFoodSheetsState = useFoodSheets();
  const [meals, setMeals] = useState<{
    breakfasts: Meal[],
    dinners: Meal[],
    ingredients: string[],
  }>({ breakfasts: [], dinners: [], ingredients: [] });
  const { settings, isInPrintMode, props: settingsProps } = usePlanSettings();

  function generateMeals(props: {
    initialBreakfast?: Meal[],
    initialDinners?: Meal[],
  } = {}) {
    if (useFoodSheetsState.isReady === false) {
      return;
    }
    const breakfasts = useFoodSheetsState.breakfastSheet.getRandomMealsSufficientFor(
      settings.sufficientFor,
      props.initialBreakfast ?? [],
    );
    const dinners = useFoodSheetsState.dinnerSheet.getRandomMealsSufficientFor(
      settings.sufficientFor,
      props.initialDinners ?? [],
    );
    const ingredients = IngredientProcessor.processToArray(
      breakfasts.map(meal => meal.ingredients).concat(dinners.map(meal => meal.ingredients)).join("\n"),
    );
    setMeals({
      breakfasts,
      dinners,
      ingredients,
    });
  }

  if (useFoodSheetsState.isReady === true) {
    return (
      <Container>
        <PlanSettings
          {...settingsProps}
          onGenerate={generateMeals}
        />
        <Header>{'Śniadanie'}</Header>
        <MealTable
          meals={meals.breakfasts}
          isInPrintMode={isInPrintMode}
          onRegenerate={(breakfasts: Meal[]) => generateMeals({ initialBreakfast: breakfasts, initialDinners: meals.dinners })}
        />
        <Header>{'Obiad'}</Header>
        <MealTable
          meals={meals.dinners}
          isInPrintMode={isInPrintMode}
          onRegenerate={(dinners: Meal[]) => generateMeals({ initialBreakfast: meals.breakfasts, initialDinners: dinners })}
        />
        <Header>{'Składniki'}</Header>
        <IngredientList ingredients={meals.ingredients} />
      </Container>
    );
  }

  return (
    <div>
      {'Ładowanie....'}
    </div>
  );
}

const Container = styled.div`
  width: 1100px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: 0 auto;
`;

const Header = styled.h4`
  margin-top: 5px;
  margin-bottom: 5px;
  font-weight: bold;
  font-size: 12px;
`;
