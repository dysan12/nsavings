import React, { useState } from "react";
import { styled } from '../../../Styles/Styled';

type Settings = {
  sufficientFor: number,
}

export function usePlanSettings() {
  const [settings, setSettings] = useState<Settings>({
    sufficientFor: 7
  });
  const [printMode, setPrintMode] = useState(false);

  const props: Omit<Props, 'onGenerate'> = {
    settings,
    onSettingsChange: setSettings,
    isInPrintMode: printMode,
    setIsInPrintMode: () => setPrintMode(true),
  }

  return {
    isInPrintMode: printMode,
    settings,
    props
  }
}

interface Props {
  onGenerate: () => void;
  settings: Settings,
  onSettingsChange: (settings: Settings) => void;
  isInPrintMode: boolean,
  setIsInPrintMode: VoidFunction;
}

export function PlanSettings(props: Props) {
  if (props.isInPrintMode === true) {
    return null;
  }

  return (
    <Container>
      <StyledLabel>
        Na jak długo:
        <StyledInput type={'number'} value={props.settings.sufficientFor} step={1} onChange={(e) => props.onSettingsChange({
          sufficientFor: parseFloat(e.target.value),
        })} />
      </StyledLabel>
      <StyledButton type={'button'} onClick={props.onGenerate}>
        {'Generuj'}
      </StyledButton>
      <StyledButton onClick={() => props.setIsInPrintMode()}>
        {'Tryb drukowania'}
      </StyledButton>
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  margin: 10px;
  justify-content: center;
  align-items: center;
  align-content: center;
`

const StyledLabel = styled.label``

const StyledButton = styled.button`
  padding: 5px;
  `;

const StyledInput = styled.input`
  `
