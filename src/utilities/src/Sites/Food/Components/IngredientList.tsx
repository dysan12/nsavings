import { chunk } from 'lodash';
import React from "react";
import { styled } from '../../../Styles/Styled';

interface Props {
  ingredients: string[],
}

export function IngredientList(props: Props) {
  const chunks = chunk(props.ingredients, 27);
  return (
    <Container>
      {chunks.map(chunk => (
        <Block>
          {chunk.map(ingredient => <StyledIngredient>{`${ingredient};`}</StyledIngredient>)}
        </Block>
      ))}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  * {
    font-size: 10px;
  }
  margin-bottom: 10px;
`

const Block = styled.div`
  display: flex;
  padding: 1px 2px 1px 2px;
  flex-direction: column;
  border: solid 1px;
  width: 300px;
  &:not(:first-of-type) {
    border-left: none;
  }
`

const StyledIngredient = styled.span`

`

