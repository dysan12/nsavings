import React from 'react';
import { styled } from '../../../Styles/Styled';
import { Meal } from '../FoodSheet';
import { IngredientProcessor } from '../FoodSheet/IngredientProcessor';

interface Props {
  meals: Meal[],
  isInPrintMode: boolean,
  onRegenerate: (meals: Meal[]) => void
}

export function MealTable(props: Props) {
  return (
    <Container>
      <StyledTable>
        {props.meals.map(meal => (<StyledRow>
          <NameColumn>
            {meal.name}
          </NameColumn>
          <IngredientsColumn>
            <IngredientList>
              {IngredientProcessor.processToArray(meal.ingredients).map(ingredient => (<Ingredient>{ingredient}</Ingredient>))}
            </IngredientList>
          </IngredientsColumn>
          <DescriptionColumn>
            {meal.description}
          </DescriptionColumn>
          <StyledColumn>
            {meal.sufficientFor}
          </StyledColumn>
          {props.isInPrintMode === false && (
            <StyledColumn>
              <button
                onClick={() => props.onRegenerate(props.meals.filter(filterMeal => filterMeal.id !== meal.id))}
              >
                Remove
              </button>
            </StyledColumn>
          )}
        </StyledRow>))}
      </StyledTable>
    </Container>
  );
}

const Container = styled.div`
  width: 1100px;

  * {
    font-size: 10px;
  }
`;

const StyledTable = styled.table`
  border-collapse: collapse;
`;

const StyledRow = styled.tr`
  border-bottom: solid 1px;
  vertical-align: top;
`;

const StyledColumn = styled.td`
  padding: 5px 3px;

  &:not(:first-of-type) {
    border-left: 1px solid;
  }
`;

const NameColumn = styled(StyledColumn)`
  width: 100px;
`;

const IngredientsColumn = styled(StyledColumn)`
  width: 200px;
`;

const DescriptionColumn = styled(StyledColumn)`
  width: 700px;
`;

const IngredientList = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 6px;
`;

const Ingredient = styled.span`
`;
