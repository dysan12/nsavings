export type Meal = {
  id: string,
  name: string,
  ingredients: string,
  sufficientFor: number,
  description: string,
  priority: string,
}
