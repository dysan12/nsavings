export class IngredientProcessor {
  public static processToArray(ingredients: string): string[] {
    /**
     * " - (w) egg " => - (w) egg
     */
    const processedStr = ingredients.replace(/["]+/g, '');
    /**
     * - (w) egg\n- (m) fish => ["- (w) egg", "- (m) fish"]
     */
    let processed = processedStr.split("\n");
    /**
     * - (w) egg => (w) egg
     */
    processed = processed.map(ing => ing.replace(/^-\s*/, ''));
    /**
     * Sort before removing \(\w+\) to keep the categorized order
     */
    processed = processed.sort();
    /**
     * (w) egg => egg
     */
    processed = processed.map(ing => ing.replace(/^\(\w+\)\s*/, ''));

    /**
     * Filter out empty strings
     */
    processed = processed.filter(ing => ing.trim().length !== 0);

    return processed;
  }
}
