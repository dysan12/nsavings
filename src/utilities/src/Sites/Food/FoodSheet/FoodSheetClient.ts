import { GoogleSpreadsheet, GoogleSpreadsheetWorksheet, GoogleSpreadsheetRow } from 'google-spreadsheet';
import { shuffle } from 'lodash';
import { Envs } from '../../../Envs';
import { Meal } from './types';

export async function buildFoodSheets() {
  const doc = new GoogleSpreadsheet(Envs.Spreadsheet.SpreadsheetId);
  doc.useApiKey(Envs.Spreadsheet.ApiKey);
  await doc.loadInfo();

  const dinnerSheet = await doc.sheetsById[Envs.Spreadsheet.DinnersPageId];
  const breakfastSheet = await doc.sheetsById[Envs.Spreadsheet.BreakfastPageId];

  return {
    dinnerSheet: new FoodSheetClient({
      sheet: dinnerSheet,
      rows: await dinnerSheet.getRows()
    }),
    breakfastSheet: new FoodSheetClient({
      sheet: breakfastSheet,
      rows: await breakfastSheet.getRows(),
    })
  }
}

type LoadedSheet = {
  sheet: GoogleSpreadsheetWorksheet,
  rows: GoogleSpreadsheetRow[],
}

export class FoodSheetClient {
  public constructor(private readonly sheet: LoadedSheet) {}


  public getMeals(): Meal[] {
    const mealsRows = this.getMealsRows();
    return mealsRows.map(row => {
      const data: any = row['_rawData'];
      return ({
        id: data[0],
        name: data[0],
        ingredients: data[1],
        sufficientFor: parseFloat(data[2]),
        priority: data[3],
        description: data[4],
      });
    });
  }

  public getRandomMealsSufficientFor(requestedSufficientFor: number, initialList: Meal[] = []): Meal[] {
    const allMeals = this.getMeals();
    const shuffledMeals = shuffle(allMeals);

    const meals: Meal[] = initialList;
    for (const meal of shuffledMeals) {
      const mealsSufficeFor = meals.reduce((prev, meal) => prev + meal.sufficientFor, 0);
      if (mealsSufficeFor >= requestedSufficientFor) {
        break;
      }
      meals.push(meal);
    }

    return meals;
  }

  private getMealsRows(): GoogleSpreadsheetRow[] {
    const notEmptyRows = this.sheet.rows.filter(row => {
      const data: any = row['_rawData'];
      return typeof data[0] === 'string' && data[0].length > 0;
    })

    return notEmptyRows;
  }
}

