import { useState, useEffect } from 'react';
import { FoodSheetClient } from './FoodSheetClient';
import { buildFoodSheets } from './FoodSheetClient';

type UseFoodSheets = {
  isReady: false,
  dinnerSheet: null
  breakfastSheet: null,
} | {
  isReady: true,
  dinnerSheet: FoodSheetClient
  breakfastSheet: FoodSheetClient,
}

export function useFoodSheets(): UseFoodSheets {
  const [dinnerSheet, setDinnerSheet] = useState<FoodSheetClient | null>(null);
  const [breakfastSheet, setBreakfastSheet] = useState<FoodSheetClient | null>(null);

  useEffect(() => {
    async function initSheets() {
      const sheets = await buildFoodSheets();
      setDinnerSheet(sheets.dinnerSheet);
      setBreakfastSheet(sheets.breakfastSheet);
    }

    initSheets()
  }, []);

  if (dinnerSheet === null || breakfastSheet === null) {
    return { isReady: false, dinnerSheet: null, breakfastSheet: null };
  }
  return { isReady: true, dinnerSheet, breakfastSheet };
}
