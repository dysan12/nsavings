/// <reference types="cypress" />

describe('Rate differences', () => {
  beforeEach(() => {
  })

  it('displays two todo items by default', () => {
    cy.visit('https://kalkulatory2.gofin.pl/Kalkulator-roznic-kursowych-przychodow,12.html')
    cy.get('#formalAgreementModalAgree').click()

    const config = {
      nrFaktury: 'INV-2022-1',
      kwota: '4577,11',
      dataWystawienia: {
        y: '2022',
        m: '03',
        d: '22'
      },
      dataPlatnosci: {
        y: '2022',
        m: '03',
        d: '27'
      },
      waluta: 'CAD'
    }


    cy.get(':nth-child(2) > .wierszPoleEdycji > input').type(config.kwota)
    cy.get(':nth-child(3) > .wierszPoleEdycji > select').select(config.waluta)

    cy.get('[name="l_rok1"]').select(config.dataWystawienia.y)
    cy.get('[name="l_mc1"]').select(config.dataWystawienia.m)
    cy.get('[name="l_dn1"]').select(config.dataWystawienia.d)

    cy.get('[name="l_rok2"]').select(config.dataPlatnosci.y)
    cy.get('[name="l_mc2"]').select(config.dataPlatnosci.m)
    cy.get('[name="l_dn2"]').select(config.dataPlatnosci.d)

    cy.get('#przyciskObliczenia').click()

    cy.get('.wynikiTabela > tbody > :nth-child(3) > td').then($elem => {
      const kursPodatkowy = $elem.text().split(' ')[0]
      cy.get(':nth-child(3) > th > span').then($elem => {
        const tabelaPodatowa = $elem.text().split(':')[1].trim().replace('. źródło', '')
        cy.get('.wynikiTabela > tbody > :nth-child(4) > td').then($elem => {
          const kursPlatnosci = $elem.text().split(' ')[0]
          cy.get(':nth-child(4) > th > span').then($elem => {
            const tabelaPlatnosci = $elem.text().split(':')[1].trim().replace('. źródło', '')
            cy.get('.wynikiTabela > tbody > :nth-child(5) > td').then($elem => {
              const roznica = $elem.text().trim().split(' ')[0]
              cy.get('#ta').type(`Różnice kursowe; Faktura: ${config.nrFaktury}; ${config.waluta}; Kurs podatkowy: ${kursPodatkowy} PLN ${tabelaPodatowa}; Data zapłaty: ${config.dataPlatnosci.d}.${config.dataPlatnosci.m}.${config.dataPlatnosci.y}; Kurs płatności ${kursPlatnosci} PLN ${tabelaPlatnosci};`)
            })
          })
        })
      })
    })
  })
})
