import React from 'react';
import { ThemeProvider } from 'styled-components';
import { FoodApp } from './Sites/Food/FoodApp';
import { GlobalStyle } from './Styles/GlobalStyle';
import { Theme } from './Styles/Theme';
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";

interface Props {

}

export function App(props: Props) {
  return (
    <>
      <ThemeProvider theme={Theme}>
        <BrowserRouter>
          <GlobalStyle />
          <Routes>
            <Route path={'/'} element={<FoodApp />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </>
  )
}
