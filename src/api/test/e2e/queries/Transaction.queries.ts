import { ResponseFields, mutation, query } from 'Test/e2e/utils/Graphql';
import { ObjectID } from 'mongodb';
import { CreateTransaction } from 'Modules/Account/UI/Transaction/CreateTransaction';
import { TransactionResponse } from 'Modules/Account/UI/Transaction/Responses/Transaction.response';
import { ListTransactionResponse } from 'Modules/Account/UI/Transaction/Responses/ListTransaction.response';
import { ListTransactionsRequest } from 'Modules/Account/UI/Transaction/Requests/ListTransactions.request';
import { MutationResponse } from 'Modules/Common/GraphQL/Mutation.response';

export function makeTransactionQuery(data: CreateTransaction | {}, fields: ResponseFields<TransactionResponse>, purposeId?: string) {
  const operations = [
    { purposeId, amount: 10, currency: 'PLN', description: 'Op #1' },
    { purposeId, amount: 20, currency: 'RUB', description: 'Op #2' },
  ];

  const createData = { operations, fulfilledAt: new Date(), description: new ObjectID().toHexString(), ...data };
  const queryName = 'makeTransaction';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'createData', value: createData },
        ],
        fields,
      }),
    },
    queryName,
  };
}

export function removeTransactionQuery(id: string, fields: ResponseFields<MutationResponse>) {
  const queryName = 'removeTransaction';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'id', value: id },
        ],
        fields,
      }),
    },
    queryName,
  };
}

export function getTransactionQuery(id: string, fields: ResponseFields<TransactionResponse>) {
  const queryName = 'transaction';
  return {
    query: {
      operationName: null,
      query: query({
        queryName,
        params: [
          { key: 'id', value: id },
        ],
        fields,
      }),
    },
    queryName,
  };
}

export function getAllTransactionsQuery(queryParams: ListTransactionsRequest, fields: ResponseFields<ListTransactionResponse>) {
  const queryName = 'transactions';
  return {
    query: {
      operationName: null,
      query: query({
        queryName,
        params: [
          { key: 'query', value: queryParams },
        ],
        fields,
      }),
    },
    queryName,
  };
}
