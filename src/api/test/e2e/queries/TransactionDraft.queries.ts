import { ResponseFields, mutation, query } from 'Test/e2e/utils/Graphql';
import { ObjectID } from 'mongodb';
import { MutationResponse } from 'Modules/Common/GraphQL/Mutation.response';
import { CreateTransactionDraftRequest } from 'Modules/Account/UI/TransactionDraft/Requests/CreateTransactionDraft.request';
import { TransactionDraftResponse } from 'Modules/Account/UI/TransactionDraft/Responses/TransactionDraft.response';
import { UpdateTransactionDraftRequest } from 'Modules/Account/UI/TransactionDraft/Requests/UpdateTransactionDraft.request';
import { TransactionDraftListResponse } from 'Modules/Account/UI/TransactionDraft/Responses/TransactionDraftList.response';

export function createTransactionDraftMutation(data: CreateTransactionDraftRequest, fields: ResponseFields<TransactionDraftResponse>) {
  const createData: CreateTransactionDraftRequest = { name: new ObjectID().toHexString(), ...data };
  const queryName = 'createTransactionDraft';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'data', value: createData },
        ],
        fields,
      }),
    },
    queryName,
  };
}

export function updateTransactionDraftMutation(data: UpdateTransactionDraftRequest, fields: ResponseFields<TransactionDraftResponse>) {
  const createData: UpdateTransactionDraftRequest = { name: new ObjectID().toHexString(), ...data };
  const queryName = 'updateTransactionDraft';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'data', value: createData },
        ],
        fields,
      }),
    },
    queryName,
  };
}

export function removeTransactionDraftMutation(id: string, fields: ResponseFields<MutationResponse>) {
  const queryName = 'removeTransactionDraft';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'id', value: id },
        ],
        fields,
      }),
    },
    queryName,
  };
}

export function listTransactionDraftsQuery(fields: ResponseFields<TransactionDraftListResponse>) {
  const queryName = 'listTransactionDrafts';
  return {
    query: {
      operationName: null,
      query: query({
        queryName,
        params: [],
        fields,
      }),
    },
    queryName,
  };
}
