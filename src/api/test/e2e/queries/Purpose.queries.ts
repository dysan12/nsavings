import { CreatePurposeData } from 'Modules/Account/Domain/AccountService';
import { ObjectID } from 'mongodb';
import { mutation, query, ResponseFields } from 'Test/e2e/utils/Graphql';
import { ChangePurposeNameRequest } from 'Modules/Account/UI/Purpose/Requests/ChangePurposeName.request';
import { ListPurposesRequest } from 'Modules/Account/UI/Purpose/Requests/ListPurposes.request';
import { ChangePurposeStatusRequest } from 'Modules/Account/UI/Purpose/Requests/ChangePurposeStatus.request';
import { PurposeResponse } from 'Modules/Account/UI/Purpose/Responses/Purpose.response';
import { PurposeListResponse } from 'Modules/Account/UI/Purpose/Responses/PurposeList.response';

export function createPurposeQuery(data: CreatePurposeData | {}, fields: ResponseFields<PurposeResponse>) {
  const createData = { name: new ObjectID().toHexString(), ...data };
  const queryName = 'createPurpose';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'createData', value: createData },
        ],
        fields,
      }),
    },
    queryName,
  }
}

export function changePurposeNameQuery(data: ChangePurposeNameRequest, fields: ResponseFields<PurposeResponse>) {
  const queryName = 'changePurposeName';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'data', value: data }
        ],
        fields
      })
    },
    queryName
  }
}

export function getAllPurposesQuery(data: ListPurposesRequest, fields: ResponseFields<PurposeListResponse>) {
  const queryName = 'purposes';
  return {
    query: {
      operationName: null,
      query: query({
        queryName,
        params: [
          { key: 'query', value: data }
        ],
        fields
      })
    },
    queryName
  }
}

export function getPurposeQuery(id: string, fields: ResponseFields<PurposeResponse>) {
  const queryName = 'purpose';
  return {
    query: {
      operationName: null,
      query: query({
        queryName,
        params: [
          { key: 'id', value: id }
        ],
        fields
      })
    },
    queryName
  }
}

export function changePurposeStatusQuery(data: ChangePurposeStatusRequest, fields: ResponseFields<PurposeResponse>) {
  const queryName = 'changePurposeStatus';
  return {
    query: {
      operationName: null,
      query: mutation({
        queryName,
        params: [
          { key: 'data', value: data }
        ],
        fields
      })
    },
    queryName
  }
}

