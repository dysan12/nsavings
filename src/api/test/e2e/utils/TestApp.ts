import { Test } from '@nestjs/testing';
import { AppModule } from 'app.module';
import { omit } from 'lodash';
import { applyAppSettings } from 'app.settings';
import { INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongoSeeder, SeedDataset } from 'Test/e2e/utils/MongoSeeder';

export class TestApp {
  private static instance: TestApp;
  private app: INestApplication;

  private constructor(app: INestApplication) {
    this.app = app;
  }

  public static async getInstance(): Promise<TestApp> {
    let instance = TestApp.instance;
    if (!instance) {
      const moduleRef = await Test
        .createTestingModule(omit(AppModule.register(), 'module'))
        .compile();

      const app = moduleRef.createNestApplication();
      applyAppSettings(app);
      await app.init();
      instance = new TestApp(app);
    }
    return instance;
  }

  public getHttpServer(): any {
    return this.app.getHttpServer();
  }

  public getSeeder(): MongoSeeder {
    const configService = this.app.get<ConfigService>(ConfigService);
    return new MongoSeeder(configService.get('MONGO_URL'));
  }

  public static async seedDatabase(data: SeedDataset<any>[]) {
    await (await TestApp.getInstance()).getSeeder().seed(data);
  }

  public static async destroy() {
    await (await TestApp.getInstance()).app.close();
  }
}
