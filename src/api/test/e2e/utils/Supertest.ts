import * as supertest from 'supertest';

// @ts-ignore
const Test = supertest.Test;

const Extensions = {
  token: function(token: string) {
    return this.set('Authorization', `Bearer ${token}`);
  },
  extract: async function(queryName: string, expectedStatus: number) {
    const response = await this;
    const { status, body } = response;
    if (status !== expectedStatus) {
      console.log('Query', response?.request?._data, 'Response', body);
    }
    expect(status).toBe(expectedStatus);
    return { ...response, body: response.body?.data[queryName] ?? undefined };
  },
};

for (const name in Extensions) {
  Test.prototype[name] = Extensions[name];
}

// @ts-ignore
export const request: (app: any) => supertest.SuperTest<supertest.Test & typeof Extensions> & typeof Extensions = supertest;

export type gqlRequestParams = { server: any, token?: string };
export function graphqlRequest(params: gqlRequestParams) {
  const { server, token } = params;
  let req = request(server)
    .post('/graphql');

  if (token) {
    req = req.token(token);
  }
  return req;
}
