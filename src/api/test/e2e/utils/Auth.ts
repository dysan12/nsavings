import { RegisterUserRequest } from 'Modules/User/UI/Requests/RegisterUser.request';
import * as request from 'supertest';

type LoggedUser = RegisterUserRequest & { token: string, id: string };
export async function registerAndLogin(server: any, data: Partial<RegisterUserRequest> = {}): Promise<LoggedUser> {
  const payload: RegisterUserRequest = {
    email: `test+${Math.random()}@test.pl`,
    firstName: 'Tester',
    password: 'qweasdzxc',
    ...data,
  };
  const { body: { id } } = await request(server)
    .post('/user/users')
    .send(payload)
    .expect(201);

  const { body } = await request(server)
    .post('/user/tokens')
    .send({
      email: payload.email,
      password: payload.password,
    })
    .expect(201);

  return {
    ...payload,
    id,
    token: body.token,
  };
}
