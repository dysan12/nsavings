import { Seeder } from 'mongo-seeding/dist';

export interface SeedDataset<Type> {
  collection: string,
  documents: Type[]
}

export class MongoSeeder {
  private seeder: Seeder;

  constructor(url: string) {
    this.seeder = new Seeder({
      dropDatabase: true,
      database: url,
    });
  }

  async seed(dataset: SeedDataset<any>[]) {
    await this.seeder.import(dataset.map(set => ({ documents: set.documents, name: set.collection })));
  }
}
