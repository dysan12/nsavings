import { omit } from 'lodash';

const ENUM_TAG = '___ENUM___:';
export function qlEnum<Enum>(en: Enum, val: keyof Enum): any {
  return `${ENUM_TAG}${val}`;
}

export function serializeData(data: any): string {
  const enumPattern = new RegExp(`"${ENUM_TAG}([a-zA-Z]+)"`, 'g');
  return JSON.stringify(data)
    .replace(
      enumPattern,
      '$1',
    ).replace(
      /"([^(")]+)":/g,
      '$1:');
}

export function mutation(body: QueryBodyParams): string {
  return `
    mutation {
      ${queryBody(body)}
    }
  `;
}

export function query(body: QueryBodyParams): string {
  return `
    query {
      ${queryBody(body)}
    }
  `;
}

export interface QueryBodyParams {
  queryName: string,
  params: { key: string, value: any }[],
  fields: ResponseFields<any>
}

function queryBody({ queryName, params, fields }: QueryBodyParams): string {
  return `
    ${queryName}(${params.map(p => `${p.key}: ${serializeData(p.value)}`).join(',')}) {
      ${parseFields(fields)}
    }
  `;
}

function parseFields(fields: ResponseFields<any>): string {
  if (Array.isArray(fields)) {
    return parsePlainFields(fields);
  } else {
    return parseNestedFields(fields);
  }
}

function parseNestedFields(fields: NestedFields<any>): string {
  let plainFields = fields.root;
  const nestedFields = [];
  for (const field in omit(fields, 'root')) {
    if (fields.root.includes(field)) {
      plainFields = plainFields.filter(e => e !== field);
      nestedFields.push({ key: field, fields: fields[field] });
    }
  }
  const stringifiedNested = nestedFields.map(n => `${n.key} { ${parsePlainFields(n.fields)} }`);
  return `${parseFields(plainFields)}, ${parseFields(stringifiedNested)}`;
}

function parsePlainFields(plainFields: PlainFields<any>): string {
  return plainFields.join(',');
}

export type ResponseFields<T> = PlainFields<T> | NestedFields<T>;

type PlainFields<T> = Array<keyof T>;
type NestedFields<T> = {
  // @ts-ignore
  [key in keyof T]?: Array<Extract<keyof T[key], Array> | keyof T[key][number]>
} & { root: PlainFields<T> };

// TS test to check if interface is correct apart from thrown errors
// interface Foo { name: string , id: number};
// interface Test {
//   id: string,
//   ids: Foo[],
//   name: {
//     firstName: string,
//     lastName: string
//   }
// }
// const k: ResponseFields<Test> = {
//   root: ['id', 'ids', 'name', 'kek'],
//   name: ['lastName', 'lastName', 'kek'],
//   ids: ['name', 'id', 'length', 'kek']
// }
