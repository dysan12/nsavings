import { AccountService } from 'Modules/Account/Domain/AccountService';
import { Injectable } from '@nestjs/common';
import { Command, Positional } from 'nestjs-command';
import * as fs from 'fs';

interface MigrationData {
  purposes: MigrationPurpose[],
  transactions: MigrationTransaction[]
}

interface MigrationPurpose {
  id: null,
  ownerId: null,
  name: string,
  status: 'active',
  operations: []
}

interface MigrationTransaction {
  id: null,
  operations: MigrationOperation[],
  ownerId: null,
  description: string,
  fulfilledAt: {
    date: string, // 2020-04-11 00:00:00.000000,
    timezone_type: 3,
    timezone: 'Europe\/Berlin'
  }
}

interface MigrationOperation {
  id: null,
  purposeId: null,
  subject: string,
  amount: number,
  currency: 'PLN',
  description: string,
  fulfilledAt: {
    date: string, // 2020-04-11 00:00:00.000000,
    timezone_type: 3,
    timezone: 'Europe\/Berlin'
  }
}

@Injectable()
export class MigrateLegacyDataCommand {
  private processedPurposes = [];

  constructor(
    private readonly accountService: AccountService,
  ) { }

  @Command({ command: 'migrate-legacy <filePath> <ownerId>', describe: 'Migrate data from legacy system', autoExit: true })
  async migrate(
    @Positional({
      name: 'filePath',
      describe: 'Migration file path',
      type: 'string',
    }) filePath: string,
    @Positional({
      name: 'ownerId',
      describe: 'Owner ID of the migrated data',
      type: 'string',
    }) ownerId: string,
  ) {
    const migrationJson = await this.loadFile(filePath);
    const { transactions: migrationTransactions } = JSON.parse(migrationJson) as MigrationData;

    for (const transaction of migrationTransactions) {
      const operations = await Promise.all(transaction.operations.map(async o => ({
        purposeId: await this.getPurposeBySubject(o.subject, ownerId),
        amount: o.amount * 100,
        currency: 'PLN',
        description: o.description,
      })));
      const filteredOperations = operations.filter(o => o.amount !== 0);
      if (filteredOperations.length) {
        await this.accountService.makeTransaction({
          description: transaction.description,
          operations: filteredOperations,
          fulfilledAt: new Date(transaction.fulfilledAt.date),
        }, ownerId);
      }
    }
  }

  private async getPurposeBySubject(subject: string, ownerId: string): Promise<string> {
    const filtered = this.processedPurposes.filter(p => p.getName() === subject);
    if (filtered.length) {
      return filtered[0].getId();
    } else {
      const purposeId = await this.accountService.createPurpose({ name: subject }, ownerId);
      const purpose = await this.accountService.findPurpose(purposeId, ownerId);
      this.processedPurposes.push(purpose);
      return purpose.getId();
    }
  }

  private async loadFile(fileLocation: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      fs.readFile(
        fileLocation,
        (error: NodeJS.ErrnoException | null, data: Buffer) => {
          if (error) {
            reject(error);
          } else {
            resolve(data.toString());
          }
        },
      );
    });
  }
}
