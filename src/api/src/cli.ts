import { NestFactory } from '@nestjs/core';
import { AppModule } from 'app.module';
import { CommandService, CommandModule } from 'nestjs-command';

(async () => {
  const app = await NestFactory.createApplicationContext(AppModule.register(), {
    logger: false,
  });
  app.select(CommandModule).get(CommandService).exec();
})();
