import { Inject } from "@nestjs/common";
import { Purpose, PurposeStatus } from "./Purpose/Purpose.entity";
import { PurposeRepository, PurposeRepositoryInterface, PurposesQuery } from "./Purpose/PurposeRepository.interface";
import { Transaction } from "./Transaction/Transaction.entity";
import { TransactionRepository, TransactionRepositoryInterface, TransactionListParams, TransactionListResult } from "./Transaction/TransactionRepository.interface";

export class MakeTransactionData {
  description: string;
  fulfilledAt: Date;
  operations: Array<{
    purposeId: string;
    amount: number;
    currency: string;
    description?: string;
  }>;
}

export class CreatePurposeData {
  name: string;
}

async function inTransaction<T>(fn: (tx: any) => Promise<T>) {
  const transaction = {
    rollback: () => {
      console.log('Rollback not implemented');
    },
  };

  try {
    return await fn(transaction);
  } catch (e) {
    transaction.rollback();
    throw e;
  }
};

export class AccountService {
  constructor(
    @Inject(TransactionRepository) private readonly transactionRepository: TransactionRepositoryInterface,
    @Inject(PurposeRepository) private readonly purposeRepository: PurposeRepositoryInterface,
  ) { }

  public async makeTransaction(data: MakeTransactionData, ownerId: string): Promise<string> {
    return await inTransaction<string>(async (tx) => {
      const operations = [];
      for (const operationData of data.operations) {
        const purpose = await this.purposeRepository.find(operationData.purposeId, ownerId);
        if (null === purpose) {
          throw new Error('Requested purpose doesnt exist'); // todo dedicated exceptions
        }
        const operation = purpose.addOperation({
          ...operationData,
          fulfilledAt: data.fulfilledAt,
          description: operationData.description || operationData.description.length ? operationData.description : data.description, // todo create test
        });
        await this.purposeRepository.save(purpose, { tx });
        operations.push(operation);
      }

      const transaction = Transaction.Create({
        operations,
        description: data.description,
        ownerId,
        fulfilledAt: data.fulfilledAt,
      });

      await this.transactionRepository.save(transaction, { tx });
      return transaction.getId();
    });
  }

  public async removeTransaction(transactionId: string, ownerId: string): Promise<void> {
    await inTransaction(async (tx) => {
      const transaction = await this.transactionRepository.find(transactionId, ownerId);
      if (null === transaction) {
        return;
      }
      const operations = transaction.getOperations();
      for (const operation of operations) {
        const purpose = await this.purposeRepository.find(operation.getPurposeId(), ownerId);
        if (null !== purpose) {
          purpose.removeOperation(operation);
          await this.purposeRepository.save(purpose, { tx });
        }
      }

      await this.transactionRepository.delete(transaction);
    });
  }

  public async createPurpose(data: CreatePurposeData, ownerId: string): Promise<string> {
    const purpose = Purpose.Create(data.name, ownerId);
    await this.purposeRepository.save(purpose);
    return purpose.getId();
  }

  public async changePurposeName(purposeId: string, ownerId: string, newName: string): Promise<void> {
    const purpose = await this.purposeRepository.find(purposeId, ownerId);
    if (null === purpose) {
      throw new Error('Purpose doesnt exist');
    }
    purpose.changeName(newName);

    await this.purposeRepository.save(purpose);
  }

  public async changePurposeStatus(purposeId: string, ownerId: string, newStatus: PurposeStatus): Promise<void> {
    const purpose = await this.purposeRepository.find(purposeId, ownerId);
    if (null === purpose) {
      throw new Error('Purpose doesnt exist');
    }
    purpose.changeStatus(newStatus);

    await this.purposeRepository.save(purpose);
  }

  public async findPurpose(purposeId: string, ownerId: string): Promise<Purpose | null> {
    return this.purposeRepository.find(purposeId, ownerId);
  }

  public async findAllPurposes(ownerId: string, query: PurposesQuery): Promise<Purpose[]> {
    return this.purposeRepository.findAll(ownerId, query);
  }

  public async findTransaction(transactionId: string, ownerId: string): Promise<Transaction | null> {
    return this.transactionRepository.find(transactionId, ownerId);
  }

  public async listTransactions(params: TransactionListParams): Promise<TransactionListResult> {
    return await this.transactionRepository.list(params);
  }
}
