import { ObjectID } from "mongodb";

export class OperationAttributes {
  id: string;
  purposeId: string;
  amount: number;
  currency: string;
  description: string;
  fulfilledAt: Date;
  createdAt: Date;
}

export interface OperationCreateParams {
  purposeId: string,
  amount: number,
  currency: string,
  description: string,
  fulfilledAt: Date
}

export class Operation {
  private attr: OperationAttributes;

  private constructor(attr: OperationAttributes) {
    this.attr = attr;
  }

  public static Create(data: OperationCreateParams): Operation {
    const { purposeId, amount, currency, description, fulfilledAt } = data;
    const self = new this({
      id: new ObjectID().toHexString(),
      purposeId,
      amount: 0,
      currency: 'PLN',
      description,
      fulfilledAt,
      createdAt: new Date()
    });
    self.setValue(amount, currency);
    return self;
  }

  /**
   * @throws {Error}
   */
  private setValue(amount: number, currency: string) {
    if (amount === 0) {
      throw new Error('Cannot create Operation with 0 amount'); // todo
    }
    if (false === Number.isInteger(amount)) {
      throw new Error('The amount can be only an integer'); // todo
    }

    this.attr.amount = amount;
    this.attr.currency = currency;
  }

  public getPurposeId(): string {
    return this.attr.purposeId;
  }

  public getAmount(): number {
    return this.attr.amount;
  }

  public getCurrency(): string {
    return this.attr.currency;
  }

  public getDescription(): string {
    return this.attr.description;
  }

  public getId(): string {
    return this.attr.id;
  }

  public static FromAttributes(attr: OperationAttributes): Operation {
    return new this(attr);
  }

  public toAttributes(): OperationAttributes {
    return this.attr;
  }
}
