import { Test } from "@nestjs/testing";
import { AccountModule } from "../Account.module";
import { AccountService, CreatePurposeData, MakeTransactionData } from "./AccountService";
import { AppModule } from "app.module";
import { PurposeStatus } from 'Modules/Account/Domain/Purpose/Purpose.entity';
import { ObjectID } from 'mongodb';
import { TestingModule } from '@nestjs/testing/testing-module';

describe('AccountService', () => {
  let module: TestingModule;
  let accountService: AccountService;
  beforeAll(async () => {
    module = await Test.createTestingModule(AppModule.registerForTests([
      AccountModule,
    ])).compile();
    module = await module.init();

    accountService = module.get<AccountService>(AccountService);
  });

  afterAll(async () => {
    await module.close();
  });

  async function createPurpose({ data = {}, ownerId = new ObjectID().toHexString() } = {}) {
    const mergedData: CreatePurposeData = { ...{ name: 'Test purpose' }, ...data };
    return { purposeId: await accountService.createPurpose(mergedData, ownerId), ownerId };
  }

  async function makeTransaction(
    ownerId: string, operations: Array<{ amount: number, currency: string, purposeId: string }>): Promise<{ transactionId: string }> {
    const data = {
      description: `Transaction ${new Date().getTime()}`,
      operations: operations.map((op, index) => ({
        amount: op.amount,
        purposeId: op.purposeId,
        currency: op.currency,
        description: op.amount > 0 ? `Donation #${index}` : `Expense #${index}`,
        fulfilledAt: new Date(new Date().getTime() - 1000 * 60 * 60 * 24), // from one day ago
      })),
      fulfilledAt: new Date(new Date().getTime() - 1000 * 60 * 60 * 24), // from one day ago
    };
    return { transactionId: await accountService.makeTransaction(data, ownerId) };
  }

  it('Creates Purpose', async () => {
    const data: CreatePurposeData = { name: 'Create purpose test !' };

    const { purposeId, ownerId } = await createPurpose({ data });

    const purpose = await accountService.findPurpose(purposeId, ownerId);
    expect(purpose).not.toBe(null);
    expect(purpose.getName()).toBe(data.name);
  });

  it('Creates transaction with operation', async () => {
    const { purposeId, ownerId } = await createPurpose();
    const data: MakeTransactionData = {
      description: 'New test transaction',
      operations: [
        { purposeId, amount: 1000, currency: 'PLN', description: 'Test donation' },
      ],
      fulfilledAt: new Date(),
    };

    const transactionId = await accountService.makeTransaction(data, ownerId);

    const transaction = await accountService.findTransaction(transactionId, ownerId);
    expect(transaction).not.toBe(null);
    expect(transaction.getDescription()).toBe(data.description);
    expect(transaction.getOperations()).toHaveLength(1);
    const operation = transaction.getOperations()[0];
    expect(operation.getPurposeId()).toBe(data.operations[0].purposeId);
    expect(operation.getAmount()).toBe(data.operations[0].amount);
    expect(operation.getCurrency()).toBe(data.operations[0].currency);
    expect(operation.getDescription()).toBe(data.operations[0].description);
  });

  it('Adds the same operation to the purpose and to the transaction', async () => {
    const { purposeId, ownerId } = await createPurpose();

    const { transactionId } = await makeTransaction(ownerId, [
      { purposeId, amount: 100, currency: 'PLN' },
    ]);

    const transaction = await accountService.findTransaction(transactionId, ownerId);
    const transactionOperation = transaction.getOperations()[0];
    const purpose = await accountService.findPurpose(purposeId, ownerId);
    const purposeOperation = purpose.getOperations().filter(o => o.getId() === transactionOperation.getId()).pop();
    expect(purposeOperation).not.toBe(undefined);
    expect(purposeOperation.getId()).toBe(transactionOperation.getId());
  });

  it('Archives purpose', async () => {
    const { purposeId, ownerId } = await createPurpose();

    await accountService.changePurposeStatus(purposeId, ownerId, PurposeStatus.Archived);

    const purpose = await accountService.findPurpose(purposeId, ownerId);
    expect(purpose.getStatus()).toBe(PurposeStatus.Archived);
  });

  it('Purpose correctly calculates its balance', async () => {
    const { purposeId: purposeId, ownerId } = await createPurpose();
    await makeTransaction(ownerId, [
      { purposeId, amount: 50, currency: 'PLN' },
      { purposeId, amount: 30, currency: 'PLN' },
      { purposeId, amount: -5, currency: 'RUB' },
      { purposeId, amount: -35, currency: 'RUB' },
      { purposeId, amount: 30, currency: 'RUB' },
      { purposeId, amount: 100, currency: 'GBP' },
    ]);
    const purpose = await accountService.findPurpose(purposeId, ownerId);

    const balance = purpose.getBalance();

    expect(balance).toEqual(
      expect.objectContaining({
        ['PLN']: 80,
        ['GBP']: 100,
        ['RUB']: -10,
      }),
    );
  });

  it('Transactions appends its operations to purposes', async () => {
    const { purposeId: purposeId1, ownerId } = await createPurpose();
    const { purposeId: purposeId2 } = await createPurpose({ ownerId });

    await makeTransaction(ownerId, [
      { amount: 50, currency: 'PLN', purposeId: purposeId1 },
      { amount: 30, currency: 'PLN', purposeId: purposeId2 },
    ]);
    await makeTransaction(ownerId, [
      { amount: 20, currency: 'PLN', purposeId: purposeId1 },
      { amount: 15, currency: 'RUB', purposeId: purposeId2 },
    ]);

    const p1 = await accountService.findPurpose(purposeId1, ownerId);
    expect(p1.getBalance()).toEqual(
      expect.objectContaining({
        ['PLN']: 70,
      }),
    );
    const p2 = await accountService.findPurpose(purposeId2, ownerId);
    expect(p2.getBalance()).toEqual(
      expect.objectContaining({
        ['PLN']: 30,
        ['RUB']: 15,
      }),
    );
  });

  it('Removes transactions with appended operations to the purposes', async () => {
    const { purposeId: p1Id, ownerId } = await createPurpose();
    const { purposeId: p2Id } = await createPurpose({ ownerId });
    await makeTransaction(ownerId, [
      { purposeId: p1Id, currency: 'RUB', amount: -20 },
      { purposeId: p2Id, currency: 'PLN', amount: 100 },
    ]);
    const { transactionId } = await makeTransaction(ownerId, [
      { purposeId: p1Id, currency: 'RUB', amount: 5 },
      { purposeId: p2Id, currency: 'PLN', amount: -30 },
    ]);
    await makeTransaction(ownerId, [
      { purposeId: p1Id, currency: 'RUB', amount: -5 },
      { purposeId: p2Id, currency: 'PLN', amount: 10 },
    ]);

    await accountService.removeTransaction(transactionId, ownerId);

    const transaction = await accountService.findTransaction(transactionId, ownerId);
    const p1Balance = (await accountService.findPurpose(p1Id, ownerId)).getBalance();
    const p2Balance = (await accountService.findPurpose(p2Id, ownerId)).getBalance();
    expect(transaction).toBe(null);
    expect(p1Balance).toEqual(
      expect.objectContaining({
        ['RUB']: -25,
      }),
    );
    expect(p2Balance).toEqual(
      expect.objectContaining({
        ['PLN']: 110,
      }),
    );
  });
});
