import { Purpose, PurposeStatus } from "./Purpose.entity";
import { Transactionable } from "Modules/Common/Repository/Settings";
import { SortingDirection } from 'Modules/Common/Repository/List';

export const PurposeRepository = Symbol('PurposeRepositoryInterface');

export interface PurposesQuery {
  status?: PurposeStatus
  sorting?: {
    createdAt?: SortingDirection;
  }
}

export interface PurposeRepositoryInterface {
  save(purpose: Purpose, settings?: Transactionable): Promise<void>;

  find(purposeId: string, ownerId: string): Promise<Purpose | null>;

  findAll(ownerId: string, query: PurposesQuery): Promise<Purpose[]>;
}

