import { Operation, OperationAttributes } from "../Operation/Operation.entity";
import { ObjectID } from "mongodb";

export enum PurposeStatus {
  Archived = 'archived',
  Active = 'active'
}

export class OperationData {
  amount: number;
  currency: string;
  description: string;
  fulfilledAt: Date;
}

export type PurposeBalance = { [key: string]: number };

export interface PurposeAttributes {
  id: string;
  name: string;
  ownerId: string;
  status: PurposeStatus;
  operations: OperationAttributes[];
  createdAt: Date;
}

export class Purpose {
  private readonly attr: PurposeAttributes;
  private operations: Operation[];

  private constructor(attr: PurposeAttributes) {
    this.attr = attr;
    this.operations = attr.operations.map(o => Operation.FromAttributes(o));
  }

  public static Create(name: string, ownerId: string): Purpose {
    const attr: PurposeAttributes = {
      id: new ObjectID().toHexString(),
      name,
      ownerId,
      status: PurposeStatus.Active,
      operations: [],
      createdAt: new Date()
    };
    return new this(attr);
  }

  public getOwnerId(): string {
    return this.attr.ownerId;
  }

  public getId(): string {
    return this.attr.id;
  }

  public getName(): string {
    return this.attr.name;
  }

  public changeName(newName: string) {
    this.attr.name = newName;
  }

  public changeStatus(newStatus: PurposeStatus) {
    this.attr.status = newStatus;
  }

  public getStatus(): PurposeStatus {
    return this.attr.status;
  }

  public addOperation(data: OperationData): Operation {
    if (this.attr.status === PurposeStatus.Archived) {
      throw new Error('Cannot add operations to an archived Purpose');
    }
    const newOperation = Operation.Create({ purposeId: this.getId(), ...data });
    this.operations.push(newOperation);

    return newOperation;
  }

  public removeOperation(operation: Operation) {
    this.operations = this.operations.filter(o => o.getId() !== operation.getId());
  }

  public getOperations(): Operation[] {
    return this.operations;
  }

  public getBalance(): PurposeBalance  {
    const balance: PurposeBalance = {} as PurposeBalance;
    for (const operation of this.operations) {
      const currency = operation.getCurrency();
      balance[currency] = (balance[currency] ?? 0) + operation.getAmount();
    }

    return balance;
  }

  public static FromAttributes(attr: PurposeAttributes): Purpose {
    return new this(attr);
  }

  public toAttributes(): PurposeAttributes {
    this.attr.operations = this.operations.map(o => o.toAttributes());
    return this.attr;
  }
}
