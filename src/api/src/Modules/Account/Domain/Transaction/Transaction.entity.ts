import { Operation, OperationAttributes } from "../Operation/Operation.entity";
import { ObjectID } from "mongodb";

export interface TransactionAttributes {
  id: string;
  operations: OperationAttributes[];
  ownerId: string;
  description: string;
  fulfilledAt: Date;
  createdAt: Date;
}

export interface TransactionCreateData {
  operations: Operation[],
  description: string,
  fulfilledAt: Date,
  ownerId: string
}

export class Transaction {
  private attr: TransactionAttributes;
  private operations: Operation[];

  private constructor(attr: TransactionAttributes) {
    this.attr = attr;
    this.operations = attr.operations.map(o => Operation.FromAttributes(o));
  }

  public static Create(data: TransactionCreateData): Transaction {
    const { operations, description, ownerId } = data;
    if (operations.length === 0) {
      throw new Error('Cannot create transaction without operations'); // todo 
    }

    return new this({
      id: new ObjectID().toHexString(),
      ownerId,
      description,
      operations: operations.map(o => o.toAttributes()),
      fulfilledAt: data.fulfilledAt,
      createdAt: new Date()
    });
  }

  public getId(): string {
    return this.attr.id;
  }

  public getOperations(): Operation[] {
    return this.operations;
  }

  public getDescription(): string {
    return this.attr.description;
  }

  public static FromAttributes(attr: TransactionAttributes): Transaction {
    return new this(attr);
  }

  public toAttributes(): TransactionAttributes {
    return { ...this.attr, operations: this.operations.map(o => o.toAttributes()) };
  }
}
