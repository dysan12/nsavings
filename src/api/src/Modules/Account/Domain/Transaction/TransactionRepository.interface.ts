import { Transaction, TransactionAttributes } from "./Transaction.entity";
import { Transactionable } from "Modules/Common/Repository/Settings";
import { Pagination, SortingSettings, ListResult } from 'Modules/Common/Repository/List';

type TransactionListQuery = { ownerId: string };
type TransactionListSorting = SortingSettings<TransactionAttributes, 'fulfilledAt'>;
export interface TransactionListParams {
  query: TransactionListQuery,
  pagination: Pagination,
  sorting?: TransactionListSorting
}
export type TransactionListResult = ListResult<Transaction>;

export const TransactionRepository = Symbol('TransactionRepositoryInterface');
export interface TransactionRepositoryInterface {
  find(transactionId: string, ownerId: string): Promise<Transaction | null>;

  findAll(ownerId: string): Promise<Transaction[]>;

  list(params: TransactionListParams): Promise<TransactionListResult>;

  delete(transaction: Transaction): Promise<void>;

  save(transaction: Transaction, settings?: Transactionable): Promise<void>;
}
