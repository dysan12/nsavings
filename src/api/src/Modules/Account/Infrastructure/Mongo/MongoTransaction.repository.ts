import { omit } from 'lodash';
import { InjectCollection } from "Modules/Mongo/InjectCollection.decorator";
import { Collection } from 'mongodb';
import { Transactionable } from "Modules/Common/Repository/Settings";
import { Transaction, TransactionAttributes } from "../../Domain/Transaction/Transaction.entity";
import { TransactionRepositoryInterface, TransactionListParams, TransactionListResult } from "../../Domain/Transaction/TransactionRepository.interface";
import { SortingSettings, SortingDirection } from 'Modules/Common/Repository/List';
import { MongoSortSettings } from 'Modules/Mongo/Repository';

export interface MongoTransaction extends Omit<TransactionAttributes, 'id'> {
  _id: string
}

export class MongoTransactionRepository implements TransactionRepositoryInterface {
  constructor(
    @InjectCollection(Transaction) private collection: Collection<MongoTransaction>,
  ) { }

  async find(transactionId: string, ownerId: string): Promise<Transaction | null> {
    const raw = await this.collection.findOne({
      _id: transactionId,
      ownerId,
    });
    return raw !== null ? this.toEntity(raw) : null;
  }

  async findAll(ownerId: string): Promise<Transaction[]> {
    const raws = await this.collection.find({
      where: {
        ownerId,
      },
    }).toArray();
    return raws.map(raw => this.toEntity(raw));
  }

  async delete(transaction: Transaction): Promise<void> {
    await this.collection.remove({
      _id: transaction.getId(),
    });
  }

  async save(transaction: Transaction, settings?: Transactionable): Promise<void> {
    const attr = transaction.toAttributes();
    await this.collection.updateOne({
      _id: attr.id,
    }, {
      $set: {
        _id: attr.id,
        ...omit(attr, 'id'),
      },
    }, {
      upsert: true,
    });
  }

  private toEntity(raw: MongoTransaction): Transaction {
    return Transaction.FromAttributes({
      id: raw._id,
      ...omit(raw, '_id'),
    });
  }

  async list(params: TransactionListParams): Promise<TransactionListResult> {
    const { query, pagination, sorting } = params;
    const { page, perPage } = pagination;

    const raws = await this.collection.find(query)
      .limit(perPage)
      .skip((page - 1) * perPage)
      .sort(this.mapSortingSettings(sorting))
      .toArray();
    const total = await this.collection.countDocuments(query);

    return {
      data: raws.map(r => this.toEntity(r)),
      pagination: pagination,
      total,
    };
  }

  private mapSortingSettings(settings: SortingSettings<any, any> = {}): MongoSortSettings {
    const entries = Object.entries(settings);
    const mapped = entries.map(e => [e[0], e[1] === SortingDirection.DESC ? -1 : 1]) as [string, 1 | -1][];
    return Object.fromEntries(mapped);
  }
}
