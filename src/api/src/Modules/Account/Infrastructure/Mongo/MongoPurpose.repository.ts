import { Injectable } from "@nestjs/common";
import { omit, omitBy } from 'lodash';
import { InjectCollection } from "Modules/Mongo/InjectCollection.decorator";
import { Collection } from 'mongodb';
import { Transactionable } from "Modules/Common/Repository/Settings";
import { Purpose, PurposeAttributes } from "Modules/Account/Domain/Purpose/Purpose.entity";
import { PurposeRepositoryInterface, PurposesQuery } from "Modules/Account/Domain/Purpose/PurposeRepository.interface";
import { isNullOrUndefined, omitUndefined } from 'Modules/Common/Utils/Filters';
import { mapSortingDirection } from 'Modules/Common/Repository/Mongo';

export interface MongoPurpose extends Omit<PurposeAttributes, 'id'> {
  _id: string
}

@Injectable()
export class MongoPurposeRepository implements PurposeRepositoryInterface {
  constructor(
    @InjectCollection(Purpose) private collection: Collection<MongoPurpose>,
  ) { }

  async save(purpose: Purpose, _settings?: Transactionable): Promise<void> {
    const attr = purpose.toAttributes();
    await this.collection.updateOne({
      _id: attr.id,
    }, {
      $set: {
        _id: attr.id,
        ...omit(attr, 'id'),
      },
    }, {
      upsert: true,
    });
  }

  async find(purposeId: string, ownerId: string): Promise<Purpose | null> {
    const raw = await this.collection.findOne({
      _id: purposeId,
      ownerId,
    });
    return raw !== null ? this.toEntity(raw) : null;
  }

  async findAll(ownerId: string, query: PurposesQuery): Promise<Purpose[]> {
    const { status, sorting } = query;
    const raws = await this.collection.find(omitBy({
      ownerId,
      status,
    }, isNullOrUndefined))
      .sort(omitUndefined({
        createdAt: mapSortingDirection(sorting?.createdAt),
      }))
      .toArray();
    return raws.map(raw => this.toEntity(raw));
  }

  private toEntity(raw: MongoPurpose): Purpose {
    return Purpose.FromAttributes({
      id: raw._id,
      ...omit(raw, ['_id']),
    });
  }
}
