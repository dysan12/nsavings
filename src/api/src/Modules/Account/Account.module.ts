import { Module } from "@nestjs/common";
import { ModuleMetadata } from "@nestjs/common/interfaces";
import { MongoModule } from "../Mongo/MongoModule";
import { AccountService } from "./Domain/AccountService";
import { Purpose } from "./Domain/Purpose/Purpose.entity";
import { PurposeRepository } from "./Domain/Purpose/PurposeRepository.interface";
import { Transaction } from "./Domain/Transaction/Transaction.entity";
import { TransactionRepository } from "./Domain/Transaction/TransactionRepository.interface";
import { MongoPurposeRepository } from "./Infrastructure/Mongo/MongoPurpose.repository";
import { MongoTransactionRepository } from "./Infrastructure/Mongo/MongoTransaction.repository";
import { PurposeResolver } from "./UI/Purpose/Purpose.resolver";
import { TransactionResolver } from "./UI/Transaction/Transaction.resolver";
import { DraftModule } from 'Modules/Draft/Draft.module';
import { TransactionDraftResolver } from 'Modules/Account/UI/TransactionDraft/TransactionDraft.resolver';

@Module(AccountModule.register())
export class AccountModule {
  static register(): ModuleMetadata {
    return {
      imports: [
        MongoModule.forFeature([
          {
            entity: Purpose,
            name: 'purposes',
          },
          {
            entity: Transaction,
            name: 'transactions',
          },
        ]),
        DraftModule
      ],
      controllers: [],
      providers: [
        {
          provide: PurposeRepository,
          useClass: MongoPurposeRepository,
        }, {
          provide: TransactionRepository,
          useClass: MongoTransactionRepository,
        },
        PurposeResolver,
        TransactionResolver,
        TransactionDraftResolver,
        AccountService,
      ],
      exports: [
        AccountService
      ]
    };
  }
}

