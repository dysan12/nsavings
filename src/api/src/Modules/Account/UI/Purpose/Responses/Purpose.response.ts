import { ObjectType, Field, registerEnumType } from "@nestjs/graphql";
import { PurposeStatus, Purpose, PurposeBalance } from "Modules/Account/Domain/Purpose/Purpose.entity";
import { OperationResponse } from 'Modules/Account/UI/Operation/Operation.response';
import { ObjectScalar } from 'Modules/Common/GraphQL/Object.scalar';
import { orderBy } from "lodash";

@ObjectType()
export class PurposeResponse {
  @Field()
  id: string;
  @Field()
  name: string;
  @Field(() => PurposeStatus)
  status: PurposeStatus;
  @Field(() => ObjectScalar)
  balance: PurposeBalance;
  @Field(() => [OperationResponse])
  operations: OperationResponse[];

  constructor(purpose: Purpose) {
    const attr = purpose.toAttributes();
    this.id = attr.id;
    this.name = attr.name;
    this.status = attr.status;
    this.balance = purpose.getBalance();
    this.operations = orderBy(
      purpose.getOperations().map(o => new OperationResponse(o)),
      ['fulfilledAt'],
      ['desc'],
    );
  }
}

registerEnumType(PurposeStatus, {
  name: 'PurposeStatus',
});
