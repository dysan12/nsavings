import { ObjectType, Field } from '@nestjs/graphql';
import { PurposeResponse } from 'Modules/Account/UI/Purpose/Responses/Purpose.response';
import { Purpose } from 'Modules/Account/Domain/Purpose/Purpose.entity';

@ObjectType()
export class PurposeListResponse {
  @Field(() => [PurposeResponse])
  data;

  constructor(purposes: Purpose[]) {
    this.data = purposes.map(p => new PurposeResponse(p));
  }
}
