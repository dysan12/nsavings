import { graphqlRequest, gqlRequestParams } from 'Test/e2e/utils/Supertest';
import { createPurposeQuery, getPurposeQuery, changePurposeNameQuery, changePurposeStatusQuery, getAllPurposesQuery } from 'Test/e2e/queries/Purpose.queries';
import { PurposeResponse } from 'Modules/Account/UI/Purpose/Responses/Purpose.response';
import { MutationStatus } from 'Modules/Common/GraphQL/Mutation.response';
import { PurposeStatus } from 'Modules/Account/Domain/Purpose/Purpose.entity';
import { qlEnum } from 'Test/e2e/utils/Graphql';
import { registerAndLogin } from 'Test/e2e/utils/Auth';
import { TestApp } from 'Test/e2e/utils/TestApp';

describe('Purpose resolver', () => {
  let params: gqlRequestParams;
  beforeAll(async () => {
    const server = (await TestApp.getInstance()).getHttpServer();
    const { token } = await registerAndLogin(server);
    params = {
      server,
      token
    };
  });

  afterAll(async () => {
    await TestApp.destroy();
  });

  async function createPurpose(name?: string) {
    const data = { name: name ?? Math.random().toString() };
    const { query, queryName } = createPurposeQuery(data,
      { root: ['name', 'id', 'status', 'operations'], operations: ['id', 'description', 'fulfilledAt'] });
    const { body } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);
    return body as Partial<PurposeResponse>;
  }

  async function fetchPurpose(id: string) {
    const { query, queryName } = getPurposeQuery(id,
      { root: ['name', 'id', 'status', 'balance', 'operations'], operations: ['id', 'description', 'fulfilledAt'] });

    const { body } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);

    return body as Partial<PurposeResponse>;
  }

  it('Creates purpose', async () => {
    const name = 'For entertainment';

    const purpose = await createPurpose(name);

    expect(purpose.name).toBe(name);
    expect(purpose.id).not.toBeUndefined();
  });

  it('Returns purpose', async () => {
    const purpose = await createPurpose();

    const fetchedPurpose = await fetchPurpose(purpose.id);

    expect(fetchedPurpose).toMatchObject(purpose);
  });

  it('Changes purpose name', async () => {
    const purpose = await createPurpose();
    const newName = Math.random().toString();
    const { query, queryName } = changePurposeNameQuery({ id: purpose.id, name: newName }, ['name']);

    const { body } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);

    expect(body.name).toBe(newName);
  });

  it('Archives purpose', async () => {
    const purpose = await createPurpose();
    const { query, queryName } = changePurposeStatusQuery({ id: purpose.id, status: qlEnum(PurposeStatus, 'Archived') }, ['status']);

    const { body } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);

    expect(body.status).toBe('Archived');
  });

  it('Returns all purposes', async () => {
    const [n1, n2] = [Math.random().toString(), Math.random().toString()];
    const [p1, p2] = [await createPurpose(n1), await createPurpose(n2)];
    const { query, queryName } = getAllPurposesQuery({ status: qlEnum(PurposeStatus, "Active") }, {root: ['data'], data: ['name']});

    const { body: { data } } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);

    expect(data).toContainEqual(
      expect.objectContaining({
        name: n1
      })
    )
    expect(data).toContainEqual(
      expect.objectContaining({
        name: n2
      })
    )
  });
});
