import { Field, InputType } from "@nestjs/graphql";
import { PurposeStatus } from 'Modules/Account/Domain/Purpose/Purpose.entity';
import { SortingDirection } from 'Modules/Common/Repository/List';

@InputType()
class Sorting {
  @Field(() => SortingDirection, { nullable: true })
  createdAt?: SortingDirection;
}

@InputType()
export class ListPurposesRequest {
  @Field(() => PurposeStatus, { nullable: true })
  status?: PurposeStatus;

  @Field(() => Sorting, { nullable: true })
  sorting?: Sorting;
}
