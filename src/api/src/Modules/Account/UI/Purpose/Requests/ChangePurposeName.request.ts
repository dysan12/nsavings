import { Field, InputType } from "@nestjs/graphql";
import { MinLength } from 'class-validator';

@InputType()
export class ChangePurposeNameRequest {
    @Field()
    id: string;
    @Field()
    @MinLength(1)
    name: string;
}
