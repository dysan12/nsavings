import { Field, InputType } from "@nestjs/graphql";
import { PurposeStatus } from 'Modules/Account/Domain/Purpose/Purpose.entity';

@InputType()
export class ChangePurposeStatusRequest {
    @Field()
    id: string;
    @Field(() => PurposeStatus)
    status: PurposeStatus;
}
