import { Args, Query, Resolver, Mutation } from "@nestjs/graphql";
import { AccountService } from "../../Domain/AccountService";
import { PurposeResponse } from "Modules/Account/UI/Purpose/Responses/Purpose.response";
import { CreatePurposeRequest } from "Modules/Account/UI/Purpose/Requests/CreatePurpose.request";
import { MutationResponse, MutationStatus } from 'Modules/Common/GraphQL/Mutation.response';
import { ChangePurposeStatusRequest } from 'Modules/Account/UI/Purpose/Requests/ChangePurposeStatus.request';
import { ChangePurposeNameRequest } from 'Modules/Account/UI/Purpose/Requests/ChangePurposeName.request';
import { ListPurposesRequest } from 'Modules/Account/UI/Purpose/Requests/ListPurposes.request';
import { PurposeListResponse } from 'Modules/Account/UI/Purpose/Responses/PurposeList.response';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'Modules/Common/JWT/Jwt.auth-guard';
import { CurrentUser } from 'Modules/Common/Decorators/CurrentUser.decorator';
import { TokenPayload } from 'Modules/Common/JWT/TokenPayload.service';

@Resolver(() => PurposeResponse)
export class PurposeResolver {
  constructor(
    private readonly accountService: AccountService,
  ) { }

  @Query(() => PurposeListResponse)
  @UseGuards(JwtAuthGuard)
  async purposes(
    @Args('query') query: ListPurposesRequest,
    @CurrentUser() { id: userId }: TokenPayload
  ) {
    const purposes = await this.accountService.findAllPurposes(userId, { status: query.status, sorting: query.sorting });
    return new PurposeListResponse(purposes);
  }

  @Query(() => PurposeResponse, { nullable: true })
  @UseGuards(JwtAuthGuard)
  async purpose(
    @Args('id') id: string,
    @CurrentUser() { id: userId }: TokenPayload
  ) {
    const purpose = await this.accountService.findPurpose(id, userId);

    return purpose !== null ? new PurposeResponse(purpose) : null;
  }

  @Mutation(() => PurposeResponse)
  @UseGuards(JwtAuthGuard)
  async createPurpose(
    @Args('createData') createData: CreatePurposeRequest,
    @CurrentUser() user: TokenPayload
  ) {
    const { id: userId } = user;
    const id = await this.accountService.createPurpose(createData, userId);
    return this.purpose(id, user);
  }

  @Mutation(() => PurposeResponse)
  @UseGuards(JwtAuthGuard)
  async changePurposeStatus(
    @Args('data') data: ChangePurposeStatusRequest,
    @CurrentUser() user: TokenPayload
  ): Promise<PurposeResponse> {
    await this.accountService.changePurposeStatus(data.id, user.id, data.status);
    return this.purpose(data.id, user);
  }

  @Mutation(() => PurposeResponse)
  @UseGuards(JwtAuthGuard)
  async changePurposeName(
    @Args('data') data: ChangePurposeNameRequest,
    @CurrentUser() user: TokenPayload
  ): Promise<PurposeResponse> {
    await this.accountService.changePurposeName(data.id, user.id, data.name);
    return this.purpose(data.id, user);
  }
}

