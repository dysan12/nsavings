import { Field, ID, Int, ObjectType } from "@nestjs/graphql";
import { Operation as DomainOperation } from "../../Domain/Operation/Operation.entity";

@ObjectType()
export class OperationResponse {
  @Field(() => ID)
  id: string;
  @Field()
  purposeId: string;
  @Field(() => Int)
  amount: number;
  @Field()
  currency: string;
  @Field()
  description: string;
  @Field()
  fulfilledAt: Date;

  constructor(operation: DomainOperation) {
    const attr = operation.toAttributes();
    this.id = attr.id.toString();
    this.purposeId = attr.purposeId;
    this.amount = attr.amount;
    this.currency = attr.currency;
    this.description = attr.description;
    this.fulfilledAt = attr.fulfilledAt;
  }
}
