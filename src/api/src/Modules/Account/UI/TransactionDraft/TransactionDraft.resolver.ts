import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import { MutationResponse, MutationStatus } from 'Modules/Common/GraphQL/Mutation.response';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'Modules/Common/JWT/Jwt.auth-guard';
import { CurrentUser } from 'Modules/Common/Decorators/CurrentUser.decorator';
import { TokenPayload } from 'Modules/Common/JWT/TokenPayload.service';
import { TransactionDraftResponse } from 'Modules/Account/UI/TransactionDraft/Responses/TransactionDraft.response';
import { DraftService } from 'Modules/Draft/Application/Draft.service';
import { TransactionDraftListResponse } from 'Modules/Account/UI/TransactionDraft/Responses/TransactionDraftList.response';
import { CreateTransactionDraftRequest } from 'Modules/Account/UI/TransactionDraft/Requests/CreateTransactionDraft.request';
import { UpdateTransactionDraftRequest } from 'Modules/Account/UI/TransactionDraft/Requests/UpdateTransactionDraft.request';

const DRAFT_TYPE = 'transaction';

@Resolver(() => TransactionDraftResponse)
export class TransactionDraftResolver {
  constructor(
    private readonly draftService: DraftService,
  ) { }

  @Mutation(() => TransactionDraftResponse)
  @UseGuards(JwtAuthGuard)
  async createTransactionDraft(
    @Args('data') data: CreateTransactionDraftRequest,
    @CurrentUser() { id: userId }: TokenPayload,
  ) {
    const draft = await this.draftService.create({
      type: DRAFT_TYPE,
      name: data.name,
      data: data.data,
      ownerId: userId,
    });
    return new TransactionDraftResponse(draft);
  }

  @Mutation(() => TransactionDraftResponse)
  @UseGuards(JwtAuthGuard)
  async updateTransactionDraft(
    @Args('data') data: UpdateTransactionDraftRequest,
    @CurrentUser() { id: userId }: TokenPayload,
  ) {
    const draft = await this.draftService.update({
      id: data.id,
      ownerId: userId,
      name: data.name,
      data: data.data,
    });
    return new TransactionDraftResponse(draft);
  }

  @Query(() => TransactionDraftListResponse)
  @UseGuards(JwtAuthGuard)
  async listTransactionDrafts(
    @CurrentUser() { id: userId }: TokenPayload,
  ) {
    const drafts = await this.draftService.findAllByType(DRAFT_TYPE, userId);
    return new TransactionDraftListResponse(drafts);
  }

  @Mutation(() => MutationResponse)
  @UseGuards(JwtAuthGuard)
  async removeTransactionDraft(
    @Args('id') id: string,
    @CurrentUser() { id: userId }: TokenPayload,
  ) {
    await this.draftService.remove(id, userId);
    return new MutationResponse({
      status: MutationStatus.OK,
      message: 'Draft removed successfully',
    });
  }
}

