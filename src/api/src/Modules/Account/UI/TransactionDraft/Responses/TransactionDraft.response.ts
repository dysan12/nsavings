import { ObjectType, Field, ID, Int } from "@nestjs/graphql";
import { DraftAttributes } from 'Modules/Draft/Domain/Draft.entity';

@ObjectType()
class TransactionDraftOperation {
  @Field({ nullable: true })
  purposeId?: string;
  @Field(() => Int, { nullable: true })
  amount?: number;
  @Field({ nullable: true })
  currency?: string;
  @Field({ nullable: true })
  description?: string;
}

@ObjectType()
class Data {
  @Field({ nullable: true })
  description?: string;
  @Field(() => [TransactionDraftOperation], { nullable: true })
  operations?: TransactionDraftOperation[];
}

@ObjectType()
export class TransactionDraftResponse {
  @Field(() => ID)
  id: string;
  @Field()
  name: string;
  @Field(() => Data)
  data: Data;

  constructor(draft: DraftAttributes) {
    this.id = draft.id;
    this.name = draft.name;
    this.data = draft.data;
  }
}
