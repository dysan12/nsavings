import { ObjectType, Field } from '@nestjs/graphql';
import { TransactionDraftResponse } from 'Modules/Account/UI/TransactionDraft/Responses/TransactionDraft.response';
import { DraftAttributes } from 'Modules/Draft/Domain/Draft.entity';

@ObjectType()
export class TransactionDraftListResponse {
  @Field(() => [TransactionDraftResponse])
  data: TransactionDraftResponse[];

  constructor(drafts: DraftAttributes[]) {
    this.data = drafts.map(d => new TransactionDraftResponse(d));
  }
}
