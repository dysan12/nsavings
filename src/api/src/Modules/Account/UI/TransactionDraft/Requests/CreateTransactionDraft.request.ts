import { Field, InputType, Int } from "@nestjs/graphql";
import { TransactionDraftData } from 'Modules/Account/UI/TransactionDraft/Requests/Common';

@InputType()
export class CreateTransactionDraftRequest {
  @Field()
  name: string;
  @Field(() => TransactionDraftData)
  data: TransactionDraftData;
}
