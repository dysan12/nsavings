import { Field, InputType } from "@nestjs/graphql";
import { TransactionDraftData } from 'Modules/Account/UI/TransactionDraft/Requests/Common';

@InputType()
export class UpdateTransactionDraftRequest {
  @Field()
  id: string;
  @Field()
  name: string;
  @Field(() => TransactionDraftData)
  data: TransactionDraftData;
}
