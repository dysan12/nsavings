import { InputType, Field, Int, ObjectType } from '@nestjs/graphql';

@InputType()
export class TransactionDraftData {
  @Field({ nullable: true })
  description?: string;
  @Field(() => [TransactionDraftOperationsInput], { nullable: true })
  operations?: TransactionDraftOperationsInput[];
}

@InputType()
export class TransactionDraftOperationsInput {
  @Field({ nullable: true })
  purposeId?: string;
  @Field(() => Int, { nullable: true })
  amount?: number;
  @Field({ nullable: true })
  currency?: string;
  @Field({ nullable: true })
  description?: string;
}
