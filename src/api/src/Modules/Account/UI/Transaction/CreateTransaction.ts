import { Field, InputType, Int } from "@nestjs/graphql";

@InputType()
export class CreateTransaction {
  @Field()
  description: string;
  @Field(() => [OperationData])
  operations: OperationData[];
  @Field()
  fulfilledAt: Date;
}

@InputType()
class OperationData {
  @Field()
  purposeId: string;
  @Field(() => Int)
  amount: number;
  @Field()
  currency: string;
  @Field()
  description: string;
}
