import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import { AccountService } from "../../Domain/AccountService";
import { CreateTransaction } from "./CreateTransaction";
import { TransactionResponse } from "Modules/Account/UI/Transaction/Responses/Transaction.response";
import { MutationResponse, MutationStatus } from 'Modules/Common/GraphQL/Mutation.response';
import { ListTransactionsRequest } from 'Modules/Account/UI/Transaction/Requests/ListTransactions.request';
import { Pagination } from 'Modules/Common/Repository/List';
import { ListTransactionResponse } from 'Modules/Account/UI/Transaction/Responses/ListTransaction.response';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'Modules/Common/JWT/Jwt.auth-guard';
import { CurrentUser } from 'Modules/Common/Decorators/CurrentUser.decorator';
import { TokenPayload } from 'Modules/Common/JWT/TokenPayload.service';

@Resolver(() => TransactionResponse)
export class TransactionResolver {
  constructor(
    private readonly accountService: AccountService,
  ) { }

  @Query(() => TransactionResponse, { nullable: true })
  @UseGuards(JwtAuthGuard)
  async transaction(
    @Args('id') id: string,
    @CurrentUser() { id: userId }: TokenPayload,
  ) {
    const transaction = await this.accountService.findTransaction(id, userId);

    return transaction !== null ? new TransactionResponse(transaction) : transaction;
  }

  @Mutation(() => TransactionResponse)
  @UseGuards(JwtAuthGuard)
  async makeTransaction(
    @Args('createData') createData: CreateTransaction,
    @CurrentUser() user: TokenPayload,
  ) {
    const { id: userId } = user;
    const id = await this.accountService.makeTransaction(createData, userId);
    return this.transaction(id, user);
  }

  @Query(() => ListTransactionResponse)
  @UseGuards(JwtAuthGuard)
  async transactions(
    @Args('query') query: ListTransactionsRequest,
    @CurrentUser() { id: userId }: TokenPayload,
  ) {
    const list = await this.accountService.listTransactions({
      query: {
        ownerId: userId
      },
      pagination: new Pagination(query.page, query.perPage),
      sorting: query.sortBy
    });
    return new ListTransactionResponse(list);
  }

  @Mutation(() => MutationResponse)
  @UseGuards(JwtAuthGuard)
  async removeTransaction(
    @Args('id') id: string,
    @CurrentUser() { id: userId }: TokenPayload,
  ) {
    await this.accountService.removeTransaction(id, userId);
    return new MutationResponse({
      status: MutationStatus.OK,
      message: 'Transaction removed successfully',
    });
  }
}

