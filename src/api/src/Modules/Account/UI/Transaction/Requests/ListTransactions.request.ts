import { InputType, Field } from '@nestjs/graphql';
import { SortingDirection } from 'Modules/Common/Repository/List';
import { PositiveInt } from 'Modules/Common/GraphQL/ExternalScalars';

@InputType()
export class SortFields {
  @Field(() => SortingDirection, { nullable: true })
  fulfilledAt?: SortingDirection;
}

@InputType()
export class ListTransactionsRequest {
  @Field(() => PositiveInt)
  page: number;

  @Field(() => PositiveInt)
  perPage: number;

  @Field(() => SortFields, { nullable: true })
  sortBy?: SortFields;
}
