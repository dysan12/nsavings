import { TestApp } from 'Test/e2e/utils/TestApp';
import { graphqlRequest, gqlRequestParams } from 'Test/e2e/utils/Supertest';
import { makeTransactionQuery, getTransactionQuery, getAllTransactionsQuery, removeTransactionQuery } from 'Test/e2e/queries/Transaction.queries';
import { TransactionResponse } from 'Modules/Account/UI/Transaction/Responses/Transaction.response';
import { CreateTransaction } from 'Modules/Account/UI/Transaction/CreateTransaction';
import { registerAndLogin } from 'Test/e2e/utils/Auth';
import { SeedDataset } from 'Test/e2e/utils/MongoSeeder';
import { MongoPurpose } from 'Modules/Account/Infrastructure/Mongo/MongoPurpose.repository';
import { PurposeStatus } from 'Modules/Account/Domain/Purpose/Purpose.entity';
import { ObjectId } from 'mongodb';
import { MutationStatus } from 'Modules/Common/GraphQL/Mutation.response';

const PURPOSE_ID = new ObjectId().toHexString();

describe('Transaction resolver', () => {
  let params: gqlRequestParams;
  let userId: string;
  beforeAll(async () => {
    const server = (await TestApp.getInstance()).getHttpServer();
    const { token, id } = await registerAndLogin(server);
    params = {
      server,
      token,
    };
    userId = id;
  });
  beforeEach(async () => {
    await TestApp.seedDatabase(getSeedDataset(userId));
  });
  afterAll(async () => {
    await TestApp.destroy();
  });

  async function makeTransaction(data: Partial<CreateTransaction> = {}) {
    const { query, queryName } = makeTransactionQuery(
      data,
      { root: ['id', 'operations', 'description', 'fulfilledAt'], operations: ['id', 'amount', 'currency'] },
      PURPOSE_ID,
    );
    const { body } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);
    return body as Partial<TransactionResponse>;
  }

  async function getTransaction(id: string) {
    const { query, queryName } = getTransactionQuery(id, ['id', 'description']);
    const { body } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);
    return body as Partial<TransactionResponse>;
  }

  it('Makes transaction', async () => {
    const purposeId = PURPOSE_ID;
    const operations = [
      { purposeId, amount: 10, currency:  'PLN', description: 'Op #1' },
      { purposeId, amount: 20, currency:  'RUB', description: 'Op #2' },
    ];
    const description = 'Trans #1';

    const transaction = await makeTransaction({ operations, description });

    expect(transaction).not.toBeUndefined();
    expect(transaction.description).toEqual(description);
    expect(transaction.operations).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          amount: 10,
          currency: 'PLN',
        }),
        expect.objectContaining({
          amount: 20,
          currency: 'RUB',
        }),
      ]),
    );
  });

  it('Removes transaction', async () => {
    const transaction = await makeTransaction();

    const { query, queryName } = removeTransactionQuery(transaction.id, ['status']);
    const { body } = await graphqlRequest(params)
      .send(query)
      .extract(queryName, 200);

    expect(body.status).toEqual(MutationStatus.OK);
  });

  it('Returns transaction', async () => {
    const { id, description } = await makeTransaction();

    const transaction = await getTransaction(id);

    expect(transaction.description).toEqual(description);
  });

  describe('Returns paginated transactions', () => {
    it('with perPage grater than collection size', async () => {
      const [{ id: id1 }, { id: id2 }] = [await makeTransaction(), await makeTransaction()];
      const { query, queryName } = getAllTransactionsQuery(
        { page: 1, perPage: 10 },
        { root: ['total', 'data', 'pagination'], data: ['id'], pagination: ['page', 'perPage'] },
      );

      const { body } = await graphqlRequest(params)
        .send(query)
        .extract(queryName, 200);

      expect(body).not.toBeUndefined();
      expect(body.total).toEqual(2);
      expect(body.pagination.page).toEqual(1);
      expect(body.pagination.perPage).toEqual(10);
      expect(body.data).toHaveLength(2);
      expect(body.data).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ id: id1 }),
          expect.objectContaining({ id: id2 }),
        ]),
      );
    });
    it('With perPage less than collection size', async () => {
      const [{ id: id1 }] = [await makeTransaction(), await makeTransaction()];
      const { query, queryName } = getAllTransactionsQuery(
        { page: 1, perPage: 1 },
        { root: ['total', 'data', 'pagination'], data: ['id'], pagination: ['page', 'perPage'] },
      );

      const { body } = await graphqlRequest(params)
        .send(query)
        .extract(queryName, 200);

      expect(body).not.toBeUndefined();
      expect(body.total).toEqual(2);
      expect(body.pagination.page).toEqual(1);
      expect(body.pagination.perPage).toEqual(1);
      expect(body.data).toHaveLength(1);
      expect(body.data).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ id: id1 }),
        ]),
      );
    });
  });

});

function getSeedDataset(ownerId: string): SeedDataset<any>[] {
  const purposes: SeedDataset<MongoPurpose> = {
    collection: 'purposes',
    documents: [
      { _id: PURPOSE_ID, status: PurposeStatus.Active, name: 'Any name', operations: [], ownerId, createdAt: new Date() },
    ],
  };
  return [
    purposes,
  ];
}
