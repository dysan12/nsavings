import { ObjectType, Field } from '@nestjs/graphql';
import { NonNegativeInt } from 'Modules/Common/GraphQL/ExternalScalars';
import { TransactionResponse } from 'Modules/Account/UI/Transaction/Responses/Transaction.response';
import { TransactionListResult } from 'Modules/Account/Domain/Transaction/TransactionRepository.interface';
import { PaginationObject } from 'Modules/Common/GraphQL/Pagination.object';

@ObjectType()
export class ListTransactionResponse {
  @Field(() => [TransactionResponse])
  data: TransactionResponse[];
  @Field(() => NonNegativeInt)
  total: number;
  @Field(() => PaginationObject)
  pagination: PaginationObject;

  constructor(list: TransactionListResult) {
    this.data = list.data.map(t => new TransactionResponse(t));
    this.total = list.total;
    this.pagination = new PaginationObject(list.pagination);
  }
}
