import { ObjectType, Field, ID } from "@nestjs/graphql";
import { Transaction as DomainTransaction } from "Modules/Account/Domain/Transaction/Transaction.entity";
import { OperationResponse } from "Modules/Account/UI/Operation/Operation.response";

@ObjectType()
export class TransactionResponse {
  @Field(() => ID)
  id: string;
  @Field(() => [OperationResponse])
  operations: OperationResponse[];
  @Field()
  description: string;
  @Field()
  fulfilledAt: Date;

  constructor(transaction: DomainTransaction) {
    const attr = transaction.toAttributes();
    this.id = attr.id;
    this.operations = transaction.getOperations().map(o => new OperationResponse(o));
    this.description = attr.description;
    this.fulfilledAt = attr.fulfilledAt;
  }
}
