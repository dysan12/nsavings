import { JwtService } from '@nestjs/jwt';
import { Injectable, Inject } from '@nestjs/common';

// Wrapper, allows to import externally once configured service
@Injectable()
export class JwtWrapperService {
  constructor(
    @Inject(JwtService) private readonly nestJwtService: JwtService,
  ) {}

  public async signAsync(payload: string | Buffer | object): Promise<string> {
    return await this.nestJwtService.signAsync(payload);
  }
}
