import { User } from 'Modules/User/Domain/User.entity';
import { pick } from 'lodash';

export interface TokenPayload {
  id: string;
  email: string;
  firstName: string;
}

export class TokenPayloadService {
  public static userToPayload(user: User): TokenPayload {
    return {
      id: user.getId(),
      email: user.getEmail(),
      firstName: user.getFirstName(),
    };
  }
  public static pickToken(payload: any): TokenPayload {
    return { ...pick(payload, ['firstName', 'email', 'id']) };
  }
}


