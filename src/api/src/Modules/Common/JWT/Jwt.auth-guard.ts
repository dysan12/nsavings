import { Injectable, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  getRequest(context: ExecutionContext) {
    const type = context.getType() as string;
    let req;
    if (type === 'graphql') {
      req = GqlExecutionContext.create(context).getContext().req;
    } else if (type === 'http') {
      req = context.switchToHttp().getRequest();
    }
    if (!req) {
      throw new Error(`Context type "${type}" not supported`);
    }

    return req;
  }
}
