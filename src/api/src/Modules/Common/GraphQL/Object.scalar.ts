import { Scalar, CustomScalar } from '@nestjs/graphql';

@Scalar('Object')
export class ObjectScalar implements CustomScalar<object, object> {
  description: 'Object scalar';

  parseLiteral(ast: any): any {
    return {};
  }

  parseValue(value: object): any {
    return value;
  }

  serialize(value: object): object {
    return value;
  }
}
