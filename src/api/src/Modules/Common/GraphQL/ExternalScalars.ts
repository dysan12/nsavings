import { resolvers } from 'graphql-scalars';
import { Scalar, CustomScalar } from '@nestjs/graphql';

@Scalar('PositiveInt')
export class PositiveInt implements CustomScalar<object, object> {
  description: string = resolvers.PositiveInt.description;

  parseLiteral(ast: any): any { return resolvers.PositiveInt.parseLiteral(ast, {}); }

  parseValue(value: any): any { return resolvers.PositiveInt.parseValue(value); }

  serialize(value: any): any { return resolvers.PositiveInt.serialize(value); }
}

@Scalar('NonNegativeInt')
export class NonNegativeInt implements CustomScalar<object, object> {
  description: string = resolvers.NonNegativeInt.description;

  parseLiteral(ast: any): any { return resolvers.NonNegativeInt.parseLiteral(ast, {}); }

  parseValue(value: any): any { return resolvers.NonNegativeInt.parseValue(value); }

  serialize(value: any): any { return resolvers.NonNegativeInt.serialize(value); }
}

export const ExternalScalars = [
  PositiveInt,
  NonNegativeInt,
];
