import { Scalar, CustomScalar } from '@nestjs/graphql';

type Any = object | string | number | boolean | Array<Any>

@Scalar('Any')
export class AnyScalar implements CustomScalar<Any, Any> {
  description: 'Any scalar';

  parseLiteral(ast: any): any {
    return {};
  }

  parseValue(value: Any): any {
    return value;
  }

  serialize(value: Any): Any {
    return value;
  }
}
