import { ObjectType, Field, registerEnumType } from "@nestjs/graphql";
import { AnyScalar } from 'Modules/Common/GraphQL/Any.scalar';

export enum MutationStatus {
  OK = 'OK',
  SERVER_ERROR = 'SERVER_ERROR',
  REQUEST_ERROR = 'REQUEST_ERROR',
  FORBIDDEN = 'FORBIDDEN'
}

@ObjectType()
export class MutationResponse {
  @Field(() => MutationStatus)
  status: MutationStatus;
  @Field(() => AnyScalar)
  message: any;

  constructor(params: { status: MutationStatus, message: any }) {
    const { status, message } = params;
    this.status = status;
    this.message = message;
  }
}

registerEnumType(MutationStatus, {
  name: 'MutationStatus',
});
