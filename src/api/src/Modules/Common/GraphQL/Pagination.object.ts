import { ObjectType, Field } from '@nestjs/graphql';
import { PositiveInt } from 'Modules/Common/GraphQL/ExternalScalars';
import { Pagination } from 'Modules/Common/Repository/List';

@ObjectType()
export class PaginationObject {
  @Field(() => PositiveInt)
  page: number;
  @Field(() => PositiveInt)
  perPage: number;

  constructor(pag: Pagination) {
    this.page = pag.page;
    this.perPage = pag.perPage;
  }
}
