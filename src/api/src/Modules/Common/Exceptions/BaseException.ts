export class BaseException {
  constructor(
    public readonly message: string,
    public readonly status: string
  ) {
  }
}
