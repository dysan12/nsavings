import { omitBy } from "lodash";

export const isNullOrUndefined = (value) => value === null || value === undefined;
export function omitUndefined(obj: any) {
  return omitBy(obj, v => v === undefined);
}
