import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { TokenPayload } from 'Modules/Common/JWT/TokenPayload.service';

export const CurrentUser = createParamDecorator<any, any, TokenPayload>(
  (data: unknown, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();
    let user: TokenPayload = request?.user;
    if (!user) {
      const ctx = GqlExecutionContext.create(context);
      user = ctx.getContext().req.user;
    }
    return user;
  },
);
