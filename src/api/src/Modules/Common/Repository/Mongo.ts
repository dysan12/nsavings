import { SortingDirection } from 'Modules/Common/Repository/List';

export function mapSortingDirection(direction?: SortingDirection): undefined | 1 | -1 {
  if (!direction) {
    return undefined;
  }
  return direction === SortingDirection.DESC ? -1 : 1;
}
