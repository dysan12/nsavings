// todo simple units

import { registerEnumType } from '@nestjs/graphql';

export class Pagination {
  constructor(public readonly page: number, public readonly perPage: number) {
    if (page <= 0) {
      throw new Error('Pagination page cannot be less than 0'); // todo dedicated error
    }
    if (perPage <= 0) {
      throw new Error('Pagination limit cannot be less than 0'); // todo dedicated error
    }
  }
}

export enum SortingDirection {
  ASC = 'ASC',
  DESC = 'DESC'
}

export type SortingSettings<T, K extends keyof T> = {
  [P in K]?: SortingDirection;
};

export type ListResult<T> = {
  data: T[],
  total: number
  pagination: Pagination
}

registerEnumType(SortingDirection, {
  name: 'SortingDirection',
});
