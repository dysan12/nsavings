import { Global, Module } from "@nestjs/common";
import { ObjectScalar } from 'Modules/Common/GraphQL/Object.scalar';
import { AnyScalar } from 'Modules/Common/GraphQL/Any.scalar';
import { ExternalScalars } from 'Modules/Common/GraphQL/ExternalScalars';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtWrapperService } from 'Modules/Common/JWT/JwtWrapper.service';
import { JwtAuthStrategy } from 'Modules/Common/JWT/Jwt.auth-strategy';
import { ConfigService } from '@nestjs/config';

@Global()
@Module({
  imports: [
    PassportModule,
    JwtAuthStrategy,
    JwtModule.registerAsync({
      useFactory: async (config: ConfigService) => ({
        secret: config.get('JWT_SECRET', 'secret'),
        signOptions: { expiresIn: '60m' },
      }),
      inject: [ConfigService]
    })
  ],
  providers: [
    ObjectScalar,
    AnyScalar,
    ...ExternalScalars,
    JwtWrapperService,
  ],
  exports: [
    ObjectScalar,
    AnyScalar,
    ...ExternalScalars,
    JwtWrapperService,
  ],
})
export class CommonModule {

}
