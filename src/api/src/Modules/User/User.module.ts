import { Module } from '@nestjs/common';
import { UsersController } from 'Modules/User/UI/Users.controller';
import { UserService } from 'Modules/User/Application/User.service';
import { USER_REPOSITORY } from 'Modules/User/Domain/User.repository';
import { MongoUserRepository } from 'Modules/User/Infrastructure/MongoUser.repository';
import { MongoModule } from 'Modules/Mongo/MongoModule';
import { User } from 'Modules/User/Domain/User.entity';
import { AuthenticationService } from 'Modules/User/Application/Authentication.service';

@Module({
  imports: [
    MongoModule.forFeature([
      { entity: User, name: 'users' },
    ]),
  ],
  controllers: [
    UsersController,
  ],
  providers: [
    UserService,
    {
      provide: USER_REPOSITORY,
      useClass: MongoUserRepository,
    },
    AuthenticationService,
  ],
})
export class UserModule {}
