import { User } from 'Modules/User/Domain/User.entity';

export const USER_REPOSITORY = Symbol('user-repository');
export interface UserRepository {
  save(user: User): Promise<void>;
  findOne(id: string): Promise<User | null>;
  findOneByEmail(email: string): Promise<User | null>;
}
