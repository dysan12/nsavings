import { ObjectId } from 'mongodb';
import { hash, compare } from 'bcryptjs';

export class UserAttributes {
  id: string;
  firstName: string;
  email: string;
  passwordHash: string;
}

export interface CreateUserParams {
  firstName: string;
  email: string;
  password: string;
}

export class User {
  private attr: UserAttributes;

  private constructor(attr: UserAttributes) {
    this.attr = attr;
  }

  public static async Create(params: CreateUserParams): Promise<User> {
    const { password, ...restParams } = params;
    const passwordHash = await this.hashPassword(password);
    return new this({
      id: new ObjectId().toHexString(),
      passwordHash,
      firstName: restParams.firstName,
      email: restParams.email,
    });
  }

  public async doesPasswordMatch(password: string): Promise<boolean> {
    return await compare(password, this.attr.passwordHash);
  }

  public getEmail(): string {
    return this.attr.email;
  }

  public getFirstName(): string {
    return this.attr.firstName;
  }

  public getId(): string {
    return this.attr.id;
  }

  public toAttributes(): UserAttributes {
    return this.attr;
  }

  public static FromAttributes(attr: UserAttributes) {
    return new this(attr);
  }

  private static async hashPassword(pwd: string): Promise<string> {
    return await hash(pwd, 10);
  }
}
