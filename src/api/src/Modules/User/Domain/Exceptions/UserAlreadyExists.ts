import { BaseException } from 'Modules/Common/Exceptions/BaseException';
import { ErrorCodes } from 'Modules/Common/ErrorCodes/ErrorCodes';

export class UserAlreadyExists extends BaseException {
  constructor() {super('User with provided email already exists', ErrorCodes.USER_ALREADY_EXISTS);}
}
