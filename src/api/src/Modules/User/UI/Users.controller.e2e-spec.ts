import { TestApp } from 'Test/e2e/utils/TestApp';
import { RegisterUserRequest } from 'Modules/User/UI/Requests/RegisterUser.request';
import { LoginRequest } from 'Modules/User/UI/Requests/Login.request';
import { ErrorCodes } from 'Modules/Common/ErrorCodes/ErrorCodes';
import { request } from 'Test/e2e/utils/Supertest';

describe('Users Controller', () => {
  let server: any;
  beforeAll(async () => {
    server = (await TestApp.getInstance()).getHttpServer();
  });

  afterAll(async () => {
    await TestApp.destroy();
  });

  function registerUser(data: Partial<RegisterUserRequest> = {}) {
    const payload: RegisterUserRequest = {
      email: `test+${Math.random()}@test.pl`,
      firstName: 'Tester',
      password: 'qweasdzxc',
      ...data,
    };
    return request(server)
      .post('/user/users')
      .send(payload);
  }

  function login(data: LoginRequest) {
    return request(server)
      .post('/user/tokens')
      .send(data);
  }

  describe('Registration', () => {
    test('OK', async () => {
      const { body } = await registerUser()
        .expect(201);

      expect(body.id).not.toBeUndefined();
    });
    it('Validates invalid input', async () => {
      const invalidEmail = `qwe${Math.random()}`;

      await registerUser({ email: invalidEmail })
        .expect(400);
    });
    it('Fails if email already occupied', async () => {
      const email = `qwe${Math.random()}@qwe.pl`;
      await registerUser({ email })
        .expect(201);

      const { body } =await registerUser({ email })
        .expect(400);

      expect(body.status).toBe(ErrorCodes.USER_ALREADY_EXISTS);
    });
  });
  describe('Logging', () => {
    it('OK', async () => {
      const data = { email: `qwe+${Math.random()}@qwe.pl`, password: 'zxcasdqwe' };
      await registerUser(data)
        .expect(201);

      const { body } = await login(data)
        .expect(201);

      expect(body.token).not.toBeUndefined();
    });
    it('Fails if provided invalid password', async () => {
      const data = { email: `qwe+${Math.random()}@qwe.pl`, password: 'zxcasdqwe' };
      await registerUser(data)
        .expect(201);

      const { body } = await login({ email: data.email, password: 'Invalid password' })
        .expect(400);

      expect(body.status).toBe(ErrorCodes.USER_PASSWORD_NOT_MATCH);
    });
  });
});
