import { LoginParams } from 'Modules/User/Application/Authentication.service';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class LoginRequest implements LoginParams {
  @IsEmail()
  email: string;
  @IsNotEmpty()
  password: string;
}
