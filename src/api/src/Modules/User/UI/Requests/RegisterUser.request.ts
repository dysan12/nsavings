import { RegisterUserParams } from 'Modules/User/Application/User.service';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class RegisterUserRequest implements RegisterUserParams{
  @IsEmail()
  email: string;
  @IsNotEmpty()
  password: string;
  @IsNotEmpty()
  firstName: string;
}
