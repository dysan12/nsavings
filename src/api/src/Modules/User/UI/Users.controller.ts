import { Controller, Post, Body, UseFilters, BadRequestException, Catch, ArgumentsHost } from "@nestjs/common";
import { RegisterUserRequest } from 'Modules/User/UI/Requests/RegisterUser.request';
import { LoginRequest } from 'Modules/User/UI/Requests/Login.request';
import { AuthenticationService } from 'Modules/User/Application/Authentication.service';
import { UserService } from 'Modules/User/Application/User.service';
import { UserAlreadyExists } from 'Modules/User/Domain/Exceptions/UserAlreadyExists';
import { BaseExceptionFilter } from '@nestjs/core';
import { UserPasswordNotMatch } from 'Modules/User/Application/Exceptions/UserPasswordNotMatch';
import { UserNotFound } from 'Modules/User/Application/Exceptions/UserNotFound';

@Catch()
class Filter extends BaseExceptionFilter {
  catch(exception: any, host: ArgumentsHost): any {
    let mapped;
    if (
      exception instanceof UserAlreadyExists ||
      exception instanceof UserPasswordNotMatch ||
      exception instanceof UserNotFound
    ) {
      mapped = new BadRequestException(exception);
    }
    return super.catch(mapped ?? exception, host);
  }
}

@Controller('user')
@UseFilters(Filter)
export class UsersController {
  constructor(
    private readonly authService: AuthenticationService,
    private readonly userService: UserService,
  ) {}

  @Post('users')
  async register(@Body() data: RegisterUserRequest) {
    return {
      id: await this.userService.register(data),
    };
  }

  @Post('tokens')
  async login(@Body() data: LoginRequest) {
    const { token } = await this.authService.login(data);
    return {
      token,
    };
  }
}
