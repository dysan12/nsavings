import { Injectable } from '@nestjs/common';
import { InjectCollection } from 'Modules/Mongo/InjectCollection.decorator';
import { Collection } from 'mongodb';
import { User, UserAttributes } from 'Modules/User/Domain/User.entity';
import { UserRepository } from 'Modules/User/Domain/User.repository';
import { omit } from 'lodash';

interface MongoUser extends Omit<UserAttributes, 'id'> {
  _id: string
}

@Injectable()
export class MongoUserRepository implements UserRepository {
  public constructor(
    @InjectCollection(User) private collection: Collection<MongoUser>,
  ) {}

  public async findOne(id: string): Promise<User | null> {
    const raw = await this.collection.findOne({
      _id: id,
    });
    return raw !== null ? this.toEntity(raw) : null;
  }

  public async save(user: User): Promise<void> {
    const attr = user.toAttributes();
    await this.collection.updateOne({
      _id: attr.id,
    }, {
      $set: {
        _id: attr.id,
        ...omit(attr, 'id'),
      }
    }, {
      upsert: true,
    });
  }

  private toEntity(raw: MongoUser): User {
    return User.FromAttributes({
      id: raw._id,
      ...omit(raw, '_id'),
    });
  }

  public async findOneByEmail(email: string): Promise<User | null> {
    const raw = await this.collection.findOne({
      email,
    });
    return raw !== null ? this.toEntity(raw) : null;
  }
}
