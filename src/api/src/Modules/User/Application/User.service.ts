import { Injectable, Inject } from '@nestjs/common';
import { USER_REPOSITORY, UserRepository } from 'Modules/User/Domain/User.repository';
import { User, CreateUserParams } from 'Modules/User/Domain/User.entity';
import { UserAlreadyExists } from 'Modules/User/Domain/Exceptions/UserAlreadyExists';

export interface RegisterUserParams extends CreateUserParams {}

@Injectable()
export class UserService {
  constructor(
    @Inject(USER_REPOSITORY) private readonly userRepository: UserRepository,
  ) {}

  public async register(params: RegisterUserParams): Promise<string> {
    const userAlreadyExists = await this.userRepository.findOneByEmail(params.email) !== null;
    if (userAlreadyExists) {
      throw new UserAlreadyExists();
    }
    const user = await User.Create(params);
    await this.userRepository.save(user);
    return user.getId();
  }
}
