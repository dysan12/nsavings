import { BaseException } from 'Modules/Common/Exceptions/BaseException';
import { ErrorCodes } from 'Modules/Common/ErrorCodes/ErrorCodes';

export class UserPasswordNotMatch extends BaseException {
  constructor() {
    super('Provided password does not match to the user\'s one', ErrorCodes.USER_PASSWORD_NOT_MATCH);
  }
}
