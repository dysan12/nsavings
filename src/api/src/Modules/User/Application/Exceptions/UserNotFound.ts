import { BaseException } from 'Modules/Common/Exceptions/BaseException';
import { ErrorCodes } from 'Modules/Common/ErrorCodes/ErrorCodes';

export class UserNotFound extends BaseException {
  constructor(id: any) { super(`User ${id} does not exist`, ErrorCodes.USER_NOT_FOUND); }
}
