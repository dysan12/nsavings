import { Injectable, Inject } from '@nestjs/common';
import { USER_REPOSITORY, UserRepository } from 'Modules/User/Domain/User.repository';
import { TokenPayloadService } from 'Modules/Common/JWT/TokenPayload.service';
import { JwtWrapperService } from 'Modules/Common/JWT/JwtWrapper.service';
import { UserNotFound } from 'Modules/User/Application/Exceptions/UserNotFound';
import { UserPasswordNotMatch } from 'Modules/User/Application/Exceptions/UserPasswordNotMatch';

export interface LoginParams {
  email: string;
  password: string;
}

interface LoginResult {
  token: string
}

@Injectable()
export class AuthenticationService {
  constructor(
    @Inject(USER_REPOSITORY) private readonly userRepository: UserRepository,
    private jwtService: JwtWrapperService,
  ) { }

  public async login(params: LoginParams): Promise<LoginResult> {
    const { email, password } = params;
    const user = await this.userRepository.findOneByEmail(email);
    if (null === user) {
      throw new UserNotFound(params.email);
    }
    if (false === await user.doesPasswordMatch(password)) {
      throw new UserPasswordNotMatch();
    }

    return {
      token: await this.jwtService.signAsync(TokenPayloadService.userToPayload(user)),
    };
  }
}
