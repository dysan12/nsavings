import { DraftAttributes, Draft } from 'Modules/Draft/Domain/Draft.entity';
import { DraftRepository } from 'Modules/Draft/Domain/Draft.repository';
import { InjectCollection } from 'Modules/Mongo/InjectCollection.decorator';
import { Collection, ObjectId } from 'mongodb';
import { omit } from 'lodash';

interface MongoDraft extends Omit<DraftAttributes, 'id' | 'ownerId'> {
  _id: ObjectId;
  ownerId: ObjectId;
}

export class MongoDraftRepository implements DraftRepository {
  public constructor(
    @InjectCollection(Draft) private collection: Collection<MongoDraft>,
  ) { }

  async find(id: string, ownerId: string): Promise<Draft | null> {
    const doc = await this.collection.findOne({
      _id: new ObjectId(id),
      ownerId: new ObjectId(ownerId),
    });
    return doc ? this.mapToEntity(doc) : null;
  }

  async findAllByType(type: string, ownerId: string): Promise<Draft[]> {
    const docs = await this.collection.find({
      ownerId: new ObjectId(ownerId),
      type: type,
    }).toArray();
    return docs.map(d => this.mapToEntity(d));
  }

  async save(draft: Draft): Promise<void> {
    const attr = draft.toAttributes();
    await this.collection.updateOne({
      _id: new ObjectId(attr.id),
    }, {
      $set: {
        _id: new ObjectId(attr.id),
        ownerId: new ObjectId(attr.ownerId),
        ...omit(attr, ['id', 'ownerId']),
      },
    }, {
      upsert: true,
    });
  }

  async remove(id: string, ownerId: string): Promise<void> {
    await this.collection.deleteOne({
      _id: new ObjectId(id),
      ownerId: new ObjectId(ownerId)
    });
  }

  private mapToEntity(doc: MongoDraft): Draft {
    return Draft.FromAttributes({
      ...omit(doc, ['_id', 'ownerId']),
      id: doc._id.toHexString(),
      ownerId: doc.ownerId.toHexString(),
    });
  }
}
