import { DraftRepository, DRAFT_REPOSITORY } from 'Modules/Draft/Domain/Draft.repository';
import { Inject } from '@nestjs/common';
import { CreateDraftParams, Draft, DraftAttributes, UpdateDraftParams } from 'Modules/Draft/Domain/Draft.entity';
import { DraftNotFound } from 'Modules/Draft/Domain/Exceptions/DraftNotFound';

interface UpdateParams extends UpdateDraftParams {
  id: string;
  ownerId: string;
}

export class DraftService {
  public constructor(
    @Inject(DRAFT_REPOSITORY) private readonly repository: DraftRepository,
  ) {}

  public async create(params: CreateDraftParams): Promise<DraftAttributes> {
    const draft = Draft.Create(params);
    await this.repository.save(draft);
    return draft.toAttributes();
  }

  public async findAllByType(type: string, ownerId: string): Promise<DraftAttributes[]> {
    const drafts = await this.repository.findAllByType(type, ownerId);
    return drafts.map(d => d.toAttributes());
  }

  public async update(params: UpdateParams): Promise<DraftAttributes> {
    const draft = await this.repository.find(params.id, params.ownerId);
    if (null === draft) {
      throw new DraftNotFound();
    }
    draft.update({
      name: params.name,
      data: params.data,
    });
    await this.repository.save(draft);
    return draft.toAttributes();
  }

  public async remove(id: string, ownerId: string): Promise<void> {
    await this.repository.remove(id, ownerId);
  }
}
