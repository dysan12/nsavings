import { Module } from "@nestjs/common";
import { ModuleMetadata } from "@nestjs/common/interfaces";
import { MongoModule } from "../Mongo/MongoModule";
import { DraftService } from 'Modules/Draft/Application/Draft.service';
import { DRAFT_REPOSITORY } from 'Modules/Draft/Domain/Draft.repository';
import { MongoDraftRepository } from 'Modules/Draft/Infrastructure/MongoDraft.repository';
import { Draft } from 'Modules/Draft/Domain/Draft.entity';

@Module(DraftModule.register())
export class DraftModule {
  static register(): ModuleMetadata {
    return {
      imports: [
        MongoModule.forFeature([
          {
            entity: Draft,
            name: 'drafts',
          },
        ]),
      ],
      controllers: [],
      providers: [
        {
          provide: DRAFT_REPOSITORY,
          useClass: MongoDraftRepository,
        },
        DraftService,
      ],
      exports: [
        DraftService,
      ],
    };
  }
}

