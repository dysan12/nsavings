import { Draft } from 'Modules/Draft/Domain/Draft.entity';

export const DRAFT_REPOSITORY = Symbol('draft_repository');
export interface DraftRepository {
  save(draft: Draft): Promise<void>;
  findAllByType(type: string, ownerId: string): Promise<Draft[]>;
  find(id: string, ownerId: string): Promise<Draft | null>;
  remove(id: string, ownerId: string): Promise<void>;
}
