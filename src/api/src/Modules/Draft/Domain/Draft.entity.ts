import { ObjectID } from "mongodb";

export interface CreateDraftParams {
  name: string;
  ownerId: string;
  type: string;
  data: any;
}

export interface UpdateDraftParams {
  name: string;
  data: any;
}

export interface DraftAttributes {
  id: string;
  name: string;
  ownerId: string;
  type: string;
  data: any;
}

export class Draft {
  private readonly attr: DraftAttributes;

  private constructor(attr: DraftAttributes) {
    this.attr = attr;
  }

  public static Create(params: CreateDraftParams): Draft {
    const attr: DraftAttributes = {
      id: new ObjectID().toHexString(),
      name: params.name,
      ownerId: params.ownerId,
      type: params.type,
      data: params.data,
    };
    return new this(attr);
  }

  public update(data: UpdateDraftParams): void {
    this.attr.name = data.name;
    this.attr.data = data.data;
  }

  public static FromAttributes(attr: DraftAttributes): Draft {
    return new this(attr);
  }

  public toAttributes(): DraftAttributes {
    return this.attr;
  }
}
