import { BaseException } from 'Modules/Common/Exceptions/BaseException';
import { ErrorCodes } from 'Modules/Common/ErrorCodes/ErrorCodes';

export class DraftNotFound extends BaseException {
  constructor() {super('Draft not found', ErrorCodes.DRAFT_NOT_FOUND);}
}
