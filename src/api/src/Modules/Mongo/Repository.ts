export type MongoSortSettings = {
  [k: string]: 1 | -1
}
