import { Inject } from "@nestjs/common";
import { getProviderName } from "./Collection";

export const InjectCollection = (
  entity: Function,
) => Inject(getProviderName(entity));
