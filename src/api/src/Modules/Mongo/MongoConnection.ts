import { ConfigService } from "@nestjs/config";
import { connect, Db, MongoClient } from 'mongodb';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MongoConnection {
  private client: MongoClient | undefined;
  private readonly url: string;
  private readonly defaultDbName: string;
  private readonly dbs: Array<{ [name: string]: Db }> = [];

  constructor(
    config: ConfigService,
  ) {
    this.url = config.get<string>('MONGO_URL');
    this.defaultDbName = config.get<string>('MONGO_DB_NAME');
  }

  public async getDb(name?: string): Promise<Db> {
    if (!this.client) {
      this.client = await connect(this.url, { useUnifiedTopology: true });
    }
    const dbName = name ?? this.defaultDbName;
    if (!this.dbs[dbName]) {
      this.dbs[dbName] = this.client.db(dbName ?? this.defaultDbName);
    }
    return this.dbs[dbName];
  }

  public async onModuleDestroy(): Promise<void> {
    if (this.client) {
      await this.client.close();
      this.client = undefined;
    }
  }
}
