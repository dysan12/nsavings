import { DynamicModule, Global, Module } from "@nestjs/common";
import { CollectionMeta, getCollectionName, getProviderName } from "./Collection";
import { MongoConnection } from "Modules/Mongo/MongoConnection";

@Global()
@Module({})
export class MongoModule {
  static forRoot(): DynamicModule {
    return {
      module: MongoModule,
      providers: [MongoConnection],
      exports: [MongoConnection],
    };
  }

  static forFeature(collectionMetas: CollectionMeta[]): DynamicModule {
    const providers = collectionMetas.map(meta => {
      const collectionName = meta.name ?? getCollectionName(meta.entity);
      const providerName = getProviderName(meta.entity);
      return {
        provide: providerName,
        useFactory: async (connection: MongoConnection) => {
          return (await connection.getDb()).collection(collectionName);
        },
        inject: [MongoConnection],
      };
    });
    return {
      module: MongoModule,
      providers,
      exports: providers.map(p => p.provide),
    };
  }
}

