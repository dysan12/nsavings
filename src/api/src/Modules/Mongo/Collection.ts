import { snakeCase } from 'lodash';

export interface CollectionMeta {
  entity: Function,
  name?: string
}

export function getProviderName(entity: Function): string {
  return `${entity.name}Collection`;
}

export function getCollectionName(entity: Function): string {
 return snakeCase(entity.name); 
}