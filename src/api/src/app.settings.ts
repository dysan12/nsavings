import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';
import * as compression from 'compression';

export function applyAppSettings(app: INestApplication) {
  app.useGlobalPipes(new ValidationPipe());
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  app.use(helmet());
  app.use(compression())
  app.enableCors({ origin: true });
}

