import { NestFactory } from '@nestjs/core';
import 'source-map-support/register'; // provides ts files source maps in thrown errors
import { AppModule } from 'app.module';
import { applyAppSettings } from 'app.settings';

async function bootstrap() {
  const app = await NestFactory.create(AppModule.register());
  applyAppSettings(app);
  await app.listen(3000);
}

bootstrap();
