import { Module, DynamicModule } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { AccountModule } from 'Modules/Account/Account.module';
import { ConfigModule } from '@nestjs/config';
import { MongoModule } from 'Modules/Mongo/MongoModule';
import { Type, ModuleMetadata } from '@nestjs/common/interfaces';
import { CommandModule } from 'nestjs-command';
import { MigrateLegacyDataCommand } from '../commands/migrate-legacy-data.command';
import { CommonModule } from 'Modules/Common/Common.module';
import { UserModule } from 'Modules/User/User.module';
import { DraftModule } from 'Modules/Draft/Draft.module';

const env = process.env.NODE_ENV ?? 'production';
export const ENV_PATHS = [`.env.${env}.local`, `.env.${env}`, '.env'];

@Module({})
export class AppModule {
  static registerForTests(requiredModules: Type<any>[] = []): ModuleMetadata {
    return {
      imports: [
        ConfigModule.forRoot({
          envFilePath: ENV_PATHS,
          isGlobal: true,
        }),
        MongoModule.forRoot(),
        CommonModule,
        ...requiredModules,
      ],
    };
  }

  static register(): DynamicModule {
    return {
      module: AppModule,
      imports: [
        GraphQLModule.forRoot({
          playground: env !== 'production',
          autoSchemaFile: 'schema.gql',
          context: ({ req }) => ({ req }),
        }),
        MongoModule.forRoot(),
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ENV_PATHS,
        }),
        AccountModule,
        CommandModule,
        CommonModule,
        UserModule,
        DraftModule,
      ],
      providers: [
        MigrateLegacyDataCommand, // todo move to a separate module,
      ],
    };
  }
}
