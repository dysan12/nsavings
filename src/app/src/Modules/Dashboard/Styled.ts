import { styled } from 'Modules/App/Styles/Styled';
import { devices } from 'Modules/App/Styles/Media';

export const SectionContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 20px 3px;
  @media ${devices.tablet} {
    padding: 20px 40px;
  }
`;

export const SectionBar = styled.div`
  display: flex;
  align-items: baseline;
`;

export const SectionMenu = styled.div`
  display: flex;
`

export const SectionMenuTab = styled.div<{ isActive?: boolean }>`
  font-size: 14px;
  font-weight: ${props => props.theme.fontWeight.medium};
  margin-left: 22px;
  padding: 0 3px;
  cursor: pointer;
  ${props => props.isActive ? `
    border-bottom: solid 3px ${props.theme.colors.primary};
  ` : `
    border-bottom: solid 3px rgba(0,0,0,0);
  `} 
`

export const SectionHeader = styled.span`
  margin-right: 10px;  
  font-weight: ${props => props.theme.fontWeight.regular};
  font-size: 24px;
`

export const SectionContent = styled.div`
  padding-top: 23px;
  padding-bottom: 23px;
  @media ${devices.tablet} {
    padding-right: 40px;
    padding-left: 40px;
  }
`
