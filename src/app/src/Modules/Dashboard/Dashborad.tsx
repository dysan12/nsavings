import React from 'react';
import { Panel } from 'Modules/App/Components/Panel/Panel';
import { PurposeSection } from 'Modules/Dashboard/Purpose/PurposeSection';
import { TransactionSection } from 'Modules/Dashboard/Transation/TransactionSection';

export function Dashboard() {
  return (
    <Panel>
      <PurposeSection />
      <TransactionSection />
    </Panel>
  );
}
