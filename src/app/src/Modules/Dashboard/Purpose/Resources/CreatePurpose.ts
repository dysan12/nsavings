import { gql } from '@apollo/client';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';

export namespace CreatePurpose {
  export const Mutation = gql`
  mutation CreatePurpose($name: String!){
    createPurpose(createData: {
      name: $name,
    }) {
      id,
      name,
      balance
    }
  }
`;

  export interface Request {
    name: string
  }

  export interface Response {
    createPurpose: Purpose
  }

  export type Purpose = Purposes.Purpose;
}

