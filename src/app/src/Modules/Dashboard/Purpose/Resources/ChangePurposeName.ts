import { gql } from '@apollo/client';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';

export namespace ChangePurposeName {
  export const Mutation = gql`
  mutation ChangePurposeName($id: String!, $name: String!){
    changePurposeName(data: {
      id: $id,
      name: $name
    }) {
      id,
      name,
      balance
    }
  }
`;

  export interface Request {
    id: string,
    name: string
  }

  export interface Response {
    changePurposeName: Purpose
  }

  export type Purpose = Purposes.Purpose;
}

