import { gql } from '@apollo/client';
import { ApiCommons } from 'Modules/App/Api/Commons';

export namespace Purposes {
  export const Query = gql`
  query GetPurposes($status: PurposeStatus) {
    purposes(query: { status: $status, sorting: { createdAt: DESC } } ) {
      data {
        id,
        name,
        status,
        balance,
      }
    }
  }
`;
  export interface Response {
    purposes : {
      __typename: 'PurposeListResponse',
      data: Purpose[]
    }
  }

  export interface Request {
    status: ApiCommons.PurposeStatus | null;
  }

  export interface Purpose {
    id: string;
    name: string;
    status: ApiCommons.PurposeStatus;
    balance: Record<string, number>;
    __typename: 'PurposeResponse';
  }
}

