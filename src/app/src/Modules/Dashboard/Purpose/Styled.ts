import { styled } from 'Modules/App/Styles/Styled';

export const PurposeContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  width: 130px;
  margin: 0 7px;
`;

export const PurposeChart = styled.div`
  height: 130px;
  width: 130px;
  position: relative;
  cursor: pointer;
`;

export const PurposeHeader = styled.span`
  padding-bottom: 6px;
  font-size: 13px;
  cursor: pointer;
`;

export const PurposeLabels = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  top:0;
  left: 0;
  width: 100%;
  height: 100%; 
`;

export const PurposeLabelElement = styled.span<{ isPositive?: boolean }>`
  font-size: 12px;
  font-weight: ${props => props.theme.fontWeight.regular};
  color: ${props => props.isPositive === false ? props.theme.colors.amount.negative: props.theme.colors.text.first }
`;
