import React, { useState } from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { DarkLoader } from 'Modules/App/Components/Loader';
import { styled } from 'Modules/App/Styles/Styled';
import { AddButton } from 'Modules/App/Components/AddButton';
import { SectionContainer, SectionBar, SectionHeader, SectionContent } from 'Modules/Dashboard/Styled';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';
import { PurposeOverview } from 'Modules/Dashboard/Purpose/Components/PurposeOverview';
import { PurposeStub } from 'Modules/Dashboard/Purpose/Components/PurposeStub';
import { useQuery, useMutation, FetchResult, ApolloCache } from '@apollo/client';
import { CreatePurpose } from 'Modules/Dashboard/Purpose/Resources/CreatePurpose';
import { ChangePurposeName } from 'Modules/Dashboard/Purpose/Resources/ChangePurposeName';
import { useWindowKeyUpCallback } from 'Modules/App/Utilities/useWindowKeyUpCallback';
import { snackbarErrorHandler } from 'Modules/App/Api/ApolloUtilities';
import Swiper from 'react-id-swiper';
import { useDispatch } from 'react-redux';
import { push } from "connected-react-router";
import { Routes } from 'Modules/App/Routing/Routes';
import { PurposesSummary } from 'Modules/Dashboard/Purpose/Components/PurposesSummary';
import { ApiCommons } from 'Modules/App/Api/Commons';
import { orderBy, memoize } from 'lodash';
import { PurposeStatusToggle } from 'Modules/Dashboard/Purpose/Components/PurposeStatusToggle';

function createPurposeUpdateCache(cache: ApolloCache<CreatePurpose.Response>, response: FetchResult<CreatePurpose.Response>) {
  const entry = cache.readQuery<Purposes.Response>({ query: Purposes.Query });
  cache.writeQuery({
    query: Purposes.Query,
    data: {
      ...entry,
      purposes: {
        ...entry?.purposes,
        data: [response.data?.createPurpose, ...entry?.purposes.data ?? []],
      },
    },
  });
}

function createPurposeOptimisticResponse(vars: CreatePurpose.Request) {
  return {
    createPurpose: {
      __typename: 'PurposeResponse' as const,
      id: '',
      name: vars.name,
      status: ApiCommons.PurposeStatus.Active,
      balance: {},
    },
  };
}

/**
 * Sort archived purposes to the end of the array
 */
const sortPurposes = memoize(function(purposes: Purposes.Purpose[]): Purposes.Purpose[] {
  return orderBy(purposes, 'status', 'asc');
});
const filterOutArchivedPurposes = memoize(function(purposes: Purposes.Purpose[]): Purposes.Purpose[] {
  return purposes.filter(p => p.status !== ApiCommons.PurposeStatus.Archived);
});

export function PurposeSection() {
  const { t } = useLocale('dashboard');
  const [isAddPurposeActive, setIsAddPurposeActive] = useState(false);
  const [showArchived, setShowArchived] = useState(false);
  useWindowKeyUpCallback({
    'Escape': () => setIsAddPurposeActive(false),
  });
  const dispatch = useDispatch();
  const { loading: loadingPurposes, error, data: purposesResponse } = useQuery<Purposes.Response>(Purposes.Query);
  const purposesData = purposesResponse?.purposes.data;
  const purposes = showArchived ? sortPurposes(purposesData ?? []) : filterOutArchivedPurposes(purposesData ?? []);

  const [createPurpose] = useMutation<CreatePurpose.Response, CreatePurpose.Request>(CreatePurpose.Mutation, {
    update(cache, response) {
      createPurposeUpdateCache(cache, response);
      setIsAddPurposeActive(false);
    },
    optimisticResponse: createPurposeOptimisticResponse,
    onError: snackbarErrorHandler,
  });
  const [changePurposeName] = useMutation<ChangePurposeName.Response, ChangePurposeName.Request>(ChangePurposeName.Mutation, {
    onError: snackbarErrorHandler,
  });

  async function onPurposeNameChange(purpose: Purposes.Purpose, newName: string) {
    await changePurposeName({
      variables: {
        id: purpose.id,
        name: newName,
      },
      optimisticResponse: {
        changePurposeName: {
          ...purpose,
          name: newName,
        },
      },
    });
  }

  let content;
  if (loadingPurposes) {
    content = <StyledLoader />;
  } else if (error) {
    content = <span>Error</span>;
  } else {
    const purposeComponents = [];
    if (isAddPurposeActive) {
      purposeComponents.push(<PurposeStub key={'purpose_stub'} onSubmit={(name: string) => createPurpose({ variables: { name } })} />);
    }
    purposes.forEach(p => purposeComponents.push(
      <PurposeOverview
        key={p.id}
        purpose={p}
        onNameChange={async (name: string) => onPurposeNameChange(p, name)}
        onChartClick={() => {
          dispatch(push(Routes.purposes(p.id)));
        }}
      />,
    ));
    const hasPurposes = purposeComponents.length > 0;
    content = (
      <ContentContainer>
        <SwiperContainer>
          <Swiper slidesPerView={'auto'} freeMode={true} mousewheel={true} shouldSwiperUpdate={true} noSwiping={hasPurposes === false}>
            {hasPurposes === false ? <span>{t('noPurposesAvailable')}</span> : purposeComponents}
          </Swiper>
        </SwiperContainer>
        {hasPurposes && <PurposesSummary purposes={purposes} />}
      </ContentContainer>
    );
  }

  return (
    <SectionContainer>
      <SectionBar>
        <SectionHeader>{t('purposes')}</SectionHeader>
        <AddButton onClick={() => setIsAddPurposeActive(true)} />
        <PurposeStatusToggle showArchived={showArchived} onShowArchivedChange={setShowArchived} />
      </SectionBar>
      <SectionContent>
        {content}
      </SectionContent>
    </SectionContainer>
  );

}

const StyledLoader = styled(DarkLoader)`
  height: 100px;
  width: 100px;
`;

const ContentContainer = styled.div`
`;

const SwiperContainer = styled.div`
  display: flex;
  // otherwise the swiper centers its content
  & > * {
    margin: 0;
  }
`
