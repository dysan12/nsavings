import { PieChart } from 'react-minimal-pie-chart';
import React from 'react';
import { PurposeContainer, PurposeChart } from 'Modules/Dashboard/Purpose/Styled';
import { PurposeNameInput } from 'Modules/Dashboard/Purpose/Components/PurposeNameInput';

const chartColor = '#ececec';

interface Props {
  onSubmit: (name: string) => void;
  className?: string;
}

export function PurposeStub(props: Props) {
  const { onSubmit } = props;
  return (
    <PurposeContainer className={props.className}>
      <PurposeNameInput onPressEnter={onSubmit} />
      <PurposeChart>
        <PieChart
          data={[
            { value: 1, color: chartColor },
          ]}
          lineWidth={23}
        />
      </PurposeChart>
    </PurposeContainer>
  );
}


