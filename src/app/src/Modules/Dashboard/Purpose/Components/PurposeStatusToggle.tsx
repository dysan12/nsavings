import React from 'react';
import { OptionToggle, ToggleOptionSide } from 'Modules/App/Components/OptionToggle';
import { styled } from 'Modules/App/Styles/Styled';
import { useLocale } from 'Modules/App/Locale/useLocale';

interface Props {
  showArchived: boolean;
  onShowArchivedChange: (state: boolean) => void;
}

export function PurposeStatusToggle(props: Props) {
  const { t } = useLocale('dashboard');
  return (
    <StyledOptionToggle
      left={t('showAll')}
      right={t('showActive')}
      activeOption={props.showArchived ? ToggleOptionSide.Left : ToggleOptionSide.Right}
      onActiveOptionChange={side => side === ToggleOptionSide.Left ? props.onShowArchivedChange(true) : props.onShowArchivedChange(false)}
    />
  );
}

const StyledOptionToggle = styled(OptionToggle)`
  width: 200px;
  margin-left: auto;
  margin-right: 40px;
`
