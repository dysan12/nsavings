import React, { useState, ReactElement } from 'react';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';
import { PieChart } from 'react-minimal-pie-chart';
import { DataEntry } from 'react-minimal-pie-chart/types/commonTypes';
import { useLocale, TranslateFunction, FormatAmountFunction } from 'Modules/App/Locale/useLocale';
import { PurposeContainer, PurposeChart, PurposeLabelElement, PurposeHeader, PurposeLabels } from 'Modules/Dashboard/Purpose/Styled';
import { useWindowKeyUpCallback } from 'Modules/App/Utilities/useWindowKeyUpCallback';
import { PurposeNameInput } from 'Modules/Dashboard/Purpose/Components/PurposeNameInput';
import { approximateAmount } from 'Modules/Dashboard/Purpose/CurrencyApproximator';
import { Currency } from 'Modules/App/Config';

const NO_FUNDS = 'NO_FUNDS';
const NEGATIVE_VALUE = 'NEGATIVE_VALUE';
const colorMap: Record<Currency | string, string> = {
  PLN: '#43770E',
  USD: '#daddc5',
  RUB: '#99b67b',
  AUD: '#bf8035',
  GBP: '#012169',
  EUR: '#003194',
  MXN: '#006545',
  ZAR: '#f7b21b',
  [NO_FUNDS]: '#d5d5d5',
  [NEGATIVE_VALUE]: '#CF1111',
};

function getRandomColor(): string {
  return "#" + ((1 << 24) * Math.random() | 0).toString(16);
}

function getDataColor(label: string): string {
  return colorMap[label.toUpperCase()] ?? getRandomColor();
}

function getChartData(balance: Purposes.Purpose['balance']): DataEntry[] {
  const entries = Object.entries(balance);
  const isBalanceZero = entries.reduce((prev, [_currency, value]) => prev + value, 0) === 0;
  if (!isBalanceZero) {
    return entries.map(
      ([currency, value]) => {
        return ({ title: currency, value: approximateAmount(value, currency), color: getDataColor(value >= 0 ? currency : NEGATIVE_VALUE) });
      });
  } else {
    return [{ title: 'no_funds', value: 1, color: getDataColor(NO_FUNDS) }];
  }
}

function getLabels(balance: Purposes.Purpose['balance'], t: TranslateFunction, formatAmount: FormatAmountFunction): ReactElement[] {
  const maxLabels = 4;
  const entries = Object.entries(balance);
  if (entries.length === 0) {
    return [<PurposeLabelElement key={'no_funds'}>{t('purposeWithNoFunds')}</PurposeLabelElement>];
  }
  const sortedBalance = entries.sort(([_c1, v1], [_c2, v2]) => v1 > v2 ? 0 : 1).slice(0, maxLabels);
  const labels = sortedBalance.map(([currency, value]) => {
    return <PurposeLabelElement key={currency} isPositive={value >= 0}>{formatAmount({ amount: value, currency })}</PurposeLabelElement>;
  });
  if (labels.length < entries.length) {
    labels.push(<PurposeLabelElement key={'more'}>...</PurposeLabelElement>);
  }
  return labels;
}

interface Props {
  purpose: Purposes.Purpose;
  onNameChange: (name: string) => void;
  className?: string;
  onChartClick: () => void;
}

export function PurposeOverview(props: Props) {
  const { t, formatAmount } = useLocale('dashboard');
  const [isNameEdited, setIsNameEdited] = useState(false);
  useWindowKeyUpCallback({
    'Escape': () => setIsNameEdited(false),
  });
  const { purpose, onNameChange } = props;

  const chartData = getChartData(purpose.balance);
  const labels = getLabels(purpose.balance, t, formatAmount);

  return (
    <PurposeContainer className={props.className}>
      {isNameEdited ? (<>
          <PurposeNameInput
            onPressEnter={(name) => {
              setIsNameEdited(false);
              if (name !== purpose.name) {
                onNameChange(name);
              }
            }}
            defaultValue={purpose.name}
          />
        </>
      ) : (
        <PurposeHeader onClick={() => setIsNameEdited(true)}>{purpose.name}</PurposeHeader>
      )}
      <PurposeChart onClick={props.onChartClick}>
        <PieChart
          data={chartData}
          lineWidth={23}
        />
        <PurposeLabels>
          {labels}
        </PurposeLabels>
      </PurposeChart>
    </PurposeContainer>
  );
}
