import React, { KeyboardEvent, ChangeEvent, useState } from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { Input, InputError } from 'Modules/App/Components/Form/Input';
import * as yup from 'yup';

interface Props {
  onPressEnter: (name: string) => void;
  defaultValue?: string;
}

const validationSchema = yup.object().shape({
  name: yup.string().min(1).required(),
});

export function PurposeNameInput(props: Props) {
  const [name, setName] = useState<string>(props.defaultValue ?? '');
  const [error, setError] = useState<InputError | undefined>(undefined);

  return (
    <StyledInput
      onKeyUp={async (e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
          try {
            await validationSchema.validate({ name });
            props.onPressEnter(name);
          } catch (e) {
            setError(e);
          }
        }
      }}
      onChange={async (e: ChangeEvent<HTMLInputElement>) => {
        setName(e.currentTarget.value);
        if (error && validationSchema.isValidSync({ name })) {
          setError(undefined);
        }
      }}
      error={error}
      autoFocus={true}
      errorPlacement={'top'}
      defaultValue={name}
    />
  );
}

const StyledInput = styled(Input)`
  font-size: 13px;
  padding: 3px 5px;
  margin-bottom: 6px;
  width: 130px;
  input {
    padding: 4px;
  }
`;
