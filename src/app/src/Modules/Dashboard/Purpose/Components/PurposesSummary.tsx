import React from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { styled } from 'Modules/App/Styles/Styled';
import { mergeWith } from 'lodash';

interface Purpose {
  balance: Record<string, number>
}

interface Props {
  purposes: Purpose[];
}

function getTotal(purposes: Purpose[]): [string, number][] {
  const balances = purposes.map(p => p.balance);
  const total = balances.reduce((prev, curr) => mergeWith(prev, curr, (v1: number, v2: number) => (v1 || 0) + (v2 || 0)), {});
  return Object.entries(total);
}

export function PurposesSummary(props: Props) {
  const { t, formatAmount } = useLocale('dashboard');
  const total = getTotal(props.purposes).map(([currency, amount]) => formatAmount({ amount, currency }));
  return (
    <Container>
      <Summary>
        {t('total')}
        {':  '}
        {total.length ? total.join(' - ') : ' - '}
      </Summary>
    </Container>
  );
}

const Container = styled.div`
  padding-top: 36px;
`;

const Summary = styled.span`
  font-size: 15px;
  font-weight: 500;
`;

