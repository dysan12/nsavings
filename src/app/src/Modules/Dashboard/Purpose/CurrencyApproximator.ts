/**
 * Approximate table of currency value in regard to PLN
 */
import { Currency } from 'Modules/App/Config';

const conversationTable: Record<Currency, number> = {
  USD: 3.75,
  RUB: 0.052,
  AUD: 2.68,
  GBP: 4.83,
  EUR: 4.40,
  MXN: 0.17,
  ZAR: 0.23,
  PLN: 1
}

/**
 * approximate the value of an amount
 */
export function approximateAmount(value: number, currency: string): number {
  return value * (conversationTable[currency as Currency] ?? 1);
}
