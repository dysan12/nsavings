import React, { useState, useMemo } from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { styled } from 'Modules/App/Styles/Styled';
import { SectionContainer, SectionBar, SectionHeader, SectionContent, SectionMenu, SectionMenuTab } from 'Modules/Dashboard/Styled';
import { useQuery, useMutation } from '@apollo/client';
import { Transactions } from 'Modules/Dashboard/Transation/Resources/Transactions';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';
import { TransactionTable } from './Components/TransactionTable';
import { MakeTransaction } from 'Modules/Dashboard/Transation/Resources/MakeTransaction';
import { snackbarErrorHandler } from 'Modules/App/Api/ApolloUtilities';
import { RemoveTransaction } from 'Modules/Dashboard/Transation/Resources/RemoveTransaction';
import { UpdateTransactionDraft } from 'Modules/Dashboard/Transation/Resources/TransactionDraft/UpdateTransactionDraft';
import { CreateTransactionDraft } from 'Modules/Dashboard/Transation/Resources/TransactionDraft/CreateTransactionDraft';
import { ListTransactionDrafts } from 'Modules/Dashboard/Transation/Resources/TransactionDraft/ListTransactionDrafts';
import { ApiCommons } from 'Modules/App/Api/Commons';
import { TransactionCreator } from 'Modules/Dashboard/Transation/TransactionCreator/TransactionCreator';

const perPage = 20;

enum MenuTabs {
  List,
  Creator
}

export function TransactionSection() {
  const { t } = useLocale('dashboard');
  const [transactionPage, setTransactionPage] = useState(1);
  const [activeTab, setActiveTab] = useState(MenuTabs.List);
  // purposes
  const { loading: purposesLoading, error: purposesError, data: purposesData } = useQuery<Purposes.Response, Purposes.Request>(Purposes.Query);
  // transactions
  const { loading: transactionsLoading, error: transactionsError, data: transactionsData } =
    useQuery<Transactions.Response, Transactions.Request>(Transactions.Query, { variables: { page: transactionPage, perPage } });
  const [createTransaction, { loading: createTransactionLoading }] =
    useMutation<MakeTransaction.Response, MakeTransaction.Request>(MakeTransaction.Mutation, {
      onError: snackbarErrorHandler,
      refetchQueries: ['GetTransactions', 'GetPurposes'],
    });
  const [removeTransaction] = useMutation<RemoveTransaction.Response, RemoveTransaction.Request>(RemoveTransaction.Mutation, {
    onError: snackbarErrorHandler,
    refetchQueries: ['GetTransactions', 'GetPurposes'],
  });
  //
  // drafts
  const [updateDraft, { loading: updateDraftLoading }] =
    useMutation<UpdateTransactionDraft.Response, UpdateTransactionDraft.Request>(UpdateTransactionDraft.Mutation, {
      onError: snackbarErrorHandler,
    });
  const [createDraft, { loading: createDraftLoading }] =
    useMutation<CreateTransactionDraft.Response, CreateTransactionDraft.Request>(CreateTransactionDraft.Mutation, {
      onError: snackbarErrorHandler,
      refetchQueries: ['ListTransactionDrafts'],
    });
  const { error: listDraftsError, data: draftList } = useQuery<ListTransactionDrafts.Response>(
    ListTransactionDrafts.Query);
  //
  const purposes = purposesData?.purposes;
  const activePurposes = useMemo(() => purposes?.data.filter(p => p.status === ApiCommons.PurposeStatus.Active) ?? [], [purposes]);
  const transactions = transactionsData?.transactions;

  let content;
  if (transactionsError || purposesError || listDraftsError) {
    content = <span>Error</span>;
  } else {
    content = (
      <ContentContainer>
        {activeTab === MenuTabs.List && (
          <TransactionTable
            purposes={purposes}
            transactions={transactions}
            onPageChange={(page: number) => setTransactionPage(page)}
            isLoading={transactionsLoading || purposesLoading}
            currentPage={transactionPage}
            perPage={perPage}
            onTransactionDelete={(id: string) => removeTransaction({ variables: { id } })}
          />
        )}
        {activeTab === MenuTabs.Creator && (
          <TransactionCreator
            purposes={activePurposes}
            onCreate={async (data: MakeTransaction.Request) => {
              await createTransaction({ variables: data });
            }}
            inProgress={createTransactionLoading || createDraftLoading || updateDraftLoading}
            drafts={draftList?.listTransactionDrafts.data}
            onDraftCreate={async (data) => {
              const res = await createDraft({
                variables: data,
              });
              return res?.data?.createTransactionDraft.id;
            }}
            onDraftUpdate={data => {
              updateDraft({
                variables: data,
              });
            }}
          />
        )}
      </ContentContainer>
    );
  }

  return (
    <SectionContainer>
      <SectionBar>
        <SectionHeader>{t('transactions')}</SectionHeader>
        <SectionMenu>
          <SectionMenuTab isActive={activeTab === MenuTabs.List} onClick={() => setActiveTab(MenuTabs.List)}>
            {t('list')}
          </SectionMenuTab>
          <SectionMenuTab isActive={activeTab === MenuTabs.Creator} onClick={() => setActiveTab(MenuTabs.Creator)}>
            {t('creator')}
          </SectionMenuTab>
        </SectionMenu>
      </SectionBar>
      <SectionContent>
        {content}
      </SectionContent>
    </SectionContainer>
  );

}

const ContentContainer = styled.div`
  display: flex;
`;
