import React, { useState } from 'react';
import { Transactions } from 'Modules/Dashboard/Transation/Resources/Transactions';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { styled } from 'Modules/App/Styles/Styled';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';
import { TrashIcon } from 'Modules/App/Components/Icons/TrashIcon';

type Props = {
  transactions: Transactions.Response['transactions']['data'],
  purposes: Purposes.Response['purposes']['data'],
  onTransactionDelete: (id: string) => void,
}

export function TransactionTableContent(props: Props) {
  const { t, formatAmount, formatDate } = useLocale('dashboard');
  const [activeTransaction, setActiveTransaction] = useState<string | undefined>(undefined);

  const purposes = new Map(props.purposes.map(p => [p.id, p]));
  return (
      <Table>
        <Headers>
          <DateCell>{t('date')}</DateCell>
          <DescriptionCell>{t('description')}</DescriptionCell>
          <AmountCell>{t('amount')}</AmountCell>
          <ActionCell>{t('actions')}</ActionCell>
        </Headers>
        <Rows>
          {props.transactions.map(t => {
            const amounts = t.operations.reduce<Record<string, number>>((prev, cur) => {
              return ({
                ...prev,
                [cur.currency]: (prev[cur.currency] ?? 0) + cur.amount,
              });
            }, {});
            const amountsCmps = [];
            for (const currency in amounts) {
              const value = amounts[currency];
              const label = formatAmount({ amount: value, currency });
              amountsCmps.push(
                value >= 0 ? <PositiveAmount key={currency}>{label}</PositiveAmount> : <NegativeAmount key={currency}>{label}</NegativeAmount>);
              amountsCmps.push('|');
            }
            amountsCmps.pop(); // remove the last |
            return (
              <div key={t.id}>
                <Row
                  onClick={() => setActiveTransaction(activeTransaction === t.id ? undefined : t.id)}
                  activated={activeTransaction === t.id}
                >
                  <DateCell>{formatDate(t.fulfilledAt)}</DateCell>
                  <DescriptionCell>{t.description}</DescriptionCell>
                  <AmountCell>{amountsCmps}</AmountCell>
                  <ActionCell>
                    <TrashIcon onClick={() => props.onTransactionDelete(t.id)} />
                  </ActionCell>
                </Row>
                {activeTransaction === t.id && (
                  <Operations>
                    {t.operations.map(o => {
                      return (
                        <OperationRow key={o.id}>
                          <Cell>{purposes.get(o.purposeId)?.name ?? ''}</Cell>
                          <Cell>{o.description}</Cell>
                          <Cell>
                            {o.amount >= 0 ?
                              <PositiveAmount>{formatAmount({ amount: o.amount, currency: o.currency })}</PositiveAmount> :
                              <NegativeAmount>{formatAmount({ amount: o.amount, currency: o.currency })}</NegativeAmount>
                            }
                          </Cell>
                        </OperationRow>
                      );
                    })}
                  </Operations>
                )}
              </div>
            );
          })}
        </Rows>
      </Table>
  );
}


const Table = styled.div`
  width: 100%;
`;

const GenericRow = styled.div`
  display: grid;
  grid-template-columns: minmax(50px, 19%) minmax(100px, 48%) minmax(50px, 18%) minmax(30px, 5%);
  grid-template-areas: "date description amount actions";
  justify-content: space-between;
  border-bottom: solid 1px ${props => props.theme.colors.border};
`;

const Headers = styled(GenericRow)`
  height: 37px;
`;

const Cell = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  font-weight: ${props => props.theme.fontWeight.regular};
  padding-right: 3px;
  padding-left: 3px;
`;

const DateCell = styled(Cell)`
  grid-area: date;
`;

const DescriptionCell = styled(Cell)`
  grid-area: description;
`;

const AmountCell = styled(Cell)`
  grid-area: amount;
`;

const ActionCell = styled(Cell)`
  grid-area: actions;
  height: 24px;
  svg {
    height: 18px;
    width: 18px;
  }
`;

const Rows = styled.div`

`;

const Row = styled(GenericRow)<{ activated: boolean }>`
  min-height: 30px;
  padding: 5px 0;
  background: ${props => props.activated ? props.theme.colors.background.hover : props.theme.colors.background.first};
  &:hover {
    background: ${props => props.theme.colors.background.hover};
  };
`;

const NegativeAmount = styled.span`
  color: ${props => props.theme.colors.amount.negative};
`;

const PositiveAmount = styled.span`
  color: ${props => props.theme.colors.amount.positive};
`;

const Operations = styled.div`
`;

const OperationRow = styled.div`
  display: grid;
  grid-template-columns: minmax(50px, 19%) auto minmax(50px, 19%);
  padding: 4px 0;
  border-bottom: solid 1px ${props => props.theme.colors.border};
  cursor: default;
  &:hover {
    background: ${props => props.theme.colors.background.hover};
  };
  &:last-of-type {
    border-bottom: solid 2px ${props => props.theme.colors.border};
  }
`;
