import { Transactions } from 'Modules/Dashboard/Transation/Resources/Transactions';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';
import { styled } from 'Modules/App/Styles/Styled';
import { Pagination } from 'Modules/App/Components/Table/Pagination';
import { DarkLoader } from 'Modules/App/Components/Loader';
import React from 'react';
import { TransactionTableContent } from 'Modules/Dashboard/Transation/Components/TransactionTableContent';
import { useLocale } from 'Modules/App/Locale/useLocale';

interface Props {
  onPageChange: (page: number) => void,
  transactions?: Transactions.Response['transactions'],
  purposes?: Purposes.Response['purposes'],
  currentPage: number,
  perPage: number,
  isLoading: boolean;
  onTransactionDelete: (id: string) => void;
}

export function TransactionTable(props: Props) {
  const { t } = useLocale('dashboard');
  let content;
  if (props.isLoading || props.transactions === undefined || props.purposes === undefined) {
    content = (
      <>
        <TableStub>
          <StyledLoader />
        </TableStub>
        <Pagination
          currentPage={props.currentPage}
          maxResults={props.transactions?.total ?? 0}
          perPage={props.perPage}
          onPageChange={props.onPageChange}
        />
      </>
    );
  } else if (props.transactions.total === 0) {
    content = t('noTransactionsMade');
  } else {
    content = (
      <>
        <TransactionTableContent transactions={props.transactions.data} purposes={props.purposes.data}
                                 onTransactionDelete={props.onTransactionDelete} />
        <Pagination
          currentPage={props.currentPage}
          maxResults={props.transactions?.total ?? 0}
          perPage={props.perPage}
          onPageChange={props.onPageChange}
        />
      </>
    );
  }

  return (
    <Container>
      {content}
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 100%;
`;

const TableStub = styled.div`
  width: 100%;
  height: 730px;
`;

const StyledLoader = styled(DarkLoader)`
  height: 100px;
  width: 100px;
`;
