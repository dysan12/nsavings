import { gql } from '@apollo/client';

export namespace RemoveTransaction {
  export const Mutation = gql`
  mutation RemoveTransaction($id: String!) {
    removeTransaction(id: $id) {
      status
    }
  }
`;

  export interface Request {
    id: string;
  }

  export interface Response {
    status: 'OK'
  }
}

