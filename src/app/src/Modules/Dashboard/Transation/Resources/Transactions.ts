import { gql } from '@apollo/client';

export namespace Transactions {
  export const Query = gql`
  query GetTransactions($page: PositiveInt!, $perPage: PositiveInt!) {
    transactions(query: {
      page: $page,
      perPage: $perPage,
      sortBy: {
        fulfilledAt: DESC
      }
    }) {
      total,
      pagination {
        page,
        perPage
      },
      data {
        id,
        operations {
          id,
          amount,
          currency,
          description,
          purposeId
        },
        description,
        fulfilledAt
      }
    }
  }
`;

  export interface Request {
    page: number,
    perPage: number
  }

  export interface Response {
    transactions: {
      __typename: 'ListTransactionResponse',
      total: number,
      pagination: Pagination,
      data: Transaction[]
    }
  }

  export interface Transaction {
    id: string;
    operations: Operation[],
    description: string,
    fulfilledAt: string,
    __typename: 'TransactionResponse',
  }

  export interface Operation {
    id: string,
    purposeId: string,
    amount: number,
    currency: string,
    description: string,
    __typename: 'OperationResponse',
  }

  interface Pagination {
    page: number,
    perPage: number
  }
}

