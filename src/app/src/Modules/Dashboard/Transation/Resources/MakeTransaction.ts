import { gql } from '@apollo/client';
import { Transactions } from 'Modules/Dashboard/Transation/Resources/Transactions';

export namespace MakeTransaction {
  export const Mutation = gql`
  mutation MakeTransaction($description: String!, $fulfilledAt: DateTime!, $operations: [OperationData!]!) {
    makeTransaction(createData: {
      description: $description,
      operations: $operations
      fulfilledAt: $fulfilledAt
    }) {
      id,
      operations {
        id,
        amount,
        currency,
        description,
        purposeId
      },
      description,
      fulfilledAt
    }
  }
`;

  export interface Request {
    description: string;
    fulfilledAt: Date,
    operations: {
      purposeId: string;
      amount: number;
      currency: string;
      description: string;
    }[]
  }

  export interface Response {
    makeTransaction: Transactions.Transaction
  }
}

