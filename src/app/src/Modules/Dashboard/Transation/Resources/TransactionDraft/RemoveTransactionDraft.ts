import { gql } from '@apollo/client';

export namespace RemoveTransactionDraft {
  export const Mutation = gql`
  mutation RemoveTransactionDraft($id: String!) {
    removeTransactionDraft(id: $id) {
      status
    }
  }
`;

  export interface Request {
    removeTransactionDraft: {
      id: string;
    }
  }

  export interface Response {
    status: string;
  }
}
