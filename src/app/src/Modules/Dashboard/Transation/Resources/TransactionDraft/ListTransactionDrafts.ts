import { gql } from '@apollo/client';

export namespace ListTransactionDrafts {
  export const Query = gql`
  query ListTransactionDrafts {
    listTransactionDrafts {
      data {
        id,
        name,
        data {
          description,
          operations {
            purposeId,
            amount,
            currency,
            description
          }
        }
      }
    }
  }
`;

  export interface Response {
    listTransactionDrafts: {
      __typename: 'TransactionDraftListResponse',
      data: TransactionDraft[]
    }
  }

  export interface TransactionDraft {
    id: string;
    name: string;
    data: Data;
    __typename: 'TransactionDraftResponse',
  }

  export interface Data {
    description?: string;
    operations?: DataOperation[]
  }

  export interface DataOperation {
    purposeId?: string;
    amount?: number;
    currency?: string;
    description?: string;
  }
}

