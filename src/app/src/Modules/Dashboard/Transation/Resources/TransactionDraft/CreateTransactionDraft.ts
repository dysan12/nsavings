import { gql } from '@apollo/client';

export namespace CreateTransactionDraft {
  export const Mutation = gql`
  mutation CreateTransactionDraft($name: String!, $data: TransactionDraftData!) {
    createTransactionDraft(data: {
      name: $name,
      data: $data
    }) {
      id,
      name,
      data {
        description,
        operations {
          purposeId,
          amount,
          currency,
          description
        }
      }
    }
  }
`;

  export interface Request {
    name: string;
    data: Data;
  }

  export interface Response {
    createTransactionDraft: Draft
  }

  interface Draft {
    id: string;
    name: string;
    data: Data;
  }

  interface Data {
    description?: string;
    operations?: {
      purposeId?: string;
      amount?: number;
      currency?: string;
      description?: string;
    }[]
  }
}

