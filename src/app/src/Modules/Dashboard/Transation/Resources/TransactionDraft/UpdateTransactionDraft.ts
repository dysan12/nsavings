import { gql } from '@apollo/client';

export namespace UpdateTransactionDraft {
  export const Mutation = gql`
  mutation UpdateTransactionDraft($name: String!, $data: TransactionDraftData!, $id: String!) {
    updateTransactionDraft(data: {
      id: $id,
      name: $name,
      data: $data
    }) {
      id,
      name,
      data {
        description,
        operations {
          purposeId,
          amount,
          currency,
          description
        }
      }
    }
  }
`;

  export interface Request {
    id: string;
    name: string;
    data: Data;
  }

  export interface Response {
    updateTransactionDraft: Draft
  }

  interface Draft {
    id: string;
    name: string;
    data: Data;
  }

  interface Data {
    description?: string;
    operations?: {
      purposeId?: string;
      amount?: number;
      currency?: string;
      description?: string;
    }[]
  }
}
