import React, { useMemo } from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { styled } from 'Modules/App/Styles/Styled';
import { ControlledSelectInput } from 'Modules/App/Components/Form/SelectInput';
import { Input } from 'Modules/App/Components/Form/Input';
import { nth, values } from 'lodash';
import { Currency } from 'Modules/App/Config';
import { OperationsSummary } from 'Modules/Dashboard/Transation/TransactionCreator/Components/OperationsSummary';
import { CreatorState } from 'Modules/Dashboard/Transation/TransactionCreator/Slice';

type Operation = CreatorState.Operation;
type Errors = CreatorState.Errors['operations'];

interface Props {
  operations: Operation[],
  purposes: {
    name: string,
    id: string
  }[],
  changeOperation: (operation: Operation) => void,
  errors: Errors,
}

export function FormOperations(props: Props) {
  const { purposes, operations, errors, changeOperation } = props;
  const { t } = useLocale('dashboard');
  const purposeItems = useMemo(() => purposes.map(p => ({ label: p.name, value: p.id })) ?? [], [purposes]);
  const currenciesItems = useMemo(() => values(Currency).map(c => ({ label: c, value: c })), []);

  return (
    <Container>
      {operations.map((o, i) => (
        <OperationDetails key={o.id}>
          <ControlledSelectInput
            name={`operations[${i}].purposeId`}
            items={purposeItems}
            label={t('transactionCreator.purpose')}
            value={o.purposeId}
            error={nth(errors, i)?.purposeId}
            listUnselectItem={true}
            unselectLabel={`---${t('purpose')}---`}
            onSelect={({ value }) => changeOperation({ ...o, purposeId: value })}
          />
          <Input
            name={`operations[${i}].amount`}
            type={'number'}
            label={t('transactionCreator.amount')}
            error={nth(errors, i)?.amount}
            value={o.amount}
            onChange={e => changeOperation({ ...o, amount: e.currentTarget.value })}
          />
          <ControlledSelectInput
            name={`operations[${i}].currency`}
            items={currenciesItems}
            value={o.currency}
            label={t('transactionCreator.currency')}
            onSelect={({ value }) => changeOperation({ ...o, currency: value })}
          />
          <Input
            name={`operations[${i}].description`}
            label={t('transactionCreator.description')}
            error={nth(errors, i)?.description}
            value={o.description}
            onChange={e => changeOperation({ ...o, description: e.currentTarget.value })}
          />
        </OperationDetails>
      ))}
      <OperationsSummary operations={operations} />
    </Container>
  );
}

const Container = styled.div`

`;

const OperationDetails = styled.div`
  display: grid;
  grid-template-columns: 150px 100px 80px auto;
  grid-gap: 10px;
`;
