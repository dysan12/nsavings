import { useLocale } from 'Modules/App/Locale/useLocale';
import { groupBy, sumBy, compact } from 'lodash';
import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';

export function OperationsSummary(props: { operations: { amount: string, currency: string }[] }) {
  const { formatAmount } = useLocale('dashboard');

  const totalByCurrency = Object.entries(groupBy(props.operations, 'currency'));
  const total = compact(totalByCurrency.map(([currency, operations]) => {
    const amount = sumBy(operations, o => parseFloat(o.amount));
    if (amount === 0) {
      return undefined;
    }

    return formatAmount({
      currency,
      amount,
      isCentesimal: false,
    });
  }));
  return (
    <Container>
      {total.join(' | ')}
    </Container>
  );
}

const Container = styled.div`
  position: absolute;
`
