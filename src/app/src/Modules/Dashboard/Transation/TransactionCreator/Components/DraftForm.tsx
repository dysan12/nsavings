import React, { useState, useMemo, useEffect } from 'react';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { styled } from 'Modules/App/Styles/Styled';
import { SelectInput } from 'Modules/App/Components/Form/SelectInput';
import { Input } from 'Modules/App/Components/Form/Input';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { yupResolver } from '@hookform/resolvers';

export type DraftFormData = {
  draftName: string;
  draftId?: string;
}
const schema = yup.object().shape<DraftFormData>({
  draftName: yup.string().min(2).required(),
  draftId: yup.string(),
});

interface Draft {
  id: string;
  name: string;
}

interface Props {
  drafts: Draft[];
  onDraftChange: (id?: string) => void;
}

export function useDraftForm(props: Props) {
  const df = useForm<DraftFormData>({ resolver: yupResolver(schema) });
  // useForm returns every time a new object, but the reset function is memoized so we can safely use it in useEffect deps array
  const { reset: resetDf } = df;
  const { t } = useLocale('dashboard');
  const [loadedDraftId, setLoadedDraftId] = useState<string | undefined>(undefined);
  const { drafts, onDraftChange } = props;
  const draftItems = useMemo(() => drafts.map(d => ({ label: d.name, value: d.id })), [drafts]);
  const loadedDraft = useMemo(() => drafts.find(d => d.id === loadedDraftId), [drafts, loadedDraftId]);

  useEffect(() => {
    resetDf({
      draftId: loadedDraft?.id ?? '',
      draftName: loadedDraft?.name ?? ''
    });
    onDraftChange(loadedDraftId);
  }, [loadedDraft, onDraftChange, resetDf, loadedDraftId]);

  return {
    form: (
      <>
        <DraftSelect
          items={draftItems}
          defaultValue={loadedDraft?.id}
          label={t('draft')}
          name={'draftId'}
          ref={df.register}
          listUnselectItem={true}
          unselectLabel={`---${t('newDraft')}---`}
          onSelect={item => {
            const draft = props.drafts?.find(d => d.id === item.value);
            if (draft) {
              if (draft.id !== loadedDraftId) {
                setLoadedDraftId(draft.id);
                props.onDraftChange(draft.id);
              }
            } else {
              setLoadedDraftId(undefined);
            }
          }}
        />
        <DraftName
          label={t('draftName')}
          name={'draftName'}
          ref={df.register}
          error={df.errors.draftName}
          defaultValue={loadedDraft?.name}
        />
      </>
    ),
    submit: async (submitCallback: (data: DraftFormData) => Promise<string | undefined>) => {
      const isValid = await df.trigger();
      if (isValid === false) {
        return;
      }
      const draft = df.getValues();
      const id = await submitCallback(draft);
      if (id) {
        setLoadedDraftId(id);
      }
    },
  };
}

const DraftSelect = styled(SelectInput)`
  grid-area: draftSelect;
`;
const DraftName = styled(Input)`
  grid-area: draftName;
  width: 160px;
`;
