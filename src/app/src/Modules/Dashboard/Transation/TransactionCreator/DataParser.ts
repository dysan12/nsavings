import * as yup from 'yup';
import { ValidationError } from 'yup';
import { set } from 'lodash';
import { CreatorState } from 'Modules/Dashboard/Transation/TransactionCreator/Slice';

export namespace CreatorDataParser {
  type ParsedData = {
    fulfilledAt: Date,
    description: string,
    operations: {
      purposeId: string,
      amount: number,
      currency: string,
      description: string
    }[]
  }
  const schema = yup.object().shape({
    fulfilledAt: yup.date().required(),
    description: yup.string().required(),
    operations: yup.array(yup.object({
        purposeId: yup.string().required(),
        amount: yup.number().notOneOf([0]).required(),
        currency: yup.string().required(),
        description: yup.string().default(''),
      }).required(),
    ).required(),
  });

  type Result = { isValid: true, data: ParsedData } | { isValid: false, errors: CreatorState.Errors };

  export function parse(data: CreatorState.Form): Result {
    try {
      const result = schema.validateSync(data, { abortEarly: false, stripUnknown: true });
      return {
        isValid: true, data: {
          ...result,
          operations: (result?.operations ?? []).map(o => ({ ...o, amount: o.amount * 100 })),
        } as ParsedData,
      };
    } catch (e) {
      if (e instanceof ValidationError) {
        return {
          isValid: false,
          errors: e.inner.reduce((prev, curr) => set(prev, curr.path, { type: curr.type }), {}),
        };
      }
      throw e;
    }
  }
}

