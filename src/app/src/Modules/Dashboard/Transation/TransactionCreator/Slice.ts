import { Currency } from 'Modules/App/Config';
import { dropRight } from 'lodash';
import { InputError } from 'Modules/App/Components/Form/Input';
import { createSlice } from '@reduxjs/toolkit';
import { PA } from 'Modules/App/Redux/Common';

export namespace CreatorState {
  export interface Operation {
    id: string | number,
    description: string,
    purposeId: string,
    amount: string,
    currency: string,
  }

  export function createFormOperation(index: number, operation?: Omit<Partial<Operation>, 'id'>): Operation {
    return {
      description: '',
      purposeId: '',
      amount: '0',
      currency: Currency.PLN,
      ...operation,
      id: `def-${index}`,
    };
  }

  export type Form = {
    description: string,
    fulfilledAt: string,
    operations: Operation[]
  };
  export type Errors = {
    description?: InputError,
    fulfilledAt?: InputError,
    // preserve the order
    operations?: (undefined | Record<keyof Operation, InputError>)[],
  }
  export const initState = {
    form: {
      description: '',
      fulfilledAt: (new Date()).toISOString().split('T')[0],
      operations: [createFormOperation(0)] as Operation[],
    } as Form,
    errors: {} as Errors,
  };

  const slice = createSlice({
    name: 'TransactionCreator',
    initialState: initState,
    reducers: {
      updateForm: (state, { payload }: PA<{ description?: string, fulfilledAt?: string }>) => {
        state.form.description = payload.description ?? state.form.description;
        state.form.fulfilledAt = payload.fulfilledAt ?? state.form.fulfilledAt;
      },
      resetForm: (state) => {
        state.form = initState.form;
        state.errors = initState.errors;
      },
      appendOperation: (state) => {
        state.form.operations.push(createFormOperation(state.form.operations.length));
      },
      removeOperation: (state) => {
        state.form.operations = dropRight(state.form.operations);
      },
      updateOperation: (state, { payload }: PA<Partial<Operation>>) => {
        state.form.operations = state.form.operations.map(o => o.id === payload.id ? { ...o, ...payload } : o);
      },
      setErrors: (state, { payload }: PA<Errors>) => {
        state.errors = payload;
      },
      replaceOperations: (state, { payload }: PA<Operation[]>) => {
        state.form.operations = payload.length > 0 ? payload : initState.form.operations;
      },
    },
  });
  export const reducer = slice.reducer;
  export const actions = slice.actions;
}
