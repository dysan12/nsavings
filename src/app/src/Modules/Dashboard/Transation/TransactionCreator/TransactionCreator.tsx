import React, { useReducer, useCallback } from 'react';
import { Purposes } from 'Modules/Dashboard/Purpose/Resources/Purposes';
import { MakeTransaction } from 'Modules/Dashboard/Transation/Resources/MakeTransaction';
import { ListTransactionDrafts } from 'Modules/Dashboard/Transation/Resources/TransactionDraft/ListTransactionDrafts';
import { CreateTransactionDraft } from 'Modules/Dashboard/Transation/Resources/TransactionDraft/CreateTransactionDraft';
import { UpdateTransactionDraft } from 'Modules/Dashboard/Transation/Resources/TransactionDraft/UpdateTransactionDraft';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { CreatorState } from 'Modules/Dashboard/Transation/TransactionCreator/Slice';
import { AddButton } from 'Modules/App/Components/AddButton';
import { RemoveButton } from 'Modules/App/Components/RemoveButton';
import { FormOperations } from 'Modules/Dashboard/Transation/TransactionCreator/Components/FormOperations';
import { styled } from 'Modules/App/Styles/Styled';
import { Input } from 'Modules/App/Components/Form/Input';
import { PrimaryButton } from 'Modules/App/Components/Button';
import { CreatorDataParser } from 'Modules/Dashboard/Transation/TransactionCreator/DataParser';
import { Form } from 'Modules/App/Components/Form/Form';
import { useDraftForm, DraftFormData } from 'Modules/Dashboard/Transation/TransactionCreator/Components/DraftForm';
import { DarkLoader } from 'Modules/App/Components/Loader';
import { omitBy, omit } from 'lodash';

interface Props {
  purposes: Purposes.Purpose[];
  onCreate: (data: MakeTransaction.Request) => void;
  inProgress: boolean;
  drafts: ListTransactionDrafts.Response['listTransactionDrafts']['data'] | undefined;
  onDraftCreate: (data: CreateTransactionDraft.Request) => Promise<string | undefined>;
  onDraftUpdate: (data: UpdateTransactionDraft.Request) => void;
}

export function TransactionCreator(props: Props) {
  const { t, formatAmount } = useLocale('dashboard');
  const [state, dispatch] = useReducer(CreatorState.reducer, CreatorState.initState);
  const { form, errors } = state;
  const { drafts, purposes } = props;
  const onDraft = {
    change: useCallback((id) => {
      const draft = drafts?.find(d => d.id === id);
      dispatch(CreatorState.actions.resetForm());
      if (draft) {
        const operations = draft.data.operations?.map(
          (o, index) => CreatorState.createFormOperation(index, {
            description: o.description ?? '',
            purposeId: o.purposeId ?? '',
            currency: o.currency ?? '',
            amount: formatAmount({ amount: o.amount ?? 0 }),
          }),
        );
        dispatch(CreatorState.actions.replaceOperations(operations ?? []));
        dispatch(CreatorState.actions.updateForm(draft.data));
      }
    }, [drafts, formatAmount, dispatch]),
    create: async (data: DraftFormData) => {
      return await props.onDraftCreate({
        name: data.draftName,
        data: omitBy({
          description: form.description,
          operations: form.operations.map(o => ({ ...omit(o, 'id'), amount: parseFloat(o.amount) * 100 })),
        }, v => !v || v.toString().length === 0),
      });
    },
    update: async (data: DraftFormData) => {
      if (!data.draftId) {
        return;
      }
      await props.onDraftUpdate({
        id: data.draftId,
        name: data.draftName,
        data: omitBy({
          description: form.description,
          operations: form.operations.map(o => ({ ...omit(o, 'id'), amount: parseFloat(o.amount) * 100 })),
        }, v => !v || v.toString().length === 0),
      });
      return data.draftId;
    },
  };
  const { form: draftForm, submit: submitDraftForm } = useDraftForm({ drafts: props.drafts ?? [], onDraftChange: onDraft.change });
  if (drafts === undefined || props.inProgress) {
    return <DarkLoader />;
  }
  const submitDraft = async () => {
    await submitDraftForm(async (data) => {
      if (!data.draftId) {
        return await onDraft.create(data);
      } else {
        return await onDraft.update(data);
      }
    });
  };

  return (
    <Container>
      <StyledForm onSubmit={() => {
        const result = CreatorDataParser.parse(form);
        if (result.isValid === false) {
          dispatch(CreatorState.actions.setErrors(result.errors));
          return;
        }
        dispatch(CreatorState.actions.resetForm());
        props.onCreate(result.data);
      }}>
        <TransactionDetails>
          {draftForm}
          <DateInput
            name={'fulfilledAt'}
            label={t('transactionCreator.date')}
            value={form.fulfilledAt}
            onChange={e => dispatch(CreatorState.actions.updateForm({ fulfilledAt: e.currentTarget.value }))}
            type={'date'}
            error={errors.fulfilledAt}
          />
          <DescriptionInput
            name={'description'}
            label={t('transactionCreator.description')}
            onChange={e => dispatch(CreatorState.actions.updateForm({ description: e.currentTarget.value }))}
            value={form.description}
            error={errors.description}
          />
        </TransactionDetails>
        <OperationList>
          <HeaderContainer>
            <Header>{t('transactionCreator.operations')}</Header>
            <HeaderButtons>
              <AddButton onClick={() => dispatch(CreatorState.actions.appendOperation())} />
              <RemoveButton onClick={() => dispatch(CreatorState.actions.removeOperation())} />
            </HeaderButtons>
          </HeaderContainer>
          <FormOperations
            operations={form.operations}
            changeOperation={o => dispatch(CreatorState.actions.updateOperation(o))}
            purposes={purposes}
            errors={errors.operations ?? []}
          />
        </OperationList>
        <SubmitSection>
          <SubmitButton inProgress={props.inProgress} onClick={submitDraft}>
            {t('saveDraft')}
          </SubmitButton>
          <SubmitButton inProgress={props.inProgress} type="submit">{t('create')}</SubmitButton>
        </SubmitSection>
      </StyledForm>
    </Container>
  );
}

const Container = styled.div`
  width: 100%;
  position: relative;
`;

const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
const TransactionDetails = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-template-columns: 160px 160px auto;
  grid-template-rows: auto;
  grid-template-areas: 
    "draftSelect draftName ."
    "date desc desc"
`;
const DateInput = styled(Input)`
  grid-area: date;
  input {
    padding: 11.5px 15px;  
  }
`;
const DescriptionInput = styled(Input)`
  grid-area: desc;
`;

const HeaderContainer = styled.div`
  display: flex;
  margin: 10px 0;
`;
const Header = styled.div`
  font-size: 15px;
  font-weight: ${props => props.theme.fontWeight.medium};
`;
const HeaderButtons = styled.div`
  margin-left: 8px;
  display: flex;
  & > * {
    margin: 0 2px;
  }
`;
const OperationList = styled.div``;

const SubmitButton = styled(PrimaryButton)`
  width: 150px;
  height: 35px;
  align-self: flex-end;
`;

const SubmitSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  & > * {
    margin-left: 5px;
  }
`;
