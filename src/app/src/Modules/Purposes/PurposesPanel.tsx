import React, { useState } from 'react';
import { Panel } from 'Modules/App/Components/Panel/Panel';
import { useParams } from 'react-router-dom';
import { useQuery, useMutation } from '@apollo/client';
import { DarkLoader } from 'Modules/App/Components/Loader';
import { PurposeList } from 'Modules/Purposes/Resources/PurposeList';
import { Purpose } from 'Modules/Purposes/Resources/Purpose';
import { PurposeFilters, Filters, OperationType, ALL_YEARS } from 'Modules/Purposes/Components/PurposeFilters';
import { PurposeTable } from 'Modules/Purposes/Components/Table/PurposeTable';
import { PurposeSummary } from 'Modules/Purposes/Components/PurposeSummary';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import { Routes } from 'Modules/App/Routing/Routes';
import { groupBy } from 'lodash';
import { styled } from 'Modules/App/Styles/Styled';
import { NavigateArrowDownIcon } from 'Modules/App/Components/Icons/NavigateArrowDownIcon';
import { StatusSwitcher } from 'Modules/Purposes/Components/StatusSwitcher';
import { ChangePurposeStatus } from 'Modules/Purposes/Resources/ChangePurposeStatus';
import { snackbarErrorHandler } from 'Modules/App/Api/ApolloUtilities';
import { ApiCommons } from 'Modules/App/Api/Commons';

function getOperationsYears(operations: Purpose.Response['purpose']['operations']): string[] {
  return Object.keys(groupBy(operations, o => new Date(o.fulfilledAt).getFullYear().toString()));
}

function applyFilters(operations: Purpose.Response['purpose']['operations'], filters: Filters) {
  let filtered = operations;
  if (filters.year !== ALL_YEARS) {
    filtered = filtered.filter(operation => new Date(operation.fulfilledAt).getFullYear().toString() === filters.year);
  }
  if (filters.type === OperationType.Expenses) {
    filtered = filtered.filter(operation => operation.amount < 0);
  }
  if (filters.type === OperationType.Incomes) {
    filtered = filtered.filter(operation => operation.amount >= 0);
  }
  return filtered;
}

export function PurposesPanel() {
  const { purposeId } = useParams();
  const dispatch = useDispatch();

  const { error: purposeListError, loading: purposeListLoading, data: purposeListData } =
    useQuery<PurposeList.Response>(PurposeList.Query);
  const { error: purposeError, loading: purposeLoading, data: purposeData } =
    useQuery<Purpose.Response, Purpose.Request>(Purpose.Query, { variables: { id: purposeId }, fetchPolicy: 'network-only' });

  const [changePurposeStatus, { loading: changePurposeStatusLoading }] = useMutation<ChangePurposeStatus.Response, ChangePurposeStatus.Request>(
    ChangePurposeStatus.Mutation, {
      onError: snackbarErrorHandler,
    });

  const [filters, setFilters] = useState<Filters>({
    purposeId,
    type: OperationType.All,
    year: ALL_YEARS,
  });
  return (
    <Panel>
      <Section>
        {purposeListLoading || purposeListError ?
          <DarkLoader /> :
          (<BarContainer>
            <NavigateArrowDownIcon onClick={() => dispatch(push(Routes.dashboard()))} />
            <PurposeFilters
              operationYears={getOperationsYears(purposeData?.purpose.operations ?? [])}
              purposes={purposeListData!.purposes.data}
              filters={filters}
              onChange={(newFilters) => {
                if (newFilters.purposeId !== filters.purposeId) {
                  dispatch(push(Routes.purposes(newFilters.purposeId)));
                  setFilters({ ...newFilters, year: ALL_YEARS });
                } else {
                  setFilters(newFilters);
                }
              }}
            />
          </BarContainer>)
        }
        {purposeLoading || purposeError ?
          <DarkLoader /> : (
            <ContentContainer>
              <PurposeTable operations={applyFilters(purposeData!.purpose.operations, filters)} />
              <PurposeAside>
                <PurposeSummary operations={applyFilters(purposeData!.purpose.operations, filters)} />
                <StatusSwitcher
                  status={purposeData!.purpose.status}
                  onActive={() => changePurposeStatus({
                    variables: {
                      id: purposeData!.purpose.id,
                      status: ApiCommons.PurposeStatus.Active,
                    },
                  })}
                  onArchive={() => changePurposeStatus({
                    variables: {
                      id: purposeData!.purpose.id,
                      status: ApiCommons.PurposeStatus.Archived,
                    },
                  })}
                  inProgress={changePurposeStatusLoading}
                />
              </PurposeAside>
            </ContentContainer>
          )
        }
      </Section>
    </Panel>
  );
}

const Section = styled.section`
  padding: 20px 40px;
`;
const BarContainer = styled.div`
  display: grid;
  grid-template-columns: 70px auto;
`;

const ContentContainer = styled.div`
  display: grid;
  grid-template-columns: auto 300px;
  align-items: flex-start;
`;

const PurposeAside = styled.div`
  padding: 0 40px;
  & > * {
    padding-bottom: 15px;
  }
`;
