import React from 'react';
import { OperationRowData, OperationRow } from 'Modules/Purposes/Components/Table/OperationRow';
import { styled } from 'Modules/App/Styles/Styled';
import { PurposeTableHeader } from 'Modules/Purposes/Components/Table/PurposeTableHeader';

interface Props {
  operations: OperationRowData[];
}

export function PurposeTable(props: Props) {
  return (
    <Table>
      <Header>
        <PurposeTableHeader />
      </Header>
      <Body>
        {props.operations.map(o => <OperationRow key={o.id} operation={o} />)}
      </Body>
    </Table>
  );
}

const Table = styled.table`
  border-collapse: collapse;
  tr {
    display: grid;
    grid-template-columns: 130px 130px auto;
  }
  tr:not(:last-of-type), & > *:not(:last-child) {
    border-bottom: solid 1px ${props => props.theme.colors.border};
  }
  tr:not(:first-of-type):hover {
    background: ${props => props.theme.colors.background.hover};
  }
  th, td {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 4px 7px;
  }
`;
const Header = styled.thead``;
const Body = styled.tbody``;
