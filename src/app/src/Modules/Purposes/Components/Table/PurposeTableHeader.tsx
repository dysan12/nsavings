import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { useLocale } from 'Modules/App/Locale/useLocale';

export function PurposeTableHeader() {
  const { t } = useLocale('purposes.table');
  return (
    <HeaderRow>
      <Header>{t('date')}</Header>
      <Header>{t('amount')}</Header>
      <Header>{t('description')}</Header>
    </HeaderRow>
  );
}

const HeaderRow = styled.tr`
`

const Header = styled.th`

`
