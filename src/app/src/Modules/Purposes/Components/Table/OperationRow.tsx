import React from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { styled } from 'Modules/App/Styles/Styled';

export type OperationRowData = {
  id: string;
  fulfilledAt: string,
  amount: number,
  currency: string,
  description: string,
};

interface Props {
  operation: OperationRowData;
}

export function OperationRow(props: Props) {
  const { formatDate, formatAmount } = useLocale();
  const { operation } = props;
  return (
    <Row>
      <Cell>{formatDate(operation.fulfilledAt)}</Cell>
      <Cell>
        {operation.amount >= 0 ?
          <PositiveAmount>{formatAmount(operation)}</PositiveAmount> :
          <NegativeAmount>{formatAmount(operation)}</NegativeAmount>
        }
      </Cell>
      <Cell>{operation.description}</Cell>
    </Row>
  );
}

const Row = styled.tr``;
const Cell = styled.td``;

const NegativeAmount = styled.span`
  color: ${props => props.theme.colors.amount.negative};
`;

const PositiveAmount = styled.span`
  color: ${props => props.theme.colors.amount.positive};
`;
