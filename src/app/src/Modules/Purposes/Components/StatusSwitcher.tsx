import React from 'react';
import { SecondaryButton } from 'Modules/App/Components/Button';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { styled } from 'Modules/App/Styles/Styled';

interface Props {
  status: 'Active' | 'Archived';
  onArchive: VoidFunction;
  onActive: VoidFunction;
  inProgress: boolean;
}

export function StatusSwitcher(props: Props) {
  const { t } = useLocale('purposes');
  return (
    <Container>
      {props.status === 'Active' && <SwitchButton inProgress={props.inProgress} onClick={props.onArchive}>{t('archive')}</SwitchButton>}
      {props.status === 'Archived' && <SwitchButton inProgress={props.inProgress} onClick={props.onActive}>{t('active')}</SwitchButton>}
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  justify-content: right;
`

const SwitchButton = styled(SecondaryButton)`
  height: 35px;
  width: 100%;
`
