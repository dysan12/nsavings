import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { groupBy, sumBy } from 'lodash';

function getSummary(operations: Operation[]): Array<{ currency: string, amount: number }> {
  const grouped = groupBy(operations, 'currency');
  const entries = Object.entries(grouped);
  return entries.map(group => ({
    currency: group[0],
    amount: sumBy(group[1], 'amount'),
  }));
}

type Operation = { amount: number, currency: string };

interface Props {
  operations: Operation[];
}

export function PurposeSummary(props: Props) {
  const { t, formatAmount } = useLocale('purposes.summary');
  const expenses = props.operations.filter(o => o.amount < 0);
  const incomes = props.operations.filter(o => o.amount >= 0);

  const expensesSummary = getSummary(expenses);
  const incomesSummary = getSummary(incomes);

  return (
    <SummaryContainer>
      <div>
        <Row>
          {t('expenses')}
        </Row>
        <SummaryList>
          {expensesSummary.length ?
            expensesSummary.map(group => (<Expense key={group.currency}>{formatAmount(group)}</Expense>)) :
            <span>0</span>
          }
        </SummaryList>
      </div>
      <div>
        <Row>
          {t('incomes')}
        </Row>
        <SummaryList>
          {incomesSummary.length ?
            incomesSummary.map(group => (<Income key={group.currency}>{formatAmount(group)}</Income>)) :
            <span>0</span>
          }
        </SummaryList>
      </div>
    </SummaryContainer>
  );
}

const SummaryContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const Row = styled.div`
  &:not(:last-of-type) {
    border-bottom: 1px solid ${props => props.theme.colors.border};
  }
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 3px 30px;
`;

const Expense = styled.div`
  color: ${props => props.theme.colors.amount.negative};
`;

const Income = styled.div`
  color: ${props => props.theme.colors.amount.positive};
`;

const SummaryList = styled(Row)`
  flex-direction: column;
  padding: 4px 10px;
`;
