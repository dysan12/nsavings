import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { SelectInput } from 'Modules/App/Components/Form/SelectInput';
import { useLocale } from 'Modules/App/Locale/useLocale';

export enum OperationType {
  Incomes = 'incomes',
  Expenses = 'expenses',
  All = 'all'
}

export const ALL_YEARS = 'all';
export type Filters = {
  purposeId: string,
  year: string,
  type: OperationType
};

interface Props {
  // years that active purpose operations were fulfilled
  operationYears: string[],
  purposes: { id: string, name: string }[]
  filters: Filters;
  onChange: (filters: Filters) => void;
}

export function PurposeFilters(props: Props) {
  const { t } = useLocale('purposes.filters');
  return (
    <FiltersContainer>
      <SelectInput
        items={[
          { label: t('expensesNIncomes'), value: OperationType.All },
          { label: t('incomes'), value: OperationType.Incomes },
          { label: t('expenses'), value: OperationType.Expenses },
        ]}
        defaultValue={props.filters.type}
        onSelect={(item) => props.onChange({
          ...props.filters,
          type: item.value as OperationType,
        })}
      />
      <SelectInput
        items={props.purposes.map(p => ({ label: p.name, value: p.id }))}
        defaultValue={props.filters.purposeId}
        onSelect={(item) => props.onChange({
          ...props.filters,
          purposeId: item.value,
        })}
      />
      <SelectInput
        items={[{ label: t('allYears'), value: ALL_YEARS }, ...props.operationYears.map(y => ({ label: y, value: y }))]}
        defaultValue={props.filters.year}
        onSelect={(item) => props.onChange({
          ...props.filters,
          year: item.value,
        })}
      />
    </FiltersContainer>
  );
}

const FiltersContainer = styled.div`
  display: flex;
  justify-content: center;
  & > * {
    margin: 15px 20px;
  }
`;
