import { gql } from '@apollo/client';
import { ApiCommons } from 'Modules/App/Api/Commons';

export namespace Purpose {
  export const Query = gql`
  query GetPurpose($id: String!) {
    purpose(id: $id) {
      id,
      name,
      status,
      operations {
        id,
        amount,
        currency,
        description,
        fulfilledAt,
      },
    }
  }
`;
  export interface Response {
    purpose: Purpose
  }

  export interface Request {
    id: string;
  }

  export interface Purpose {
    id: string;
    name: string;
    status: ApiCommons.PurposeStatus;
    operations: Operation[];
    __typename: 'PurposeResponse';
  }

  export interface Operation {
    id: string;
    amount: number;
    currency: string;
    description: string;
    fulfilledAt: string;
  }
}

