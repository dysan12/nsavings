import { gql } from '@apollo/client';
import { ApiCommons } from 'Modules/App/Api/Commons';

export namespace ChangePurposeStatus {
  export const Mutation = gql`
  mutation ChangePurposeStatus($id: String!, $status: PurposeStatus!){
    changePurposeStatus(data: {
      id: $id,
      status: $status
    }) {
      id,
      status,
    }
  }
`;

  export interface Request {
    id: string,
    status: ApiCommons.PurposeStatus,
  }

  export interface Response {
    changePurposeStatus: Purpose;
  }

  export type Purpose = {
    id: string,
    status: ApiCommons.PurposeStatus
  };
}

