import { gql } from '@apollo/client';

export namespace PurposeList {
  export const Query = gql`
  query PurposesGetPurposes {
    purposes(query: { status: null }) {
      data {
        id,
        name,
      }
    }
  }
`;

  export interface Response {
    purposes: {
      __typename: 'PurposeListResponse',
      data: Purpose[]
    }
  }

  export interface Purpose {
    id: string;
    name: string;
    __typename: 'PurposeResponse';
  }
}

