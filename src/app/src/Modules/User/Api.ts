import { request } from 'Modules/App/Api/Request';

export namespace UserApi {
  export const registerUser = (data: Request.Register) => request.post('/user/users', data);
  export const loginUser = (data: Request.Login) => request.post<Response.Login>('/user/tokens', data);

  export namespace Response {
    export interface Login {
      token: string;
    }
    export interface Register {

    }
  }
  export namespace Request {
    export interface Login {
      email: string;
      password: string;
    }
    export interface Register {
      email: string;
      password: string;
      firstName: string;
    }
  }
}
