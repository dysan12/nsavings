import { RegisterSlice } from 'Modules/User/Register/Slice';
import { AuthSlice } from 'Modules/User/Auth/Slice';

export const UserReducers = {
  [RegisterSlice.name]: RegisterSlice.reducer,
  [AuthSlice.name]: AuthSlice.reducer,
};
