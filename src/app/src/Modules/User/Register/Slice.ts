import { createSlice, createSelector } from '@reduxjs/toolkit';
import { AsyncStatus, PA } from 'Modules/App/Redux/Common';
import { UserApi } from 'Modules/User/Api';
import { Error } from 'Modules/App/Api/Error';

export namespace RegisterSlice {
  const initialState = {
    registration: {
      status: AsyncStatus.Idle,
      response: undefined,
      error: undefined as Error | undefined,
    },
  };
  type State = typeof initialState;
  const slice = createSlice({
    name: 'register',
    initialState,
    reducers: {
      registerUser: (state, _action: RegisterSlice.Payloads.registerUser) => {
        state.registration.status = AsyncStatus.Pending;
      },
      registerUserSucceed: (state) => {
        state.registration.status = AsyncStatus.Successful;
        state.registration.error = undefined;
      },
      registerUserFailed: (state, { payload }: RegisterSlice.Payloads.registerUserFailed) => {
        state.registration.status = AsyncStatus.Failed;
        state.registration.error = payload.error;
      },
    },
  });

  export const actions = slice.actions;
  export const reducer = slice.reducer;
  export const name = slice.name;

  const selectDomain = () => (state: any) => state[name] as State;
  export const selectors = {
    registration: () => createSelector(
      selectDomain(),
      (state) => state.registration,
    ),
  };
  export namespace Payloads {
    export type registerUser = PA<UserApi.Request.Register>;
    export type registerUserFailed = PA<{ error?: Error }>;
  }
}
