import React from 'react';
import { RegisterForm } from 'Modules/User/Register/Components/RegisterForm';
import { styled } from 'Modules/App/Styles/Styled';
import { Logo } from 'Modules/App/Components/Logo';
import { PageContainer, PageMiddleBox, ActionAlternativeBox } from 'Modules/User/Styled';
import { Link } from 'Modules/App/Components/Link';
import { Routes } from 'Modules/App/Routing/Routes';
import { useLocale } from 'Modules/App/Locale/useLocale';

export function RegisterPage() {
  const { t } = useLocale('user');
  return (
    <PageContainer>
      <PageMiddleBox>
        <StyledLogo />
        <RegisterForm />
        <ActionAlternativeBox>
          {t('haveYouSignedUp')}
          {' '}
          <Link to={Routes.login()}>
            {t('loginAction')}
          </Link>
        </ActionAlternativeBox>
      </PageMiddleBox>
    </PageContainer>
  );
}

const StyledLogo = styled(Logo)`
  margin-bottom: 17px;
`;
