import { call, put, takeLatest } from '@redux-saga/core/effects';
import { push } from 'connected-react-router';
import { Routes } from 'Modules/App/Routing/Routes';
import { RegisterSlice } from 'Modules/User/Register/Slice';
import { UserApi } from 'Modules/User/Api';
import { isError } from 'Modules/App/Api/Error';

function* registerUser({ payload }: RegisterSlice.Payloads.registerUser) {
  try {
    yield call(UserApi.registerUser, payload);
    yield put(RegisterSlice.actions.registerUserSucceed());
    yield put(push(Routes.login()));
  } catch (e) {
    yield put(RegisterSlice.actions.registerUserFailed({
      error: isError(e) ? e : undefined,
    }));
  }
}

export function* registerSagas() {
  yield takeLatest(RegisterSlice.actions.registerUser, registerUser);
}
