import React from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { useForm } from 'react-hook-form';
import { styled } from 'Modules/App/Styles/Styled';
import { PrimaryButton } from 'Modules/App/Components/Button';
import { Input } from 'Modules/App/Components/Form/Input';
import * as yup from 'yup';
import { TestContext } from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { RegisterSlice } from 'Modules/User/Register/Slice';
import { AsyncStatus } from 'Modules/App/Redux/Common';
import { ErrorBlock } from 'Modules/User/Styled';
import { yupResolver } from '@hookform/resolvers';

const passwordConfirmationTestName = 'passwordConfirmation';
const validationSchema = yup.object().shape({
  firstName: yup.string().min(3).max(255).required(),
  email: yup.string().email().required(),
  password: yup.string().min(3).required(),
  passwordConfirmation: yup.string().test(passwordConfirmationTestName, '', function (this: TestContext, value) {
    return this.parent.password === value;
  }).required(),
});

export function RegisterForm() {
  const { t } = useLocale('user');
  const { register, handleSubmit, errors } = useForm({ resolver: yupResolver(validationSchema) });
  const dispatch = useDispatch();
  const { error, status } = useSelector(RegisterSlice.selectors.registration());

  const onSubmit = (data: any) => {
    dispatch(RegisterSlice.actions.registerUser(data))
  };
  return (
    <Container>
      {error && <ErrorBlock>{t(`errorCodes.${error.code}`)}</ErrorBlock>}
      <Form onSubmit={handleSubmit(onSubmit)}>
        <StyledInput
          label={t('firstName')}
          placeholder={t('firstName')}
          name="firstName"
          ref={register}
          error={errors.firstName}
        />
        <StyledInput
          label={t('email')}
          placeholder={t('email')}
          name="email"
          ref={register}
          error={errors.email}
        />
        <StyledInput
          label={t('password')}
          placeholder={t('password')}
          type={'password'}
          name="password"
          ref={register}
          error={errors.password}
        />
        <StyledInput
          label={t('passwordConfirmation')}
          placeholder={t('passwordConfirmation')}
          type={'password'}
          name="passwordConfirmation"
          ref={register}
          error={errors.passwordConfirmation}
          translateError={(error) => {
            if (error.type === passwordConfirmationTestName) {
              return t('validation.passwordConfirmation');
            }
            return false;
          }}
        />
        <SubmitButton inProgress={status === AsyncStatus.Pending} type="submit">{t('registerAction')}</SubmitButton>
      </Form>
    </Container>
  );
}

const Container = styled.div`
  
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StyledInput = styled(Input)`
  width: 220px;
  &:last-of-type {
    margin-bottom: 18px;
  }
  & input {
    height: 34.5px;
  }
`;

const SubmitButton = styled(PrimaryButton)`
  width: 220px;
  height: 35px;
`;
