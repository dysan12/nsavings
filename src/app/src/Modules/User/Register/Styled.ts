import { styled } from 'Modules/App/Styles/Styled';

export const RegisterContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 4px;
  background: ${props => props.theme.colors.primary};
`
