import { all } from '@redux-saga/core/effects';
import { registerSagas } from 'Modules/User/Register/Sagas';
import { authSaga } from 'Modules/User/Auth/Saga';

export function* userSagas() {
  yield all([
    registerSagas(),
    authSaga(),
  ])
}
