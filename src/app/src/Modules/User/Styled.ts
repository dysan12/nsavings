import { styled } from 'Modules/App/Styles/Styled';

export const ErrorBlock = styled.div`
  margin-bottom: 13px;
  background-color: ${props => props.theme.colors.background.error};
  padding: 12px 8px;
  color: ${props => props.theme.colors.text.error};
  border-radius: 4px;
  font-size: 12px;
`;

export const PageContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  background-color: ${props => props.theme.colors.background.first};
`;

export const PageMiddleBox = styled.div`
  width: 350px;
  border: 1px solid ${props => props.theme.colors.border};
  border-top: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${props => props.theme.colors.background.first};
  padding: 8px 65px 20px;
  border-radius: 4px;
`;

export const ActionAlternativeBox = styled.div`
  font-size: 11px;
  margin-top: 14px;
`
