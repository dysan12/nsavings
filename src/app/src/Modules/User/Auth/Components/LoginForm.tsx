import React from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { useForm } from 'react-hook-form';
import { styled } from 'Modules/App/Styles/Styled';
import { PrimaryButton } from 'Modules/App/Components/Button';
import { Input } from 'Modules/App/Components/Form/Input';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { AsyncStatus } from 'Modules/App/Redux/Common';
import { AuthSlice } from 'Modules/User/Auth/Slice';
import { ErrorBlock } from 'Modules/User/Styled';
import { yupResolver } from '@hookform/resolvers';

const schema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required(),
});

export function LoginForm() {
  const { t } = useLocale('user');
  const { register, handleSubmit, errors } = useForm<AuthSlice.Payloads.login>({ resolver: yupResolver(schema) });
  const dispatch = useDispatch();
  const { error, status } = useSelector(AuthSlice.selectors.loginRequest());

  const onSubmit = (data: AuthSlice.Payloads.login) => {
    dispatch(AuthSlice.actions.login(data));
  };
  return (
    <Container>
      {error && <ErrorBlock>{t(`errorCodes.${error.code}`)}</ErrorBlock>}
      <Form onSubmit={handleSubmit(onSubmit)}>
        <StyledInput
          label={t('email')}
          placeholder={t('email')}
          name="email"
          ref={register}
          error={errors.email}
        />
        <StyledInput
          label={t('password')}
          placeholder={t('password')}
          name="password"
          ref={register}
          error={errors.password}
          type={'password'}
        />
        <SubmitButton inProgress={status === AsyncStatus.Pending} type="submit">{t('loginAction')}</SubmitButton>
      </Form>
    </Container>
  );
}

const Container = styled.div`
  
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StyledInput = styled(Input)`
  width: 220px;
  &:last-of-type {
    margin-bottom: 18px;
  }
  & input {
    height: 34.5px;
  }
`;

const SubmitButton = styled(PrimaryButton)`
  width: 220px;
  height: 35px;
`;
