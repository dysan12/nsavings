import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { Logo } from 'Modules/App/Components/Logo';
import { LoginForm } from 'Modules/User/Auth/Components/LoginForm';
import { PageContainer, PageMiddleBox, ActionAlternativeBox } from 'Modules/User/Styled';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { Link } from 'Modules/App/Components/Link';
import { Routes } from 'Modules/App/Routing/Routes';

export function LoginPage() {
  const { t } = useLocale('user');
  return (
    <PageContainer>
      <PageMiddleBox>
        <StyledLogo />
        <LoginForm />
        <ActionAlternativeBox>
          {t('dontYouHaveAccount')}
          {' '}
          <Link to={Routes.register()}>
            {t('registerAction')}
          </Link>
        </ActionAlternativeBox>
      </PageMiddleBox>
    </PageContainer>
  );
}

const StyledLogo = styled(Logo)`
  margin-bottom: 17px;
`;
