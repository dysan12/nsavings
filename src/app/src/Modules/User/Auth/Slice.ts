import { createSlice, createSelector } from '@reduxjs/toolkit';
import { AsyncStatus, PA } from 'Modules/App/Redux/Common';
import { UserApi } from 'Modules/User/Api';
import { Error } from 'Modules/App/Api/Error';
import { getToken } from 'Modules/App/Services/TokenStorage';

export namespace AuthSlice {
  const initialState = {
    loginRequest: {
      status: AsyncStatus.Idle as AsyncStatus,
      error: undefined as Error | undefined,
    },
    isAuthenticated: Boolean(getToken()),
  };
  type State = typeof initialState;

  const slice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
      login: (state, _action: PA<Payloads.login>) => {
        state.loginRequest.status = AsyncStatus.Pending;
      },
      loginSucceeded: (state) => {
        state.loginRequest.status = AsyncStatus.Successful;
        state.isAuthenticated = true;
      },
      loginFailed: (state, { payload }: PA<Payloads.loginFailed>) => {
        state.loginRequest.status = AsyncStatus.Failed;
        state.loginRequest.error = payload.error;
      },
      logout: (state) => {
        state.isAuthenticated = false;
      },
    },
  });
  // destructing does not work with CRA babel configuration
  export const actions = slice.actions;
  export const reducer = slice.reducer;
  export const name = slice.name;
  const selectDomain = () => (state: any) => state[name] as State;
  export const selectors = {
    isUserAuthenticated: () => createSelector(
      selectDomain(),
      (state) => state.isAuthenticated
    ),
    loginRequest: () => createSelector(
      selectDomain(),
      (state) => state.loginRequest,
    ),
  }
  export namespace Payloads {
    export type login = UserApi.Request.Login;
    export type loginFailed = { error?: Error };
  }
}
