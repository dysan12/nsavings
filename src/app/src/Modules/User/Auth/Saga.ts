import { call, put, takeLatest } from '@redux-saga/core/effects';
import { PA } from 'Modules/App/Redux/Common';
import { UserApi } from 'Modules/User/Api';
import { push } from 'connected-react-router';
import { Routes } from 'Modules/App/Routing/Routes';
import { AuthSlice } from 'Modules/User/Auth/Slice';
import { saveToken, removeToken } from 'Modules/App/Services/TokenStorage';
import { isError } from 'Modules/App/Api/Error';
import { ApolloClientManager } from 'Modules/App/Api/ApolloClientManager';

function* login(action: PA<AuthSlice.Payloads.login>) {
  const { payload } = action;
  try {
    const { token }: UserApi.Response.Login = yield call(UserApi.loginUser, payload);
    yield put(AuthSlice.actions.loginSucceeded());
    yield call(saveToken, token);
    yield put(push(Routes.dashboard()));
  } catch (e) {
    yield put(AuthSlice.actions.loginFailed({
      error: isError(e) ? e : undefined
    }));
  }
}

function* logout() {
  yield call(removeToken);
  yield put(push(Routes.login()));
  yield call([ApolloClientManager, ApolloClientManager.clearCache]);
}

export function* authSaga() {
  yield takeLatest(AuthSlice.actions.login, login);
  yield takeLatest(AuthSlice.actions.logout, logout);
}
