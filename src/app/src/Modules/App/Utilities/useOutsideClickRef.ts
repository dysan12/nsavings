import { useRef, useEffect } from 'react';

export function useOutsideClickRef<T extends HTMLElement>(callback: VoidFunction) {
  const ref = useRef<T>(null);
  const handleClick = (e: any) => {
    const current = ref.current;
    if (current !== null && current.contains(e.target) === false) {
      callback();
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  });

  return {
    ref,
  };
}
