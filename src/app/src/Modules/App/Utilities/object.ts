import { omitBy } from "lodash";

export function omitUndefined(obj: any) {
  return omitBy(obj, v => v === undefined);
}
