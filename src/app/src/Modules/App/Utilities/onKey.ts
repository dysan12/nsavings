import { KeyboardEvent } from 'react';

type Fn = (e: KeyboardEvent) => void;

export function onKey(keys: string[], callback: VoidFunction): Fn {
  return (e: KeyboardEvent) => {
    if (keys.includes(e.key)) {
      callback();
    }
  };
}
