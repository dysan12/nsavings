import { useEffect } from 'react';

type onKeyUpCallback = () => void;

interface KeyCallbacks {
  [key: string]: onKeyUpCallback;
}

export function useWindowKeyUpCallback(keyCallbacks: KeyCallbacks) {
  useEffect(() => {
    const handler = (e: KeyboardEvent) => {
      if (keyCallbacks[e.key]) {
        keyCallbacks[e.key]();
      }
    };
    window.addEventListener('keyup', handler);
    return () => {
      window.removeEventListener('keyup', handler);
    };
  });
}
