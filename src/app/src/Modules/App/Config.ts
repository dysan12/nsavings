export enum Currency {
  PLN = 'PLN',
  USD = 'USD',
  RUB = 'RUB',
  AUD = 'AUD',
  GBP = 'GBP',
  EUR = 'EUR',
  MXN = 'MXN',
  ZAR = 'ZAR',
}

export const API_DOMAIN = process.env.REACT_APP_API_DOMAIN ?? '';
export const NODE_ENV = process.env.NODE_ENV;
