import { useTranslation as wrappedUseTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { useCallback } from 'react';

export function useLocale(translationsNamespace: string = 'common'): { t: TranslateFunction, formatAmount: FormatAmountFunction, formatDate: FormatDateFunction } {
  const { t, i18n, ...rest } = wrappedUseTranslation();
  const lang = i18n.language;

  return {
    t: makeTranslate(translationsNamespace, t),
    formatAmount: useCallback(makeFormatAmount(lang), [lang]),
    formatDate: useCallback(makeFormatDate(lang), [lang]),
    ...rest,
  };
}

const makeTranslate = (namespace: string, t: TFunction) => (key: string, overrodeNamespace?: string) => {
  const translateNamespace = overrodeNamespace ?? namespace;
  return t(translateNamespace ? `${translateNamespace}.${key}` : key);
};
export type TranslateFunction = (key: string, overrodeNamespace?: string) => string;

// isCentesimal is amount given in a hundredth part eg. true if the value is given in cents https://en.wiktionary.org/wiki/centesimal
type FormatAmountParams = { amount: number, currency?: string, isCentesimal?: boolean };
const makeFormatAmount = (locale: string) => (params: FormatAmountParams) => {
  const options: Intl.NumberFormatOptions = params.currency ? { style: 'currency', currency: params.currency } : { maximumFractionDigits: 2 };
  return new Intl.NumberFormat(locale, options).format(
    (params.isCentesimal ?? true) ? params.amount / 100 : params.amount);
};
export type FormatAmountFunction = (params: FormatAmountParams) => string;

const makeFormatDate = (locale: string) => (date: string) => {
  return new Intl.DateTimeFormat(locale).format(new Date(date));
};
export type FormatDateFunction = (date: string) => string;
