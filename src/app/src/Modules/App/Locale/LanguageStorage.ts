const KEY = 'language';

export function saveLanguageToStorage(language: string) {
  localStorage.setItem(KEY, language);
}

export function removeStoredLanguage() {
  localStorage.removeItem(KEY);
}

export function getStoredLanguage(): string | null {
  return localStorage.getItem(KEY);
}
