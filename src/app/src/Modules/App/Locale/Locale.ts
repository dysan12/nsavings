import i18n from 'i18next';
import { initReactI18next, useTranslation } from 'react-i18next';
import { pl } from 'Modules/App/Locale/Translations/PL';
import { en } from 'Modules/App/Locale/Translations/EN';
import { getStoredLanguage, saveLanguageToStorage } from 'Modules/App/Locale/LanguageStorage';
import { NODE_ENV } from 'Modules/App/Config';

export enum SupportedLanguage {
  pl = 'pl',
  en = 'en'
}

const FALLBACK_LANG = SupportedLanguage.en;

export async function initTranslations(): Promise<void> {
  await i18n
    .use(initReactI18next)
    .init({
      lng: getInitialLanguage(),
      fallbackLng: FALLBACK_LANG,
      debug: NODE_ENV !== 'production',
      resources: {
        pl: {
          translation: pl,
        },
        en: {
          translation: en,
        },
      },
    });
}

export function getInitialLanguage(): SupportedLanguage {
  const lang = (getStoredLanguage() ?? '') as SupportedLanguage;
  if (Object.values(SupportedLanguage).includes(lang)) {
    return lang;
  } else {
    return SupportedLanguage.pl;
  }
}

export function getActiveLanguage(): SupportedLanguage {
  return i18n.language as SupportedLanguage;
}

export function useActiveLanguage(): SupportedLanguage {
  const { i18n } = useTranslation();
  return i18n.language as SupportedLanguage;
}

export async function changeLanguage(lang: SupportedLanguage): Promise<void> {
  await i18n.changeLanguage(lang);
  saveLanguageToStorage(lang);
}
