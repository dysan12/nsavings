import { Translations } from 'Modules/App/Locale/Translations/PL';
import { ErrorCode } from 'Modules/App/Api/Error';

export const en: Translations = {
  common: {
    ok: 'OK',
    validation: {
      required: 'This field is required',
      min: 'The value is too short',
      max: 'The value is too long',
      typeError: 'The value is invalid',
      notOneOf: 'The value is invalid'
    },
    of: 'of',
  },
  user: {
    registerAction: 'Sign up',
    loginAction: 'Log in',
    dontYouHaveAccount: 'New to Savings?',
    haveYouSignedUp: 'Have you already signed up?',
    firstName: 'First name',
    email: 'Email',
    password: 'Password',
    passwordConfirmation: 'Password confirmation',
    validation: {
      passwordConfirmation: 'Confirmation of the password must be identical',
    },
    errorCodes: {
      [ErrorCode.USER_ALREADY_EXISTS]: 'Provided email address is already occupied',
      [ErrorCode.USER_PASSWORD_NOT_MATCH]: 'Provided email or password are not valid',
      [ErrorCode.USER_NOT_FOUND]: 'Provided email or password are not valid',
    },
  },
  dashboard: {
    purposes: 'Purposes',
    purpose: 'Purpose',
    purposeWithNoFunds: 'No operations',
    transactions: 'Transactions',
    noPurposesAvailable: 'You don\'t have any active purposes. Create one!',
    date: 'Date',
    actions: 'Actions',
    description: 'Description',
    amount: 'Amount',
    list: 'List',
    creator: 'Creator',
    create: 'Create',
    transactionCreator: {
      date: 'Date',
      description: 'Description',
      operations: 'Operations',
      purpose: 'Purpose',
      amount: 'Amount',
      currency: 'Currency',
    },
    draft: 'Draft',
    draftName: 'Name',
    saveDraft: 'Save draft',
    newDraft: 'New draft',
    total: 'Total',
    showActive: 'Active',
    showAll: 'All',
    noTransactionsMade: 'You have not made any transactions yet.',
  },
  purposes: {
    filters: {
      expenses: 'Expenses',
      incomes: 'Incomes',
      expensesNIncomes: 'Expenses and incomes',
      allYears: 'Whenever',
    },
    summary: {
      expenses: 'Expenses',
      incomes: 'Incomes',
    },
    table: {
      date: 'Date',
      description: 'Description',
      amount: 'Amount',
    },
    archive: 'Archive',
    active: 'Active'
  }
};
