import { ErrorCode } from 'Modules/App/Api/Error';

export const pl = {
  common: {
    ok: 'OK',
    validation: {
      required: 'To pole jest wymagane',
      min: 'Wartość jest zbyt krótka',
      max: 'Wartość jest zbyt długa',
      typeError: 'Nieprawidłowa wartość',
      notOneOf: 'Nieprawidłowa wartość'
    },
    of: 'z',
  },
  user: {
    registerAction: 'Zarejestruj się',
    loginAction: 'Zaloguj się',
    dontYouHaveAccount: 'Nie masz jeszcze konta?',
    haveYouSignedUp: 'Masz już konto?',
    firstName: 'Imię',
    email: 'Adres email',
    password: 'Hasło',
    passwordConfirmation: 'Potwierdzenie hasła',
    validation: {
      passwordConfirmation: 'Potwierdzenie hasła musi być identyczne',
    },
    errorCodes: {
      [ErrorCode.USER_ALREADY_EXISTS]: 'Użytkownik o podanym adresie email już istnieje',
      [ErrorCode.USER_PASSWORD_NOT_MATCH]: 'Podany email lub hasło są niepoprawne',
      [ErrorCode.USER_NOT_FOUND]: 'Podany email lub hasło są niepoprawne', // deliberately left the same translation
    },
  },
  dashboard: {
    purposes: 'Cele',
    purpose: 'Cel',
    purposeWithNoFunds: 'Brak operacji',
    transactions: 'Transakcje',
    noPurposesAvailable: 'Nie posiadasz obecnie żadnych aktywnych celów. Załóż je!',
    date: 'Data',
    actions: 'Akcje',
    description: 'Opis',
    amount: 'Kwota',
    list: 'Lista',
    creator: 'Kreator',
    create: 'Stwórz',
    transactionCreator: {
      date: 'Data',
      description: 'Opis',
      operations: 'Operacje',
      purpose: 'Cel',
      amount: 'Kwota',
      currency: 'Waluta',
    },
    draft: 'Szablon',
    draftName: 'Nazwa',
    saveDraft: 'Zapisz szablon',
    newDraft: 'Nowy szablon',
    total: 'Łącznie',
    showActive: 'Aktywne',
    showAll: 'Wszystkie',
    noTransactionsMade: 'Nie dokonałeś jeszcze żadnych transakcji.',
  },
  purposes: {
    filters: {
      expenses: 'Odpływy',
      incomes: 'Wpływy',
      expensesNIncomes: 'Wpływy i odpływy',
      allYears: 'Kiedykolwiek',
    },
    summary: {
      expenses: 'Odpływy',
      incomes: 'Wpływy',
    },
    table: {
      date: 'Data',
      description: 'Opis',
      amount: 'Kwota',
    },
    archive: 'Zarchiwizuj',
    active: 'Aktywuj'
  }
};

export type Translations = typeof pl;
