export function isError(obj: any): obj is Error {
  return obj.code !== undefined;
}

export interface Error {
  code: ErrorCode;
}

export enum ErrorCode {
  USER_ALREADY_EXISTS = 'USER/ALREADY_EXISTS',
  USER_PASSWORD_NOT_MATCH = 'USER/PASSWORD_NOT_MATCH',
  USER_NOT_FOUND = 'USER/NOT_FOUND',
}
