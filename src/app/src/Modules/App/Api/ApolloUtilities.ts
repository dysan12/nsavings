import { ApolloError } from '@apollo/client';
import { toast } from 'react-toastify';

export function snackbarErrorHandler(error: ApolloError) {
  toast(error.message);
}

export function makeSnackbarAcknowledgement(msg: string) {
  return () => {
    toast(msg);
  }
}
