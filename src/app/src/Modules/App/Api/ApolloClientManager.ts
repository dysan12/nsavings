import { getToken } from 'Modules/App/Services/TokenStorage';
import { store } from 'Modules/App/Redux/Store';
import { AuthSlice } from 'Modules/User/Auth/Slice';
import { ApolloClient, HttpLink, ApolloLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client';
import { onError } from '@apollo/link-error';
import { API_DOMAIN } from 'Modules/App/Config';

class Manager {
  private readonly client: ApolloClient<NormalizedCacheObject>;

  public constructor() {
    this.client = new ApolloClient({
      link: ApolloLink.from(this.createLinks()),
      cache: new InMemoryCache({
        typePolicies: {
          PurposeResponse: {
            fields: {
              balance: {
                merge(_existing, incoming) {
                  return incoming;
                }
              }
            }
          }
        }
      }),
    });
  }

  public async clearCache(): Promise<void> {
    await this.client.clearStore();
  }

  public getClient() {
    return this.client;
  }

  private createLinks(): ApolloLink[] {
    return [
      this.createErrorHandlerLink(),
      this.createAuthLink(),
      this.createHttpLink(),
    ];
  }

  private createErrorHandlerLink(): ApolloLink {
    return onError((error) => {
      const { response } = error;
      for (const error of response?.errors ?? []) {
        if (error.message === 'Unauthorized') {
          store.dispatch(AuthSlice.actions.logout());
        }
      }
      console.error(error);
    });
  }

  private createHttpLink(): ApolloLink {
    return new HttpLink({
      uri: `${API_DOMAIN}/graphql`,
    });
  }

  private createAuthLink(): ApolloLink {
    return new ApolloLink((operation, forward) => {
      const token = getToken();
      if (token) {
        operation.setContext(({ headers = {} }) => ({
            headers: {
              ...headers,
              'Authorization': `Bearer ${token}`,
            },
          }),
        );
      }
      return forward(operation);
    });
  }
}

export const ApolloClientManager = new Manager();
