export namespace ApiCommons {
  export enum PurposeStatus {
    Archived = 'Archived',
    Active = 'Active'
  }
}
