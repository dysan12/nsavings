import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { getToken } from 'Modules/App/Services/TokenStorage';
import { store } from 'Modules/App/Redux/Store';
import { AuthSlice } from 'Modules/User/Auth/Slice';
import { API_DOMAIN } from 'Modules/App/Config';

const responseInterceptor = {
  onFulfilled: (response: AxiosResponse) => {
    if (response.status === 401) {
      store.dispatch(AuthSlice.actions.logout());
      return Promise.reject(response);
    }
    if (response.status === 400) {
      return Promise.reject({ code: response.data?.status });
    }
    return response.data;
  },
  onRejected: (error: any) => {
    console.log(error);
    return Promise.reject(error);
  },
};

export const request = axios.create({
  baseURL: API_DOMAIN,
  validateStatus: (status: number) => status < 500,
});
request.interceptors.request.use(function(config: AxiosRequestConfig) {
  const token = getToken();
  if (token) {
    config.headers.common['Authorization'] = `Bearer ${getToken()}`;
  }
  return config;
});
request.interceptors.response.use(responseInterceptor.onFulfilled, responseInterceptor.onRejected);
