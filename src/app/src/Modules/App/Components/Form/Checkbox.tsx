import { styled } from 'Modules/App/Styles/Styled';
import React from 'react';

interface Props {
  value?: string;
  onChange?: VoidFunction;
  label?: string;
  className?: string;
}

function CheckboxComponent(props: Props) {
  return (
    <Container className={props.className}>
        {props.label && <LabelText>{props.label}</LabelText>}
        <Input type={'checkbox'} onChange={props.onChange} value={props.value} />
    </Container>
  );
}

const Container = styled.label`
  display: flex;
  align-items: center;
`;

const LabelText = styled.span`
  font-size: 11px;
  padding-right: 5px;
`;

const Input = styled.input`
  padding: 13px 15px 13px 15px;
  border: solid 1px;
  border-radius: 4px;
  font-size: 12px;
`;

export const Checkbox = styled(CheckboxComponent)`

`;
