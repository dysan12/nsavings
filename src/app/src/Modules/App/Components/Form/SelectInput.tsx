import { styled } from 'Modules/App/Styles/Styled';
import React, { useState, useRef, useEffect, useCallback } from 'react';
import { useWindowKeyUpCallback } from 'Modules/App/Utilities/useWindowKeyUpCallback';
import { DownArrowIcon } from 'Modules/App/Components/Icons/DownArrowIcon';
import { useOutsideClickRef } from 'Modules/App/Utilities/useOutsideClickRef';
import { onKey } from 'Modules/App/Utilities/onKey';
import { memoize } from 'lodash';
import { InputError } from 'Modules/App/Components/Form/Input';

type Value = string;
type Item = { label: string, value: Value };

interface Props {
  className?: string;
  items: Item[];
  defaultValue?: Value;
  value?: string;
  name?: string;
  label?: string;
  onSelect?: (item: Item) => void;
  unselectLabel?: string;
  listUnselectItem?: boolean;
  // todo generic container handling errors
  error?: InputError;
}

const getItemByValue = (value: Value, list: Item[], defaultItem: Item): Item => {
  return list.find(item => item.value === value) ?? defaultItem;
};
const createItem = memoize((label: string, value: Value): Item => ({ label, value }));
export const SelectInput = React.forwardRef<HTMLInputElement | null, Props>((props: Props, formRef) => {
  const [isUnfolded, setIsUnfolded] = useState(false);
  const { ref: elementRef } = useOutsideClickRef<HTMLDivElement>(() => setIsUnfolded(false));
  const firstItem = useRef<HTMLDivElement>();
  useWindowKeyUpCallback({
    'Escape': () => setIsUnfolded(false),
  });

  const { defaultValue = '', listUnselectItem = false, onSelect, error } = props;
  const unselectItem = createItem(props.unselectLabel ?? '----', '');
  const [selected, setSelected] = useState<Item>(getItemByValue(defaultValue, props.items, unselectItem));

  useEffect(() => {
    setSelected(getItemByValue(defaultValue, props.items, unselectItem));
  }, [defaultValue, props.items, unselectItem]);

  const selectItem = useCallback((item: Item) => {
    setSelected(item);
    setIsUnfolded(false);
    if (onSelect) {
      onSelect(item);
    }
  }, [setSelected, setIsUnfolded, onSelect]);
  let items: Item[];
  if (listUnselectItem) {
    items = [unselectItem, ...props.items];
  } else {
    items = props.items;
  }

  return (
    <Container className={props.className} ref={elementRef}>
      {props.label && (
        <Label>
          {props.label}
        </Label>
      )}
      <Select
        onClick={() => setIsUnfolded(prev => !prev)}
        onKeyDown={onKey([' ', 'ArrowUp', 'ArrowDown', 'Enter'], () => {
          setIsUnfolded(true);
          if (firstItem.current) {
            firstItem.current!.focus();
          }
        })}
        tabIndex={0}
        hasError={Boolean(error)}
      >
        <SelectedValue>
          {selected.label}
        </SelectedValue>
        <SelectArrow>
          <DownArrowIcon />
        </SelectArrow>
      </Select>
      {isUnfolded && (
        <UnfoldedSection>
          {items.map((i, index) => (
            <StyledItem
              onClick={() => {
                selectItem(i);
              }} key={i.value}
              onKeyDown={onKey(['Enter'], () => {
                selectItem(i);
              })}
              ref={index === 0 ? firstItem : undefined as any}
            >
              {i.label}
            </StyledItem>
          ))}
        </UnfoldedSection>
      )}
      <StubInput ref={formRef} name={props.name} value={selected?.value ?? ''} readOnly={true} />
    </Container>
  );
});

/**
 * TODO: Generalize select with uncontrolled one
 */
export const ControlledSelectInput = React.forwardRef<HTMLInputElement | null, Props>((props: Props, formRef) => {
  const [isUnfolded, setIsUnfolded] = useState(false);
  const { ref: elementRef } = useOutsideClickRef<HTMLDivElement>(() => setIsUnfolded(false));
  const firstItem = useRef<HTMLDivElement>();
  useWindowKeyUpCallback({
    'Escape': () => setIsUnfolded(false),
  });

  const { value, onSelect, error, listUnselectItem } = props;
  const unselectItem = createItem(props.unselectLabel ?? '----', '');
  const selected = getItemByValue(value ?? '', props.items, unselectItem);

  const selectItem = useCallback((item: Item) => {
    setIsUnfolded(false);
    if (onSelect) {
      onSelect(item);
    }
  }, [setIsUnfolded, onSelect]);
  let items: Item[];
  if (listUnselectItem) {
    items = [unselectItem, ...props.items];
  } else {
    items = props.items;
  }

  return (
    <Container className={props.className} ref={elementRef}>
      {props.label && (
        <Label>
          {props.label}
        </Label>
      )}
      <Select
        onClick={() => setIsUnfolded(prev => !prev)}
        onKeyDown={onKey([' ', 'ArrowUp', 'ArrowDown', 'Enter'], () => {
          setIsUnfolded(true);
          if (firstItem.current) {
            firstItem.current!.focus();
          }
        })}
        tabIndex={0}
        hasError={Boolean(error)}
      >
        <SelectedValue>
          {selected.label}
        </SelectedValue>
        <SelectArrow>
          <DownArrowIcon />
        </SelectArrow>
      </Select>
      {isUnfolded && (
        <UnfoldedSection>
          {items.map((i, index) => (
            <StyledItem
              onClick={() => {
                selectItem(i);
              }} key={i.value}
              onKeyDown={onKey(['Enter'], () => {
                selectItem(i);
              })}
              ref={index === 0 ? firstItem : undefined as any}
            >
              {i.label}
            </StyledItem>
          ))}
        </UnfoldedSection>
      )}
      <StubInput ref={formRef} name={props.name} value={selected?.value ?? ''} readOnly={true} />
    </Container>
  );
});


const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  margin-bottom: 12.5px;
  * {
    background-color: ${props => props.theme.colors.background.first};    
  }
`;

const Label = styled.label`
  padding-bottom: 4.75px;
  font-size: 11px;
`;

const Select = styled.div<{ hasError: boolean }>`
  display: grid;
  grid-template-columns: auto 15px;
  grid-template-areas: "value arrow";
  padding: 12.5px 7px 12.5px 15px;
  border: solid 1px ${props => !props.hasError ? props.theme.colors.border : props.theme.colors.borderError};
  border-radius: 4px;
  font-size: 12px;
`;

const SelectedValue = styled.div`
  grid-area: value;
`;

const SelectArrow = styled.div`
  grid-area: arrow;
  display: flex;
  justify-content: center;
  pointer-events: none;
  svg {
    width: 10px;
    height: 10px;
  }
`;

const UnfoldedSection = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  border: solid 1px ${props => props.theme.colors.border};
  border-radius: 4px;
  border-top: none;
  z-index: 100;
  width: 100%;
  max-height: 210px;
  overflow-y: auto;
  top: 100%;
`;

const StyledItem = styled.div`
  cursor: pointer;
  padding: 13px 15px;
  &:not(:last-of-type) {
    border-bottom: solid 1px ${props => props.theme.colors.border};
  }
  &:hover &:focus {
    background-color: ${props => props.theme.colors.background.hover};
  }
`;

const StubInput = styled.input`
  visibility: hidden;
  height: 0;
  border: none;
`;

