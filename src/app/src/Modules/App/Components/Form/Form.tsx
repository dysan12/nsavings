import React, { ReactElement } from 'react';

interface Props {
  onSubmit: () => void;
  className?: string;
  children: ReactElement[];
}

export function Form(props: Props) {
  return (<form onSubmit={e => {
    e.preventDefault();
    props.onSubmit();
  }
  } className={props.className}>{props.children}</form>);
}
