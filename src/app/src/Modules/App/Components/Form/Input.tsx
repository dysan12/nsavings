import React, { KeyboardEvent, ChangeEvent, SyntheticEvent, Ref } from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { useLocale } from 'Modules/App/Locale/useLocale';
import { omitUndefined } from 'Modules/App/Utilities/object';

export type InputError = {
  type: string;
}
type ErrorTranslator = (error: InputError) => false | string;

interface InputProps {
  placeholder?: string;
  label?: string;
  name?: string;
  type?: 'text' | 'password' | 'number' | 'date' | 'checkbox';
  error?: InputError;
  errorPlacement?: 'bottom' | 'top';
  // if false returned; use default translation
  translateError?: ErrorTranslator;
  className?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onKeyUp?: (e: KeyboardEvent<HTMLInputElement>) => void;
  onBlur?: (e: SyntheticEvent<HTMLInputElement>) => void;
  defaultValue?: string | number | Date;
  value?: string | number;
  autoFocus?: boolean;
  hidden?: boolean;
}

const defaults: Partial<InputProps> = {
  errorPlacement: 'bottom',
};

function normalizeDefaultValue(value: any) {
  if (value instanceof Date) {
    return value.toISOString().split('T')[0];
  }
  return value?.toString();
}

export const Input = React.forwardRef<HTMLInputElement, InputProps>((props: InputProps, ref: Ref<HTMLInputElement>) => {
  const { t } = useLocale();
  const { error, type, placeholder, name, className, label, onChange, onKeyUp, onBlur, defaultValue, value, errorPlacement } = { ...defaults, ...props };
  const translateError = (error: InputError) => {
    const customTranslator = props.translateError;
    if (customTranslator) {
      return customTranslator(error) || t(`validation.${error.type}`);
    }
    return t(`validation.${error.type}`);
  };
  const hasError = error !== undefined;

  const inputProps = omitUndefined({ type, ref, placeholder, name, onChange, onKeyUp, onBlur, value, defaultValue: normalizeDefaultValue(defaultValue) });
  return (
    <InputBox className={className} hasError={hasError} hidden={props.hidden}>
      {label && (
        <Label>
          {label}
        </Label>
      )}
      {error && errorPlacement === 'top' && (
        <ErrorContainer>
          <Error>
            {translateError(error)}
          </Error>
        </ErrorContainer>
      )}
      <StyledInput {...inputProps} />
      {error && errorPlacement === 'bottom' && (
        <ErrorContainer>
          <Error>
            {translateError(error)}
          </Error>
        </ErrorContainer>
      )}
    </InputBox>
  );
});

const Label = styled.label`
  padding-bottom: 4.75px;
  font-size: 11px;
`;

const ErrorContainer = styled.div`
  margin-top: 4.75px;
`;

const Error = styled.span`
  font-size: 11px;
`;

const InputBox = styled.div<{ hasError: boolean, hidden?: boolean }>`
  display: ${props => props.hidden ? 'none' : 'flex'};
  flex-direction: column;
  margin-bottom: 12.5px;
  * {
    color: ${props => props.hasError ? props.theme.colors.text.error : 'initial'};
    border-color: ${props => props.hasError ? props.theme.colors.borderError : props.theme.colors.border} !important;  
  }
`;

export const StyledInput = styled.input`
  padding: 13px ${props => props.type === 'number' ? '4px' : '15px'} 13px 15px;
  border: solid 1px;
  border-radius: 4px;
  font-size: 12px;
`;
