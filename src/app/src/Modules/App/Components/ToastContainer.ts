import { ToastContainer as WrappedToastContainer } from 'react-toastify';
import { styled } from 'Modules/App/Styles/Styled';

export const ToastContainer = styled(WrappedToastContainer).attrs({
  position: 'bottom-right'
})`
  .Toastify__toast-container {}
  .Toastify__toast {}
  .Toastify__toast--error {}
  .Toastify__toast--warning {}
  .Toastify__toast--success {}
  .Toastify__toast-body {}
  .Toastify__progress-bar {
    background: ${props => props.theme.colors.primary};
  }
`;
