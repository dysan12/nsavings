import { styled } from 'Modules/App/Styles/Styled';
import { NextArrowIcon } from 'Modules/App/Components/Icons/NextArrowIcon';
import { BackArrowIcon } from 'Modules/App/Components/Icons/BackArrowIcon';
import React from 'react';
import { useLocale } from 'Modules/App/Locale/useLocale';

interface Props {
  className?: string,
  currentPage: number,
  maxResults: number,
  perPage: number,
  onPageChange: (page: number) => void,
}

function PaginationComponent(props: Props) {
  const { t } = useLocale();
  const { currentPage, perPage, maxResults, onPageChange, className } = props;
  const bottomRange = perPage * (currentPage - 1) + 1;
  const upperRange = maxResults < currentPage * perPage ? maxResults : currentPage * perPage;
  const label = `${bottomRange}-${upperRange} ${t('of')} ${maxResults}`;

  return (
    <div className={className}>
      <Label>{label}</Label>
      <Arrows>
        <BackArrowIcon onClick={() => onPageChange(currentPage - 1)} disabled={currentPage === 1} />
        <NextArrowIcon onClick={() => onPageChange(currentPage + 1)} disabled={currentPage === Math.ceil(maxResults / perPage)} />
      </Arrows>
    </div>
  );
}

const Label = styled.div`
  margin-right: 16px;
`;

const Arrows = styled.div`
  display: flex;
  & > * {
    margin: 0 16px;
  }
  svg {
    width: 15px;
    height: 15px;
  }
`;

export const Pagination = styled(PaginationComponent)`
  display: flex;
  justify-content: right;
  padding: 15px 35px;
`;
