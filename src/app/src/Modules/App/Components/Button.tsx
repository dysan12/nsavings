import { styled } from 'Modules/App/Styles/Styled';
import React, { ReactNode } from 'react';
import { DarkLoader, LightLoader } from 'Modules/App/Components/Loader';
import { css } from 'styled-components';

export namespace Button {
  export interface Props {
    inProgress?: boolean;
    disabled?: boolean;
    children?: ReactNode;
    type?: 'submit' | 'button';
    className?: string;
    onClick?: VoidFunction;
    loaderMotif?: 'dark' | 'light';
  }

  export function Component(props: Props) {
    function getLoader() {
      return props.loaderMotif === 'dark' ? <DarkLoader /> : <LightLoader />;
    }

    return (
      <button
        className={props.className}
        disabled={props.disabled}
        onClick={() => {
          if (!props.inProgress && !props.disabled && props.onClick) {
            props.onClick();
          }
        }}
        type={props.type ?? 'button'}
      >
        {props.inProgress ? getLoader() : props.children}
      </button>
    );
  }
}


const StyledButton = styled(Button.Component)`
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  font-size: 15px;
  font-weight: ${props => props.theme.fontWeight.medium};
  border: none;
  cursor: ${props => props.disabled ? 'default' : 'pointer'};
`;

export const PrimaryButton = styled(StyledButton).attrs({ loaderMotif: 'light'})`
  ${props => props.disabled ? css`
    background-color: ${props.theme.colors.button.disabled.background};
    color: ${props.theme.colors.button.disabled.text};
  ` : css`
    background-color: ${props.theme.colors.button.primary.background};
    color: ${props.theme.colors.button.primary.text};
  `}
`;

// todo disabled state
export const SecondaryButton = styled(StyledButton).attrs({ loaderMotif: 'dark' })`
  border: 3px solid ${props => props.theme.colors.button.secondary.border};
  color: ${props => props.theme.colors.button.secondary.text};
  background-color: ${props => props.theme.colors.button.secondary.background};
  box-shadow: none;
`;
