import React, { ReactElement } from 'react';
import { Logo } from 'Modules/App/Components/Logo';
import { styled } from 'Modules/App/Styles/Styled';
import { useDispatch } from 'react-redux';
import { AuthSlice } from 'Modules/User/Auth/Slice';
import { PowerOffIcon } from 'Modules/App/Components/Icons/PowerOffIcon';
import { LanguageSwitcher } from 'Modules/App/Components/Panel/Components/LanguageSwitcher';
import { push } from 'connected-react-router';
import { Routes } from 'Modules/App/Routing/Routes';

interface Props {
  children: ReactElement | ReactElement[];
}

export function Panel(props: Props) {
  const dispatch = useDispatch();

  return (
    <PanelContainer>
      <TopBarContainer>
        <StyledLogo onClick={() => dispatch(push(Routes.dashboard()))} />
        <ControlButtons>
          <LanguageSwitcher />
          <LogoutButton onClick={() => dispatch(AuthSlice.actions.logout())} />
        </ControlButtons>
      </TopBarContainer>
      <PanelContent>
        {props.children}
      </PanelContent>
    </PanelContainer>
  );
}

const PanelContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const TopBarContainer = styled.div`
  display: grid;
  grid-template-columns: 200px auto 70px;
  grid-template-areas: "logo . buttons";
  padding: 10px 34px;
  border-bottom: solid 1px ${props => props.theme.colors.border};
`;

const ControlButtons = styled.div`
  grid-area: buttons;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const StyledLogo = styled(Logo)`
  cursor: pointer;
`;

const LogoutButton = styled(PowerOffIcon)`
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const PanelContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;
