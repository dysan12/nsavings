import { styled } from 'Modules/App/Styles/Styled';
import { SupportedLanguage, changeLanguage, useActiveLanguage } from 'Modules/App/Locale/Locale';
import React from 'react';

export function LanguageSwitcher() {
  const activeLanguage = useActiveLanguage();

  const onSwitcherClick = async function() {
    if (activeLanguage === SupportedLanguage.en) {
      await changeLanguage(SupportedLanguage.pl);
    } else {
      await changeLanguage(SupportedLanguage.en);
    }
  };

  return <StyledLanguageSwitcher onClick={onSwitcherClick}>{activeLanguage.toUpperCase()}</StyledLanguageSwitcher>;
}

const StyledLanguageSwitcher = styled.span`
  font-size: 20px;
  color: ${props => props.theme.colors.icons};
  cursor: pointer;
`;
