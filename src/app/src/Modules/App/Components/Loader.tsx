import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';

function LoaderComponent(props: { className?: string }) {
  return (
    <LoaderContainer>
      <svg className={props.className} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <circle
          cx="50"
          cy="50"
          fill="none"
          stroke="black"
          strokeWidth="5"
          r="35"
          strokeDasharray="164.93361431346415 56.97787143782138"
          transform="rotate(29.2644 50 50)"
        >
          <animateTransform
            attributeName="transform"
            type="rotate"
            calcMode="linear"
            values="0 50 50;360 50 50"
            keyTimes="0;1"
            dur="1s"
            begin="0s"
            repeatCount="indefinite" />
        </circle>
      </svg>
    </LoaderContainer>
  );
}

const LoaderContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Loader = styled(LoaderComponent)`
  height: 30px;
  width: 30px;
  * {
    stroke: ${props => props.theme.colors.loaderStrokeLight};
  }
`;

export const LightLoader = styled(Loader)`
  * {
    stroke: ${props => props.theme.colors.loaderStrokeLight}
  }
`

export const DarkLoader = styled(Loader)`
  * {
    stroke: ${props => props.theme.colors.loaderStrokeDark}
  }
`
