import React from 'react';
import { wrapIcon } from 'Modules/App/Components/Icons/Icon';

const svg = (
  <svg viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M7.705 1.41L6.295 0L0.295 6L6.295 12L7.705 10.59L3.125 6L7.705 1.41Z"
    />
  </svg>
);
export const BackArrowIcon = wrapIcon(svg);

