import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';

interface IconWrapperProps {
  disabled?: boolean;
  className?: string;
  onClick?: VoidFunction;
}

interface IconProps extends IconWrapperProps {
  svg: JSX.Element;
}

export function wrapIcon(svg: JSX.Element) {
  return (props: IconWrapperProps) => (<Icon {...props} svg={svg} />)
}

export function IconComponent(props: IconProps) {
  return (
    <Container className={props.className} onClick={(e) => {
      if (!props.disabled && props.onClick) {
        props.onClick();
      }
      e.stopPropagation();
    }}>
      {props.svg}
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

export const Icon = styled(IconComponent)`
  cursor: ${props => props.disabled ? 'initial' : 'pointer'};
  svg {
    width: 24px;
    height: 24px;
    * {
      fill: ${props => props.disabled ? props.theme.colors.iconsDisabled : props.theme.colors.icons};
    }
  }
`;
