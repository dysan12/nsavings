import { styled } from 'Modules/App/Styles/Styled';
import React from 'react';

interface Props {
  className?: string;
  onClick: VoidFunction;
}

function PowerOffIconComponent(props: Props) {
  return (
    <svg {...props} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.75 0.75H13.25V13.25H10.75V0.75ZM17.5125 5.23751L19.2875 3.46251C21.7125 5.52501 23.25 8.57501 23.25 12C23.25 18.2125 18.2125 23.25 12 23.25C5.7875 23.25 0.75 18.2125 0.75 12C0.75 8.57501 2.2875 5.52501 4.7125 3.46251L6.475 5.22501C4.5125 6.82501 3.25 9.26251 3.25 12C3.25 16.8375 7.1625 20.75 12 20.75C16.8375 20.75 20.75 16.8375 20.75 12C20.75 9.26251 19.4875 6.82501 17.5125 5.23751Z"
      />
    </svg>
  )
}

export const PowerOffIcon = styled(PowerOffIconComponent)`
  width: 24px;
  height: 24px;
  * {
    fill: ${props => props.theme.colors.icons};
  }
`
