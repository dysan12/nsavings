import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { PrimaryButton, Button } from 'Modules/App/Components/Button';

function RemoveButtonComponent({ loaderMotif, ...rest }: Button.Props) {
  return (
    <PrimaryButton {...rest}>
      <svg width="12" height="2" viewBox="0 0 12 2" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0H12V2H0V0Z" fill="white"/>
      </svg>
    </PrimaryButton>
  )
}

export const RemoveButton = styled(RemoveButtonComponent)`
  height: 22px;
  width: 22px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  box-shadow: none;
  align-items: center;
  
`
