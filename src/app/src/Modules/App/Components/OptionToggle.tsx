import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { css } from 'styled-components';

export enum ToggleOptionSide {
  Left = 'left',
  Right = 'right'
}

interface Props {
  activeOption: ToggleOptionSide;
  onActiveOptionChange: (side: ToggleOptionSide) => void;
  left: string;
  right: string;
  className?: string;
}

export function OptionToggle(props: Props) {
  return (
    <Container className={props.className}>
      <OptionLeft onClick={() => props.onActiveOptionChange(ToggleOptionSide.Left)}
                  isActive={props.activeOption === ToggleOptionSide.Left}>{props.left}</OptionLeft>
      <OptionRight onClick={() => props.onActiveOptionChange(ToggleOptionSide.Right)}
                   isActive={props.activeOption === ToggleOptionSide.Right}>{props.right}</OptionRight>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
`;

const StyledOption = styled.div<{ isActive: boolean }>`
  border: 3px solid ${props => props.isActive ? props.theme.colors.button.secondary.border : props.theme.colors.button.secondaryDisabled.border};
  color: ${props => props.isActive ? props.theme.colors.button.secondary.text : props.theme.colors.button.secondaryDisabled.text};
  text-align: center;
  width: 50%;
  cursor: ${props => props.isActive ? 'default' : 'pointer'};
  padding: 4px;
`;

const OptionLeft = styled(StyledOption)`
  border-bottom-left-radius: 4px;
  border-top-left-radius: 4px;
  ${props => props.isActive === false && css`
    border-right: none;
  `}
`;

const OptionRight = styled(StyledOption)`
  border-bottom-right-radius: 4px;
  border-top-right-radius: 4px;
  ${props => props.isActive === false && css`
    border-left: none;
  `}
`;
