import { Link as RouterLink } from 'react-router-dom';
import { styled } from 'Modules/App/Styles/Styled';

export const Link = styled(RouterLink)`
  color: ${props => props.theme.colors.primary};
  font-weight: ${props => props.theme.fontWeight.bold};
  text-decoration: none;
`
