import React from 'react';
import { styled } from 'Modules/App/Styles/Styled';
import { PrimaryButton, Button } from 'Modules/App/Components/Button';

function AddButtonComponent({ loaderMotif, ...rest }: Button.Props) {
  return (
    <PrimaryButton {...rest}>
      <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.1334 6.73334H6.73337V11.1333H5.2667V6.73334H0.866699V5.26667H5.2667V0.866669H6.73337V5.26667H11.1334V6.73334Z" fill="white"/>
      </svg>
    </PrimaryButton>
  )
}

export const AddButton = styled(AddButtonComponent)`
  height: 22px;
  width: 22px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  box-shadow: none;
  align-items: center;
  
`
