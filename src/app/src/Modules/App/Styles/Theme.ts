const primary = '#059ABB';
export const Theme = {
  colors: {
    /**
     * Elements that should attract attention:
     * - title
     * - headlines
     * - logo
     * - menu tabs(font or background)
     * - call to action buttons
     * - important information
     * - buttons
     */
    primary,
    /**
     * Elements that are not main points of the site
     * - subtitle
     * - secondary buttons and info boxes
     */
    secondary: '#E0A984',

    background: {
      first: '#FFF',
      second: '#DFE6E6',
      error: '#f6e7e7',
      hover: '#F6F6F6',
    },
    border: '#CACACA',
    borderError: '#CB4646',
    loaderStrokeLight: '#FFF',
    loaderStrokeDark: primary,
    button: {
      primary: {
        background: primary,
        text: '#FFF',
      },
      secondary: {
        text: primary,
        background: '#FFF',
        border: primary
      },
      secondaryDisabled: {
        text: '#7C8080',
        border: '#C8C8C8'
      },
      disabled: {
        background: '#C8C8C8',
        text: '#7C8080',
      }
    },
    amount: {
      negative: '#CF1111',
      positive: '#43770E'
    },
    text: {
      first: '#3E4040',
      second: '#7C8080',
      third: '#BABFBF',
      error: '#E81717',
    },
    icons: '#7C8080',
    iconsDisabled: '#c1c4c4',
  },
  fontFamily: 'Roboto,Arial,Sans-serif',
  fontSize: {
    m3: '10px',
    m2: '11px',
    m1: '12px',
    basal: '13px',
    p1: '14px',
    p2: '15px',
  },
  fontWeight: {
    regular: 500 as const,
    medium: 600 as const,
    bold: 700 as const,
  },
};

export type ThemeInterface = typeof Theme;
