import baseStyled, { ThemedStyledInterface } from 'styled-components';
import { ThemeInterface } from 'Modules/App/Styles/Theme';

export const styled = baseStyled as ThemedStyledInterface<ThemeInterface>;
