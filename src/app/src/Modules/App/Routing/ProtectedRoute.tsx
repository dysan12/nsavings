import { useSelector, useDispatch } from 'react-redux';
import { AuthSlice } from 'Modules/User/Auth/Slice';
import { RouteProps, Route } from 'react-router-dom';
import React from 'react';

function ProtectedComponent({ component }: { component: any }) {
  const dispatch = useDispatch();
  const isUserAuthenticated = useSelector(AuthSlice.selectors.isUserAuthenticated());
  if (isUserAuthenticated === false) {
    dispatch(AuthSlice.actions.logout());
  }

  return component;
}

interface Props extends RouteProps {}

export function ProtectedRoute({ component, children, render, ...rest }: Props) {
  return (
    <Route
      {...rest}
      render={() => {
        return <ProtectedComponent component={component || children} />;
      }}
    />
  );

}
