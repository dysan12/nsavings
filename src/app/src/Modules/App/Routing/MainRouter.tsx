import React from "react";
import { Switch, Route, Router } from "react-router-dom";
import { RegisterPage } from 'Modules/User/Register/RegisterPage';
import { Routes } from 'Modules/App/Routing/Routes';
import { Dashboard } from 'Modules/Dashboard/Dashborad';
import { ProtectedRoute } from 'Modules/App/Routing/ProtectedRoute';
import { AppHistory } from 'Modules/App/Routing/History';
import { LoginPage } from 'Modules/User/Auth/LoginPage';
import { PurposesPanel } from 'Modules/Purposes/PurposesPanel';

export function MainRouter() {
  return (
    <>
      <Router history={AppHistory} >
        <Switch>
          <Route path={Routes.register()}>
            <RegisterPage />
          </Route>
          <Route path={Routes.login()}>
            <LoginPage />
          </Route>
          <ProtectedRoute exact={true} path={Routes.dashboard()}>
            <Dashboard />
          </ProtectedRoute>
          <ProtectedRoute exact={true} path={Routes.purposes()}>
            <PurposesPanel />
          </ProtectedRoute>
          <Route>
            <h3>Not found</h3>
          </Route>
        </Switch>
      </Router>
    </>
  );
}
