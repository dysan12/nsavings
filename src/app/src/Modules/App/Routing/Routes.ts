export namespace Routes {
  export const register = () => '/register';
  export const login = () => '/login';
  export const purposes = (id?: string) => `/purposes/${id ?? ':purposeId'}`
  export const dashboard = () => '/';
}
