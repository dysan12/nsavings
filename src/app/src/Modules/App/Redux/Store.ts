import createSagaMiddleware from 'redux-saga';
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { createRootReducer } from 'Modules/App/Redux/RootReducer';
import { routerMiddleware } from 'connected-react-router';
import { AppHistory } from 'Modules/App/Routing/History';
import { rootSaga } from 'Modules/App/Redux/RootSaga';

function createStore() {
  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    reducer: createRootReducer({ history: AppHistory }),
    middleware: [
      ...getDefaultMiddleware({
        thunk: false,
      }),
      routerMiddleware(AppHistory),
      sagaMiddleware,
    ],
  });

  sagaMiddleware.run(rootSaga);

  return store;
}

export const store = createStore();
