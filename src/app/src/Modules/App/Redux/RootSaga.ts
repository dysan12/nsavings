import { all } from '@redux-saga/core/effects';
import { userSagas } from 'Modules/User/Saga';

export function* rootSaga() {
  yield all([
    userSagas(),
  ]);
}
