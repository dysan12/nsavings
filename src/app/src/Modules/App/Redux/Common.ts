import { PayloadAction } from '@reduxjs/toolkit';

export enum AsyncStatus {
  Idle = 'IDLE',
  Pending = 'PENDING',
  Successful = 'SUCCESSFUL',
  Failed = 'FAILED'
}

export type PA<T> = PayloadAction<T>;
