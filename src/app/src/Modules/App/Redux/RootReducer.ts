import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import { combineReducers } from 'redux';
import { UserReducers } from 'Modules/User/Reducers';

interface CreateRootReducerParams {
  history: History
}

export function createRootReducer({ history }: CreateRootReducerParams) {
  return combineReducers({
    router: connectRouter(history),
    ...UserReducers,
  });
}
