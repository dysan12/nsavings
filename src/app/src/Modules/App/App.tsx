import React from 'react';
import { ThemeProvider } from 'styled-components';
import { store } from 'Modules/App/Redux/Store';
import { Provider } from 'react-redux';
import { GlobalStyle } from 'Modules/App/Styles/GlobalStyle';
import { Theme } from 'Modules/App/Styles/Theme';
import { ConnectedRouter } from 'connected-react-router';
import { AppHistory } from 'Modules/App/Routing/History';
import { MainRouter } from 'Modules/App/Routing/MainRouter';
import { initTranslations } from 'Modules/App/Locale/Locale';
import { ApolloProvider } from '@apollo/client';
import { ToastContainer } from 'Modules/App/Components/ToastContainer';
import 'react-toastify/dist/ReactToastify.css';
import 'swiper/swiper-bundle.css';
import { ApolloClientManager } from 'Modules/App/Api/ApolloClientManager';

initTranslations();

const client = ApolloClientManager.getClient();
function App() {
  return (
    <Provider store={store}>
      <ApolloProvider client={client}>
        <ConnectedRouter history={AppHistory}>
          <ThemeProvider theme={Theme}>
            <GlobalStyle />
            <MainRouter />
            <ToastContainer />
          </ThemeProvider>
        </ConnectedRouter>
      </ApolloProvider>
    </Provider>
  );
}

export default App;
